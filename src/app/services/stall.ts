import { Vendor } from "./vendor";

export class Stall{
    stall_id: Number;
    location: String;
    name: String;
    description: String;
    book_stock: Number;
    queue_stock: Number;
    created_by: String;
    updated_by: String;
    is_deleted: Boolean;
    food_id: Number;
    vendor_staff_nip:String;
    vendor:Vendor;
}