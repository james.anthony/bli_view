export class RecommendationReq{
    key: String;
    nip: String;
    top: number;
    actionType: String;
}

export class RecommendationReturn{
    status: String;
    result: string[];
}