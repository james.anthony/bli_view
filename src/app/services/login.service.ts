import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, throwError} from 'rxjs';
import { Credential } from '../login/Credential';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private masterUrl = environment.apiUrl;
  private url = this.masterUrl + 'auth';
  constructor(private http: HttpClient) { }

  login(login: Credential): Observable<any> {
    return this.http.post(this.url, login, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      responseType: 'json',
      observe: 'response'
    })
  }
}
