    export class Material{
        code: String;
        name: String;
        year: Number;
        url: String;
        created_by: String;
        updated_by: String;
    }