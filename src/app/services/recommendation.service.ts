import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RecommendationReq, RecommendationReturn } from './recommendation';

@Injectable({
  providedIn: 'root'
})
export class RecommendationService {
  url: string;
  recommendationReq: RecommendationReq;
  token = localStorage.getItem('token');
  userNip = localStorage.getItem('nip');

  constructor(private http: HttpClient) { 
    this.url =  'http://10.20.228.116:8003/api/summa/';
    
    this.recommendationReq = {
        key: 'BLI123',
        nip: this.userNip,
        top: 3,
        actionType: 'predict'
    }
  }

  getRecommendation(){
    return this.http.post<RecommendationReturn>(this.url, this.recommendationReq, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
}