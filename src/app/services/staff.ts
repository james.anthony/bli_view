import { Division } from "./division";

export class Staff{
    nip: String;
    name: String;
    dob: Date;
    gender: String; //ubah
    domain: String;
    program: String;
    internal: Boolean;
    role: String; //ubah
    flag_trainer: Boolean;
    flag_trainee: Boolean;
    created_at: Date;
    created_by: String;
    updated_at: Date;
    updated_by: String;
    is_deleted: Boolean;
    division: Division; //ubah
    status: String //temp
}