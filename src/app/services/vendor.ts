export class Vendor{
    vendor_staff_nip: String;
    address: String;
    email: String;
    phone: String;
    start_date: Number;
    end_date: Number;
    created_by: String;
    updated_by: String;
}