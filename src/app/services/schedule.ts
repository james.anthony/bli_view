export class Schedule{
    id: Number;
    trainer_nip: String;
    class_code: Number;
    program_code: String;
    material_code: String;
    training_date: Date;
 }