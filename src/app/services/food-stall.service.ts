import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stall } from '../services/stall';
import { Vendor } from '../services/vendor';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FoodStallService {
  private masterUrl = environment.apiUrl;
  private url = this.masterUrl + 'stall/';
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {  }

  getAllStalls(){
    return this.http.get<Stall[]>(this.url+'all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getInqStalls(){
    return this.http.get<Stall[]>(this.url+'inquiry', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getStallByLocation(location: String){
    return this.http.get<Stall>(this.url+'location/'+location, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createStall(stall: Stall){
    return this.http.post<Stall>(this.url, stall, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateStall(stall: Stall){
    return this.http.put<Stall>(this.url, stall, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  deleteStall(stall: Stall){
    return this.http.delete<Stall>(this.url+'delete/'+stall, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getAllStallId(id: Number){
    return this.http.get<Stall>(this.url+'id', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getOneStallById(id: Number){
    return this.http.get<Stall>(this.url+'detail/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getOneStallByNip(nip: String){
    return this.http.get<Stall>(this.url+'detail/nip/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  checkStock(location: String){
    return this.http.get<Stall>(this.url+'stock/check/'+location, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  reduceStock(id: Number){
    return this.http.get<Stall>(this.url+'stock/reduce/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  resetStock(){
    return this.http.get<Stall>(this.url+'stock/reset/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  //vendors
  getVendors(){
    return this.http.get<Vendor[]>(this.url+'vendor/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getVendorByNip(nip:String){
    return this.http.get<Vendor>(this.url+'vendor/' +nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createVendor(vendor: Vendor){
    return this.http.post<Vendor>(this.url+'vendor/', vendor, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateVendor(vendor: Vendor){
    return this.http.put<Vendor>(this.url+'vendor', vendor, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  //medal vendor
  getSoldOut(id: Number){
    return this.http.get<Stall>(this.url+'soldout/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
}