import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Booking } from '../layout/food-stall/booking';
import { BookingResult } from 'src/app/services/bookingresult';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private masterUrl = environment.apiUrl;
  private url = this.masterUrl + 'booking/';
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {}
  getListBooking(){
    return this.http.get<Booking[]>(this.url+'all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getBookingById(id: Number){
    return this.http.get<Booking>(this.url+'detail/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getTotalBookingByNip(nip: String){
    return this.http.get<Booking[]>(this.url+'booking/total/nip/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getBookingByNip(nip: String){
    return this.http.get<Booking>(this.url+'nip/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  isBooked(nip: String){
    return this.http.get<boolean>(this.url+'today/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getBookingByTotal(){
    return this.http.get<Booking>(this.url+'totalbook/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getTotalBookingByDate(stall_id: Number){
    return this.http.get<BookingResult[]>(this.url+'total/date/'+stall_id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getMedal(){
    return this.http.get<Booking>(this.url+'medals/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createBooking(booking: Booking){
    return this.http.post<Booking>(this.url, booking, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateConfirmation(booking: Booking){
    return this.http.put<boolean>(this.url+'confirmation',booking, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateRating(booking: Booking){
    return this.http.put<boolean>(this.url+'rating',booking, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  /////////////////////////////
  getAvgRating(id: Number){
    return this.http.get<Booking>(this.url+'rating/average/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getStallRank(id: Number){
    return this.http.get<Booking>(this.url+'rank/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getFirstRank(){
    return this.http.get<Booking[]>(this.url+'first', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getRareFoodBadge(nip: String){
    return this.http.get(this.url+'rare/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTotalBookMonth(id: Number){
    return this.http.get<Booking>(this.url+'total/month/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTotalBook(id: Number){
    return this.http.get<Booking>(this.url+'total/stall/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTotalBookDate(id: Number){
    return this.http.get(this.url+'total/date/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getFavoriteStall(nip: String){
    return this.http.get<Booking[]>(this.url+'favorite/food/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getVisitAgain(nip: String){
    return this.http.get<Booking[]>(this.url+'least/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTrendingPick(){
    return this.http.get<Booking[]>(this.url+'trend/picks', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getEnthusiastBadge(nip: String){
    return this.http.get(this.url+'food/enthusiast/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getHealthyBadge(){
    return this.http.get(this.url+'food/healthy/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getFeastBadge(){
    return this.http.get(this.url+'food/feast/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

}