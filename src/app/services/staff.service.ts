import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, throwError} from 'rxjs';
import { Staff } from '../services/staff';
import { Division } from './division';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StaffService {
  private masterUrl = environment.apiUrl;
  private url = this.masterUrl + 'staff/';  
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) { }

  getAllStaffs(){
    return this.http.get<Staff[]>(this.url+'all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getAllStaffsByProgram(program: String){
    return this.http.get<Staff[]>(this.url+'all/'+program, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getAllTrainingStaffsByProgram(program: String){
    return this.http.get<Staff[]>(this.url+'all/training/'+program, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getAllTrainees(){
    return this.http.get<Staff[]>(this.url+'trainees', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getAllTrainers(){
    return this.http.get<Staff[]>(this.url+'trainers', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getAllDivisions(){
    return this.http.get<Division[]>(this.url+'division', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getStaffById(nip: String){
    return this.http.get<Staff>(this.url+'detail/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createStaff(staff: Staff){
    console.log(staff);
    return this.http.post<Staff>(this.url, staff, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateStaff(staff: Staff){
    console.log(staff);
    return this.http.put<Staff>(this.url, staff, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  deleteStaff(nip: String){
    return this.http.delete<Staff>(this.url+'delete/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTrainingBadge(nip: String){
    return this.http.get(this.url+'training/badge/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTrainerBadge(nip: String){
    return this.http.get(this.url+'trainer/badge/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTotalTrainerToday(){
    return this.http.get<Number>(this.url+'trainer/today/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
  getTotalTraineeToday(){
    return this.http.get<Number>(this.url+'trainee/today/', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }
}