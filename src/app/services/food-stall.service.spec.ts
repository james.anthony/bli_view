import { TestBed, inject } from '@angular/core/testing';

import { FoodStallService } from './food-stall.service';

describe('FoodStallService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FoodStallService]
    });
  });

  it('should be created', inject([FoodStallService], (service: FoodStallService) => {
    expect(service).toBeTruthy();
  }));
});
