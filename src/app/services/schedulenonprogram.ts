export class ScheduleNonProgram{
    id: Number;
    schedule_id: Number;
    staff_nip: String;
    class_code: String;
    training_date: Date;
}