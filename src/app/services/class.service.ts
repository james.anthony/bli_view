import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Class } from './class';
import { Material } from './material';
import { Program } from './program';
import { Competency } from './competency';
import { Schedule } from './schedule';
import { ScheduleNonProgram } from './schedulenonprogram';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClassService {
  private masterUrl = environment.apiUrl;
  private url = this.masterUrl + 'class/';
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) { }

  getListClass(){
    return this.http.get<Class[]>(this.url+'class/all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListAvailableClass(){
    return this.http.get<Class[]>(this.url+'class/available', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListUnavailableClass(){
    return this.http.get<Class[]>(this.url+'class/unavailable', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getClassByCode(code:String){
    return this.http.get<Class>(this.url+'class/detail/'+code, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createClass(classEntity:Class){
    return this.http.post<Class>(this.url+'class/'+classEntity, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateClass(classEntity:Class){
    return this.http.put<Class>(this.url+'class/'+classEntity, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListMaterial(){
    return this.http.get<Material[]>(this.url+'material/all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getMaterialById(code: String){
    return this.http.get<Material>(this.url+'material/detail/'+code, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createMaterial(material: Material){
    return this.http.post<Material>(this.url+'material/',material, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateMaterial(material: Material){
    return this.http.put<Material>(this.url+'material/', material, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListProgram(){
    return this.http.get<Program[]>(this.url+'program/all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getProgramByCode(code:String){
    return this.http.get<Program>(this.url+'program/detail/'+code, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createProgram(program: Program){
    return this.http.post<Program>(this.url+'program/',program, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateProgram(program: Program){
    return this.http.put<Program>(this.url+'program/',program, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  deleteProgram(code:String){
    return this.http.delete<Program>(this.url+'program/delete/'+code, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListCompetency(){
    return this.http.get<Competency[]>(this.url+'competency/all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListCompetencybyCode(materialCode: String){
    return this.http.get<Competency[]>(this.url+'competency/material/'+materialCode, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getCompetencyByTrainerNip(trainerNip: String){
    return this.http.get<Competency[]>(this.url+'competency/detail/'+trainerNip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getCompetencyByCode(material_code: String){
    return this.http.get<Competency[]>(this.url+'competency/material/'+material_code, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createCompetency(competency: Competency){
    return this.http.post<Competency>(this.url+'competency/',competency, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListSchedule(){
    return this.http.get<Schedule[]>(this.url+'schedule/all', {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getListScheduleByProgram(program: String){
    return this.http.get<Schedule[]>(this.url+'schedule/all/'+program, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getScheduleById(id: Number){
    return this.http.get<Schedule>(this.url+'schedule/detail/'+id, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getScheduleByCode(code: String){
    return this.http.get<Schedule>(this.url+'schedule/class/'+code, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getScheduleByProgram(program: String){
    return this.http.get<Schedule>(this.url+'schedule/program/'+program, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  getScheduleByTrainerNip(nip: String){
    return this.http.get<Schedule>(this.url+'schedule/trainer/'+nip, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createSchedule(schedule: Schedule){
    return this.http.post<Schedule>(this.url+'schedule/',schedule, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  updateSchedule(schedule: Schedule){
    return this.http.put<Class>(this.url+'schedule/',schedule, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  createScheduleNonProgram(scheduleNonProgram:ScheduleNonProgram){
    return this.http.post<ScheduleNonProgram>(this.url+'schedule/nonprogram', scheduleNonProgram, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'json',
      observe: 'response'});
  }

  uploadMaterial(file: FormData){
    return this.http.post(this.url+'material/upload/', file, {
      headers: new HttpHeaders().set('Authorization', this.token),
      reportProgress: true,
      observe: 'events'})
  }

  downloadMaterial(filename: String){
    return this.http.get(this.url+'material/download/'+filename, {
      headers: new HttpHeaders().set('Authorization', this.token),
      responseType: 'blob'
    })
  }
  

  
}