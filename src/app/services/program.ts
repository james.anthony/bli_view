export class Program{
    code: String;
    description: String;
    start_date: Number;
    end_date: String;
    created_by: String;
    updated_by: String;
    is_deleted: Boolean;
 }