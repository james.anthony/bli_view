import {Component, Input} from '@angular/core';
import {AbstractControlDirective, AbstractControl} from '@angular/forms';

@Component({
    selector: 'app-show-errors',
    template: `
        <div *ngIf="shouldShowErrors()">
            <p style="margin-left:10px; font-size:0.8em; margin-top:-10px;" *ngFor="let error of listOfErrors()">{{error}}</p>
        </div>
    `
})
export class ShowErrorsComponent {

    private static readonly errorMessages = {
        'required': () => 'This field is required',
        'minlength': (params) => 'The min number of characters is ' + params.requiredLength,
        'maxlength': (params) => 'The max allowed number of characters is ' + params.requiredLength,
        'ipaddress': (params) => params.message,
        'pattern': (params) => 'Numbers only'
    };

    @Input()
    private control: AbstractControlDirective | AbstractControl;

    shouldShowErrors(): boolean {
        return this.control &&
            this.control.errors &&
            (this.control.dirty || this.control.touched);
    }

    listOfErrors(): string[] {
        return Object.keys(this.control.errors)
            .map(field => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        return ShowErrorsComponent.errorMessages[type](params);
    }
}
