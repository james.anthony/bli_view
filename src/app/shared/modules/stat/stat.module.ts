import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatComponent } from './stat.component';
import { ShowErrorsComponent } from '../../../error/show-errors.component';
import { MatCardModule, MatDialogModule } from '@angular/material';
import { MatGridListModule, MatIconModule } from '@angular/material';
import { CustomDialogComponent } from '../../../layout/custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../../../layout/info-dialog/info-dialog.component';

@NgModule({
    imports: [CommonModule, MatCardModule, MatGridListModule, MatIconModule, MatDialogModule],
    declarations: [StatComponent, ShowErrorsComponent, CustomDialogComponent, InfoDialogComponent],
    exports: [StatComponent, ShowErrorsComponent, CustomDialogComponent, InfoDialogComponent]
})
export class StatModule {}
