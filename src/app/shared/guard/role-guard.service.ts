import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService {

  constructor(private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const userRole = localStorage.getItem('role');
    const allowedRole = next.data.role;

    if (userRole == allowedRole) {
      return true;
    }

    // navigate to not found page
    if(userRole == 'ROLE_VENDOR'){
      this.router.navigate(['/dashboard-vendor']);
    }else if(userRole == 'ROLE_ADMIN'){
      this.router.navigate(['/admin-page']);
    }else if(userRole == 'ROLE_STAFF'){
      this.router.navigate(['/dashboard']);
    }else if(userRole == 'ROLE_BO'){
      this.router.navigateByUrl('https://10.20.212.231');
    }else{
      this.router.navigate(['/login']);
    }
    return false;
}
}
