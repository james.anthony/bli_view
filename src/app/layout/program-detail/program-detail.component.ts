import { Component, OnInit, ViewChildren, QueryList  } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ClassService } from 'src/app/services/class.service';
import { Program } from 'src/app/services/program';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Schedule } from 'src/app/services/schedule';
import { Router } from '@angular/router';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
const INQ_SCHEDULE: Schedule[] = [];

@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: ['./program-detail.component.scss']
})
export class ProgramDetailComponent implements OnInit {

  displayedColumnsClass = ['training_date', 'class_code', 'material_code', 'trainer_nip'];
  dataSourceSchedule = new MatTableDataSource<Schedule>(INQ_SCHEDULE);
  scheduleArray: Schedule[];

  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();

  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;
  code: String;
  program: Program;
  updateProgram: Program;
  updateForm: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute, private classService:ClassService, private dialog: MatDialog) {
    this.updateProgram = new Program();
    this.flag = true;
    this.route.params.subscribe( params => {
      this.code = params.code;
      this.classService.getProgramByCode(this.code).subscribe(res=>{
        this.program = res.body;
        this.classService.getListScheduleByProgram(this.program.code).subscribe(res=>{
          this.scheduleArray = res.body;
          const INQ_SCHEDULE = res.body;
          this.dataSourceSchedule = new MatTableDataSource<Schedule>(INQ_SCHEDULE);
          this.dataSourceSchedule.paginator = this.paginator.toArray()[0];
        })
      })
    });

  }

  ngOnInit() {

    this.updateForm = new FormGroup({
      description: new FormControl(),
      start_date: new FormControl(),
      end_date: new FormControl()
    });

    let top = document.getElementById('top');
    if (top !== null) {
    top.scrollIntoView();
    top = null;
    }  
  }

  ngAfterViewInit(){
  }

  noEmpty(arr){
    for(var i = 0; i<arr.length; i++){
      if(arr[i] == "" || arr[i] == null){
        arr[i] = "-";
      }
    }
  }

  toggleClass(){
    document.getElementById("btnLeft").classList.toggle('btn-active-green');
    document.getElementById("btnRight").classList.toggle('btn-active-red');
  }
  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.toggleClass();
    }
  }
  leftFunction(update){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
      this.toggleClass();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.updateProgram.code = this.program.code;
      this.updateProgram.description = update.description;
      this.updateProgram.start_date = update.start_date;
      this.updateProgram.end_date = update.end_date;
      this.classService.updateProgram(this.updateProgram).subscribe(res=>{
        console.log(res);
        if(res.status == 200 || res.status == 201){
          this.infoDialog("Detail program berhasil diubah");
          this.router.navigate(['/admin-page']);
        }
        else{
          this.infoDialog("Detail program gagal diubah. Silahkan mencoba kembali.");
        }
       
      })
    }
  }
  
  uploadFunction(data){
    console.log(data);
  }
  downloadFunction(data){
    console.log(data);
  }

  openDialog(bookingData) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: bookingData
    };
    
    const dialogRef = this.dialog.open(CustomDialogComponent, dialogConfig);
  }
  
  infoDialog(text) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: text
    };
    
    const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);  
  }


}
