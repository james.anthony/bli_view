import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatInputModule, MatIconModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import { MatTableModule, MatPaginatorModule } from '@angular/material';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { ProgramDetailRoutingModule } from './program-detail-routing.module';
import { ProgramDetailComponent } from './program-detail.component';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';

@NgModule({
  imports: [
    CommonModule,
    ProgramDetailRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule,
    StatModule
  ],
  declarations: [ProgramDetailComponent],
  providers: [],
  entryComponents:  [CustomDialogComponent, InfoDialogComponent]
})
export class ProgramDetailModule { }
