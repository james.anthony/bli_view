import { Component, OnInit } from '@angular/core';
import { StaffService } from 'src/app/services/staff.service';
import { BookingService } from 'src/app/services/booking.service';
import { Staff } from 'src/app/services/staff';
import { HttpClient } from '@angular/common/http';
import { RecommendationService } from 'src/app/services/recommendation.service';
import { RecommendationReturn } from 'src/app/services/recommendation';

export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' }
];
import { Booking } from 'src/app/layout/food-stall/booking';
import { ClassService } from 'src/app/services/class.service';
import { Schedule } from 'src/app/services/schedule';
import { Material } from 'src/app/services/material';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
    displayedColumns = ['position', 'name', 'weight', 'symbol'];
    places: Array<any> = [];
    panelOpenState = false;
    allFavoriteArray: Booking[];
    allVisitArray: Booking[];
    totalBook: Booking[];
    allTrendingArray: Booking[];
    nip:String;
    staff:Staff;
    schedule:Schedule;
    material:Material;

    topRec: RecommendationReturn;
    //badges
    enthusiast: number;
    healthy: number;
    feast: number;
    rare: number;
    trainer: number;
    training: number;

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    }

    constructor(private staffService:StaffService, private http: HttpClient, private recommendationService:RecommendationService, private bookService:BookingService, private classService:ClassService) {

        this.nip = localStorage.getItem('nip');
        this.allFavoriteArray = [];
        this.allVisitArray = [];
        this.allTrendingArray = [];
        this.totalBook = [];
        this.enthusiast = 0;
        this.healthy = 0;
        this.feast = 0;
        this.rare = 0;
        this.trainer = 0;
        this.training = 0;

        this.topRec = {status:'', result:[]}
        

        this.staffService.getStaffById(this.nip).subscribe(res=>{
            this.staff=res.body;
            if(this.staff.flag_trainee==true){
                this.classService.getScheduleByProgram(this.staff.program).subscribe(res=>{
                    this.schedule = res.body;
                    this.classService.getMaterialById(this.schedule.material_code).subscribe(res=>{
                        this.material = res.body;
                    })
                })
            }
            else if(this.staff.flag_trainer==true){
                this.classService.getScheduleByTrainerNip(this.nip).subscribe(res=>{
                    this.schedule = res.body;
                    this.classService.getMaterialById(this.schedule.material_code).subscribe(res=>{
                        this.material = res.body;
                    })
                })
            }
        });

        this.recommendationService.getRecommendation().subscribe(res=>{
            this.topRec = res.body;
        });
        
        this.bookService.getFavoriteStall(this.nip).subscribe(res=>{
            //console.log(res.body);
            this.allFavoriteArray = res.body;
        });
        this.bookService.getVisitAgain(this.nip).subscribe(res=>{
            //console.log(res.body);
            this.allVisitArray = res.body;
        });
        this.bookService.getTotalBookingByNip(this.nip).subscribe(res=>{
            console.log(res.body);
            this.totalBook = res.body;
        });
        this.bookService.getTrendingPick().subscribe(res=>{
            console.log(res.body);
            this.allTrendingArray = res.body;
        });
        this.bookService.getEnthusiastBadge(this.nip).subscribe(res=>{
            //console.log(res.body);
            var x = res.body;
            if(x == 0)
            {
                this.enthusiast = 0;
            }
        });
        this.bookService.getHealthyBadge().subscribe(res=>{
            //console.log(res.body);
            var x = res.body;
            if(x == 0)
            {
                this.healthy = 0;
            }
        });
        this.bookService.getFeastBadge().subscribe(res=>{
            //console.log(res.body);
            var x = res.body;
            if(x == 0)
            {
                this.feast = 0;
            }
        });
        this.bookService.getRareFoodBadge(this.nip).subscribe(res=>{
            //console.log(res.body);
            var x = res.body;
            if(x == 0)
            {
                this.rare = 0;
            }
        });
        this.staffService.getTrainerBadge(this.nip).subscribe(res=>{
            var x = res.body;
            if(x == 0)
            {
                this.trainer = 0;
            }
        });
        this.staffService.getTrainingBadge(this.nip).subscribe(res=>{
            var x = res.body;
            if(x == 0)
            {
                this.training = 0;
            }
        });

        this.places = [
            {
                imgSrc: 'assets/images/BLI01.jpeg',
                place: 'Finalis Miss Grand Indonesia 2018',
                description:
                    'Finalis Miss Grand Indonesia 2018 berkunjung ke BCA Learning Institute dan KEMENSOS RI',
                charge: 'BCA Learning Institute',
                location: 'Bogor, Jawa Barat'
            },
            {
                imgSrc: 'assets/images/BLI02.jpg',
                place: 'BCA Learning Institute',
                description:
                    'Sebagai salah satu instansi keuangan terbesar di Indonesia, PT Bank Central Asia Tbk. (BCA) berkomitmen untuk ikut hadir memberikan dukungan bagi tumbuhnya budaya belajar di perusahaan, khususnya perbankan.',
                charge: 'BCA Learning Institute',
                location: 'Bogor, Jawa Barat'
            },
            {
                imgSrc: 'assets/images/BLI03.jpg',
                place: 'Miss Grand Indonesia 2018',
                description:
                    'Dikna Faradiba selaku National Director of Miss Grand Indonesia mengungkapkan rasa terima kasihnya kepada BCA atas kesempatan berkunjung yang diberikan kepada para finalis Miss Grand Indonesia 2018.',
                charge: 'BCA Learning Institute',
                location: 'Bogor, Jawa Barat'
            }
        ];
    }

    ngOnInit() {
        // Hack: Scrolls to top of Page after page view initialized
        let top = document.getElementById('top');
        if (top !== null) {
        top.scrollIntoView();
        top = null;
        }
    }

    ngAfterViewInit() {
        
      }
}
