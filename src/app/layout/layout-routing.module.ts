import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { RoleGuardService } from '../shared/guard/role-guard.service';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate:[RoleGuardService], data:{role:'ROLE_STAFF'}
            },
            {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            },
            {
                path: 'components',
                loadChildren:
                    './material-components/material-components.module#MaterialComponentsModule'
            },
            {
                path: 'forms',
                loadChildren: './forms/forms.module#FormsModule'
            },
            {
                path: 'grid',
                loadChildren: './grid/grid.module#GridModule'
            },
            {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            },
            {
                path: 'blank-page',
                loadChildren: './blank-page/blank-page.module#BlankPageModule'
            },
            {
                path: 'black-page',
                loadChildren: './black-page/black-page.module#BlackPageModule'
            },
            {
                path: 'food-stall',
                loadChildren: './food-stall/food-stall.module#FoodStallModule', canActivate:[RoleGuardService], data:{role:'ROLE_STAFF'}
            },
            {
                path: 'dashboard-vendor',
                loadChildren: './dashboard-vendor/dashboard-vendor.module#DashboardVendorModule', canActivate:[RoleGuardService], data:{role:'ROLE_VENDOR'}
            },
            {
                path: 'admin-page',
                loadChildren: './admin-page/admin-page.module#AdminPageModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'confirmation-page',
                loadChildren: './confirmation-page/confirmation-page.module#ConfirmationPageModule', canActivate:[RoleGuardService], data:{role:'ROLE_STAFF'}
            },
            {
                path: 'class',
                loadChildren: './class/class.module#ClassModule', canActivate:[RoleGuardService], data:{role:'ROLE_STAFF'}
            },
            {
                path: 'shuttle',
                loadChildren: './shuttle/shuttle.module#ShuttleModule', canActivate:[RoleGuardService], data:{role:'ROLE_STAFF'}
            },
            {
                path: 'booking-detail',
                loadChildren: './booking-detail/booking-detail.module#BookingDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'booking-detail/:booking_id/:stall_id/:nip',
                loadChildren: './booking-detail/booking-detail.module#BookingDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'staff-detail',
                loadChildren: './staff-detail/staff-detail.module#StaffDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'staff-detail/:nip',
                loadChildren: './staff-detail/staff-detail.module#StaffDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'program-detail',
                loadChildren: './program-detail/program-detail.module#ProgramDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'program-detail/:code',
                loadChildren: './program-detail/program-detail.module#ProgramDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'schedule-detail',
                loadChildren: './schedule-detail/schedule-detail.module#ScheduleDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'schedule-detail/:code/:availability',
                loadChildren: './schedule-detail/schedule-detail.module#ScheduleDetailModule'
            },
            {
                path: 'vendor-detail',
                loadChildren: './vendor-detail/vendor-detail.module#VendorDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'vendor-detail/:stall_id',
                loadChildren: './vendor-detail/vendor-detail.module#VendorDetailModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            },
            {
                path: 'create-schedule',
                loadChildren: './create-schedule/create-schedule.module#CreateScheduleModule', canActivate:[RoleGuardService], data:{role:'ROLE_ADMIN'}
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
