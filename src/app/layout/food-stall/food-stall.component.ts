import { Component, OnInit } from '@angular/core';
import { FoodStallService } from '../../services/food-stall.service';
import { Booking } from './booking';
import { Stall } from '../../services/stall';
import { BookingService } from '../../services/booking.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-food-stall',
  templateUrl: './food-stall.component.html',
  styleUrls: ['./food-stall.component.scss']
})

export class FoodStallComponent implements OnInit {

  allMenuArray: Stall[];
  prasmananArray: Stall[];
  ugArray: Stall[];
  lgArray: Stall[];
  booking: Booking;
  nip: String = "";
  bookStat: Booking;
  
  constructor(private router: Router,private foodStallService: FoodStallService, private bookingService: BookingService, private dialog: MatDialog) {
    this.allMenuArray = [];
    this.prasmananArray = [];
    this.ugArray = [];
    this.lgArray = [];
    this.nip = localStorage.getItem('nip');
    this.booking = {booking_id: null, nip: this.nip, stall_id: null, stall_name: '', stall_content: '', rating: null, is_confirmed: null};
    this.nip = localStorage.getItem("nip");
    foodStallService.getAllStalls().subscribe(
      res=>{
        this.allMenuArray=res.body;
        this.allMenuArray.forEach(element => {
          if(element.location.includes('UG')){
            this.ugArray.push(element);
          } else if(element.location.includes('LG')){
            this.lgArray.push(element);
          } else if(element.location.includes('100')){
            this.prasmananArray.push(element);
          }
        });
      }
    );
    this.bookingService.getBookingByNip(this.nip).subscribe(res=>{
      console.log(res.body);
      this.booking = res.body;
      if(this.booking.is_confirmed == 1 && this.booking.rating == 0){
        this.router.navigate(['/confirmation-page']);
      }
    });
  }

  bookingAction(data){
    if(data){
      if(data.data.book_stock != 0){
        this.booking.stall_id = data.data.stall_id;
        this.booking.stall_name = data.data.food.name;
        this.booking.stall_content = data.data.food.description;
        this.bookingService.isBooked(this.nip).subscribe(res=>{
          if(res.body==false){
            this.foodStallService.checkStock(data.location).subscribe(res=>{
              if(res.body[0]!=0){
                this.foodStallService.reduceStock(data.data.stall_id).subscribe(res=>{
                  this.bookingService.createBooking(this.booking).subscribe(res=>{
                    if(res.status == 200 || res.status == 201)
                    {
                      this.infoDialog("proses booking berhasil");
                      this.router.navigate(['/confirmation-page']);
                    }
                    else{
                      this.infoDialog("proses booking gagal");
                    }
                  });
                });
              }
              else{
                this.infoDialog("stok telah habis");
              }
            });
          }
          else{
            this.infoDialog("Hari ini sudah booking");
          }
        });
      } else {
        this.infoDialog("makanan tidak bisa di book lagi!");
      }
    }
    
  }

  ngOnInit() {
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

  openDialog(bookingData) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: bookingData
    };
    
    const dialogRef = this.dialog.open(CustomDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => this.bookingAction(data)
    );    
  }

  infoDialog(text) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: text
    };
    
    const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);  
  }
}