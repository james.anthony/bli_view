export class Booking{
    booking_id: Number;
    nip: String;
    stall_id: Number;
    stall_name: String;
    stall_content: String;
    rating: Number;
    is_confirmed: Number;
}