import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatTooltipModule, MatDialogModule } from '@angular/material';

import { FoodStallRoutingModule } from './food-stall-routing.module';
import { FoodStallComponent } from './food-stall.component';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { StatModule } from '../../shared/modules/stat/stat.module';
@NgModule({
  imports: [
    CommonModule,
    FoodStallRoutingModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    StatModule
  ],
  declarations: [FoodStallComponent],
  entryComponents: [CustomDialogComponent, InfoDialogComponent]
})
export class FoodStallModule { }
