import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSelectModule, MatAutocompleteModule, MatIconModule, MatInputModule, MatDatepickerModule, MatFormFieldModule, MatChipsModule, MatNativeDateModule, MatButtonModule
 } from '@angular/material';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { CreateScheduleRoutingModule } from './create-schedule-routing.module';
import { CreateScheduleComponent } from './create-schedule.component';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
@NgModule({
  imports: [
    CommonModule,
    CreateScheduleRoutingModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    StatModule,
    MatAutocompleteModule
  ],
  declarations: [CreateScheduleComponent],
  entryComponents:  [CustomDialogComponent, InfoDialogComponent]
})
export class CreateScheduleModule { }
