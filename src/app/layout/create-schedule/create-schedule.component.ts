import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ClassService } from 'src/app/services/class.service';
import { Program } from 'src/app/services/program';
import { Material } from 'src/app/services/material';
import { Competency } from 'src/app/services/competency';
import { Class } from 'src/app/services/class';
import { StaffService } from 'src/app/services/staff.service';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { Staff } from 'src/app/services/staff';
import { Schedule } from 'src/app/services/schedule';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { HttpClient, HttpEventType , HttpHeaders} from '@angular/common/http';
import { ScheduleNonProgram } from 'src/app/services/schedulenonprogram';

@Component({
  selector: 'app-create-schedule',
  templateUrl: './create-schedule.component.html',
  styleUrls: ['./create-schedule.component.scss']
})
export class CreateScheduleComponent implements OnInit {

  program: Program[];
  material: Material[];
  competency: Competency[];
  class: Class[];
  staff: Staff[];
  competencyMaterial: Competency[];
  schedule: Schedule;
  newSchedule: FormGroup;
  scheduleNonProgram: ScheduleNonProgram;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  traineeCtrl = new FormControl();
  filteredTrainee: Observable<String[]>;
  selectedTrainee: String[] = [];
  allTrainee: String[] = [];
  token = localStorage.getItem('token');
  materialUpload: Material;
  selectedFile: File = null;

  @ViewChild('traineeInput') traineeInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(private http:HttpClient, private classService:ClassService, private staffService:StaffService,  private dialog: MatDialog) {
    this.materialUpload = new Material();
    this.scheduleNonProgram = new ScheduleNonProgram();
    classService.getListProgram().subscribe(res=>this.program = res.body); 
    classService.getListMaterial().subscribe(res=>this.material = res.body);
    classService.getListCompetency().subscribe(res=>this.competency=res.body);
    classService.getListClass().subscribe(res=>this.class=res.body);
    
    this.schedule = new Schedule();
    
    this.filteredTrainee = this.traineeCtrl.valueChanges.pipe(
      startWith(null),
      map((trainee: string | null) => trainee ? this._filter(trainee) : this.allTrainee.slice()));
  }

  ngOnInit() {
    this.newSchedule = new FormGroup({
      program: new FormControl(''),
      tanggal: new FormControl(''),
      materi: new FormControl(''),
      trainer: new FormControl(''),
      kelas: new FormControl(''),
      listTrainee: new FormControl('')
    })

    let top = document.getElementById('top');
    if (top !== null) {
    top.scrollIntoView();
    top = null;
    }
  }

  add(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.selectedTrainee.push(value.trim());
      }

      if (input) {
        input.value = '';
      }

      this.traineeCtrl.setValue(null);
    }
  }

  remove(trainee: string): void {
    const index = this.selectedTrainee.indexOf(trainee);

    if (index >= 0) {
      this.selectedTrainee.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.selectedTrainee.push(event.option.viewValue.substring(0,6));
    this.traineeInput.nativeElement.value = '';
    this.traineeCtrl.setValue(null);
  }

  private _filter(value: String): String[] {
    const filterValue = value.toLowerCase();

    return this.allTrainee.filter(element => element.toLowerCase().indexOf(filterValue) === 0);
  }

  createSchedule(data){
    this.schedule.class_code = data.kelas;
    this.schedule.material_code = data.materi;
    this.schedule.program_code = data.program;
    this.schedule.trainer_nip = data.trainer;
    this.schedule.training_date = data.tanggal;
    this.materialUpload.code = data.materi;
    this.materialUpload.url = this.selectedFile.name;
    
    this.onUpload();
    this.classService.updateMaterial(this.materialUpload).subscribe(res=>{
      console.log(res.body);
      this.classService.createSchedule(this.schedule).subscribe(res=>{
        console.log(res.status);

        this.selectedTrainee.forEach(element => {
          this.scheduleNonProgram.schedule_id = 1;
          this.scheduleNonProgram.staff_nip = element;
          this.scheduleNonProgram.class_code = data.kelas;
          this.scheduleNonProgram.training_date = data.tanggal;
          this.classService.createScheduleNonProgram(this.scheduleNonProgram).subscribe();
        });
        
        if(res.status == 200 || res.status == 201){
          this.infoDialog("Jadwal berhasil dibuat atau diubah");
        }
        else{
          this.infoDialog("Jadwal gagal dibuat atau dibuah. Silahkan coba kembali");
        }
      });
    });
    
  }

  changeTrainees(program){
    if(program.value=='NONPR'){
      this.staffService.getAllStaffsByProgram(program.value).subscribe(res=>{
        this.staff=res.body;
        this.staff.forEach(element => {
          this.allTrainee.push(element.nip+'-'+element.name);
        });
      })
    }
  }
  selectedTrainerChange(data){
    this.classService.getCompetencyByTrainerNip(data).subscribe(res=>this.competencyMaterial = res.body);
  }
  selectedMateriChange(data){
    this.classService.getListCompetencybyCode(data).subscribe(res=>this.competencyMaterial = res.body);
  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      console.log(target.files[0]);
    };
    reader.readAsBinaryString(target.files[0]);
 }

  onUpload(){ 
    const file = new FormData();
    file.append('file', this.selectedFile);
    console.log(file.get("file"));
    
    this.classService.uploadMaterial(file)
    .subscribe(event=>{
        if(event.type === HttpEventType.UploadProgress) {
            console.log('Upload Progress : ' + Math.round(event.loaded / event.total * 100) + '%'); 
        } else if (event.type === HttpEventType.Response) {
            console.log(event);
        }
    });
    
 }

 download(){
   this.classService.downloadMaterial(this.selectedFile.name).subscribe(res=>{
    var newBlob = new Blob([res], { type: "application" });
    
    // For other browsers: 
    // Create a link pointing to the ObjectURL containing the blob.
    const data = window.URL.createObjectURL(newBlob);

    var link = document.createElement('a');
    link.href = data;
    link.download = this.selectedFile.name;
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
    }, 100);
   })
 }

 openDialog(bookingData) {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  dialogConfig.data = {
      data: bookingData
  };
  
  const dialogRef = this.dialog.open(CustomDialogComponent, dialogConfig);
}

infoDialog(text) {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  dialogConfig.data = {
      data: text
  };
  
  const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);  
}
}