import { StaffDetailModule } from './staff-detail.module';

describe('StaffDetailModule', () => {
  let staffDetailModule: StaffDetailModule;

  beforeEach(() => {
    staffDetailModule = new StaffDetailModule();
  });

  it('should create an instance', () => {
    expect(staffDetailModule).toBeTruthy();
  });
});
