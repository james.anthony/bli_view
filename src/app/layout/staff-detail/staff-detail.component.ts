import { Component, OnInit } from '@angular/core';
import { StaffService } from 'src/app/services/staff.service';
import { ActivatedRoute } from '@angular/router';
import { Staff } from 'src/app/services/staff';
import { Program } from 'src/app/services/program';
import { ClassService } from 'src/app/services/class.service';
import { Division } from 'src/app/services/division';
import { Competency } from 'src/app/services/competency';
import { Material } from 'src/app/services/material';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { MatDialogConfig, MatDialog } from '@angular/material';

@Component({
  selector: 'app-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.scss']
})
export class StaffDetailComponent implements OnInit {

  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;
  division: Division[];
  editable: boolean = false;
  competency: Competency[];
  listCompetency: Competency[];
  newCompetency: Competency;
  updateStaff: FormGroup;

  nip:String;
  material_code:String;
  staff:Staff;
  program:Program[];
  material:Material[];

  constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private dialog: MatDialog,
    private staffService:StaffService, private classService:ClassService) {
    this.newCompetency = new Competency();
    this.route.params.subscribe( params => {
      this.nip = params.nip;
      this.staffService.getStaffById(this.nip).subscribe(res=>{
        this.staff = res.body;
        if(this.staff.flag_trainer==true) this.staff.status = 'Trainer';
        else if(this.staff.flag_trainee==true) this.staff.status = 'Trainee';
        else this.staff.status = 'None';
        console.log(this.staff);
        if(this.staff.flag_trainer==true){
          this.classService.getCompetencyByTrainerNip(this.nip).subscribe(res=>{
            this.competency = res.body;
            console.log(this.competency);
          })
        }
      })
    });
    this.classService.getListProgram().subscribe(res=>{
      this.program = res.body;
    })

    this.classService.getListMaterial().subscribe(res=>{
      this.material = res.body;
    })

    this.staffService.getAllDivisions().subscribe(res=>{
      this.division = res.body;
    })

  }

  ngOnInit() {

    this.updateStaff = new FormGroup({
      flag_status: new FormControl(),
      program: new FormControl(),
      division: new FormControl()
    });

    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }

  }

  toggleClass(){
    document.getElementById("btnLeft").classList.toggle('btn-active-green');
    document.getElementById("btnRight").classList.toggle('btn-active-red');
  }
  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.toggleClass();
    }
  }
  leftFunction(staff){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
      this.toggleClass();
    }else{
     this.rightIcon = "chevron_left";
     this.leftIcon = "create";
     this.flag = true;
     this.staff.program = staff.program;
     this.staff.division = staff.division;
     if(staff.flag_status=='Trainee'){
       this.staff.flag_trainee = true;
       this.staff.flag_trainer = false;
     }
     else if(staff.flag_status=='Trainer'){
      this.staff.flag_trainee = false;
      this.staff.flag_trainer = true;
     }
     else{
      this.staff.flag_trainee = false;
      this.staff.flag_trainer = false;
     }
     this.staffService.updateStaff(this.staff).subscribe(res=>{
       console.log(res.status);
       if(res.status == 200 || res.status == 201){
        this.infoDialog("Detail staff telah berhasil diubah");
        this.router.navigate(['/admin-page']);
      }
      else{
        this.infoDialog("Detail staff gagal diubah. Silahkan mencoba kembali");
      }
       
     });
    }
  }

  addCompetency(material_code){
    this.newCompetency.trainer_nip = this.staff.nip;
    this.newCompetency.material_code = material_code;
    this.classService.createCompetency(this.newCompetency).subscribe(res=>{
      console.log(res.status);
    });
  }

  openDialog(bookingData) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: bookingData
    };
    
    const dialogRef = this.dialog.open(CustomDialogComponent, dialogConfig);
  }
  
  infoDialog(text) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: text
    };
    
    const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);  
  }
}
