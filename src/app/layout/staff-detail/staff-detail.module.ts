import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatInputModule, MatIconModule, MatSelectModule} from '@angular/material';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { StaffDetailRoutingModule } from './staff-detail-routing.module';
import { StaffDetailComponent } from './staff-detail.component';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    StaffDetailRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    StatModule
  ],
  declarations: [StaffDetailComponent],
  entryComponents:  [CustomDialogComponent, InfoDialogComponent]
})
export class StaffDetailModule { }
