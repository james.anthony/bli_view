import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatDatepickerModule, 
          MatInputModule, 
          MatButtonModule, 
          MatFormFieldModule,
          MatIconModule, 
          MatAutocompleteModule, 
          MatNativeDateModule,
          MatSelectModule
        } from '@angular/material';

import { ScheduleDetailRoutingModule } from './schedule-detail-routing.module';
import { ScheduleDetailComponent } from './schedule-detail.component';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    ScheduleDetailRoutingModule,
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatSelectModule,
    StatModule
  ],
  declarations: [ScheduleDetailComponent],
  providers: [],
  entryComponents:  [CustomDialogComponent, InfoDialogComponent]
})
export class ScheduleDetailModule { }
