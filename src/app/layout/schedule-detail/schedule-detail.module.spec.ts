import { ScheduleDetailModule } from './schedule-detail.module';

describe('ScheduleDetailModule', () => {
  let scheduleDetailModule: ScheduleDetailModule;

  beforeEach(() => {
    scheduleDetailModule = new ScheduleDetailModule();
  });

  it('should create an instance', () => {
    expect(scheduleDetailModule).toBeTruthy();
  });
});
