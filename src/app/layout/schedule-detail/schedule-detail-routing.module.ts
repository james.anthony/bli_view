import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleDetailComponent } from './schedule-detail.component';

const routes: Routes = [{
  path:'',
  component: ScheduleDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleDetailRoutingModule { }
