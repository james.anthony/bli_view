import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ClassService } from 'src/app/services/class.service';
import { map, startWith } from 'rxjs/operators';
import { Schedule } from '../../services/schedule';
import { Program } from '../../services/program';
import { Competency } from '../../services/competency';
import { Material } from '../../services/material';
import { Router } from '@angular/router';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { MatDialogConfig, MatDialog } from '@angular/material';

@Component({
  selector: 'app-schedule-detail',
  templateUrl: './schedule-detail.component.html',
  styleUrls: ['./schedule-detail.component.scss']
})
export class ScheduleDetailComponent implements OnInit {

  newForm: FormGroup;
  today: number = Date.now();
  rightIcon: string = "chevron_left";
  competencyMaterial: Competency[];
  leftIcon: string = "create";
  flag: boolean = true;
  code: String;
  availability: String;
  status: String;
  schedule: Schedule;
  competency: Competency[];
  materialCode: String;
  program: Program[];
  material: Material[];
  listStatus: String[] = ['Occupied', 'Unoccupied'];
  myControl = new FormControl({value: 'Daniel', disabled: this.flag});
  
  constructor(private router: Router, private route: ActivatedRoute, 
    private classService:ClassService, private formBuilder: FormBuilder,private dialog: MatDialog) {
    this.today = Date.now();
    this.schedule = new Schedule();  
      this.route.params.subscribe( params => {
      this.schedule.class_code = params.code;
      this.availability = params.availability;
      if(this.availability=='true') this.availability = 'Unoccupied';
      else if(this.availability=='false') this.availability = 'Occupied';
      this.classService.getScheduleByCode(this.code).subscribe(res=>{
        this.schedule = res.body;
      });
    });

    this.classService.getListProgram().subscribe(res=>{
      this.program = res.body;
    });

    classService.getListMaterial().subscribe(res=>this.material = res.body);
    classService.getListCompetency().subscribe(res=>this.competency=res.body);

  }

  ngOnInit() {

    this.newForm = new FormGroup({
      materi: new FormControl('', [
      Validators.required
    ]),
    status: new FormControl(''),
    program: new FormControl(''),
    trainer: new FormControl(''),
    classCode: new FormControl(''),
    tanggal: new FormControl('')
  });

    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

  noEmpty(arr){
    for(var i = 0; i<arr.length; i++){
      if(arr[i] == "" || arr[i] == null){
        arr[i] = "-";
      }
    }
  }
  selectedMateriChange(data){
    this.classService.getListCompetencybyCode(data).subscribe(res=>this.competencyMaterial = res.body);
  }

  toggleClass(){
    document.getElementById("btnLeft").classList.toggle('btn-active-green');
    document.getElementById("btnRight").classList.toggle('btn-active-red');
  }
  materialChanged(code){
    this.materialCode = code;

    this.classService.getCompetencyByCode(this.materialCode).subscribe(res=>{
      this.competency = res.body;
    });
    
  }
  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.toggleClass();
      this.myControl.disable();
    }
  }
  leftFunction(data){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
      this.toggleClass();
      this.myControl.enable();
    }else{
      this.updateData(data);
    }
  }
  updateData(data){
    this.schedule.class_code = data.classCode;
    this.schedule.material_code = data.materi;
    this.schedule.program_code = data.program;
    this.schedule.trainer_nip = data.trainer;
    this.schedule.training_date = new Date();
    console.log(this.schedule)
    if(this.schedule.id == null)
    {
      this.classService.createSchedule(this.schedule).subscribe(res=>{
        if(res.status == 200 || res.status == 201){
          this.infoDialog("Detail jadwal telah berhasil diubah atau dibuat");
          this.router.navigate(['/admin-page']);
        }
        else{
          this.infoDialog("Detail jadwal gagal dubah atau dibuat. Silahkan mencoba kembali.");
        }
      })
    }
    else{
      this.classService.updateSchedule(this.schedule).subscribe(res=>{
        console.log(res.body);
        if(res.status == 200 || res.status == 201){
          this.infoDialog("Detail jadwal telah berhasil diubah atau dibuat");
          this.router.navigate(['/admin-page']);
        }
        else{
          this.infoDialog("Detail jadwal gagal dubah atau dibuat. Silahkan mencoba kembali.");
        }
      });
    }
  }
  openDialog(bookingData) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: bookingData
    };
    
    const dialogRef = this.dialog.open(CustomDialogComponent, dialogConfig);
  }
  
  infoDialog(text) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: text
    };
    
    const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);  
  }
}