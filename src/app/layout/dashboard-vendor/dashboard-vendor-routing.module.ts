import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardVendorComponent } from './dashboard-vendor.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardVendorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardVendorRoutingModule { }
