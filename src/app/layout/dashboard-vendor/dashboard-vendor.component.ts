import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import {MatPaginator, MatTableDataSource } from '@angular/material';
import { FoodStallService } from 'src/app/services/food-stall.service';
import { BookingService } from 'src/app/services/booking.service';
import { Stall } from 'src/app/services/stall';
import { Booking } from 'src/app/layout/food-stall/booking';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StaffService } from 'src/app/services/staff.service';
import { Staff } from 'src/app/services/staff';
import { BookingResult } from 'src/app/services/bookingresult';

const INQ_BOOK: BookingResult[] = [];

@Component({
  selector: 'app-dashboard-vendor',
  templateUrl: './dashboard-vendor.component.html',
  styleUrls: ['./dashboard-vendor.component.scss']
})

export class DashboardVendorComponent implements OnInit {
  newForm: FormGroup;
  menuForm: FormGroup;
  allBookArray: BookingResult[];
  updateStall: Stall;
  displayedColumns: String[] = ['dt', 'total_book', 'stall_name'];
  dataSourceBooking = new MatTableDataSource<BookingResult>(INQ_BOOK);

  editFlag: Number = 0;
  toggleText: String = "Edit";
  loginStaff: Staff;
  nip:String;
  stall:Stall;
  stallPosition:String = "";
  stallRank: Booking;
  firstRank: Booking[];
  rating : Booking;
  totalBookMonth : Booking;
  totalBook : Booking;
  totalSoldOut : Stall;
  totalTrainer: Number = 0;
  totalTrainee: Number = 0;
  totalVisitor: number = 0;

  constructor(private stallService:FoodStallService, private staffService: StaffService, private bookingService: BookingService) {
    this.nip = localStorage.getItem('nip');
    staffService.getStaffById(this.nip).subscribe(res=>{
      this.loginStaff = res.body;
    })

    
    stallService.getOneStallByNip(this.nip).subscribe(res=>{
      this.stall = res.body;
      if(this.stall.location.includes('UG')) this.stallPosition = 'Upperground';
      else if(this.stall.location.includes('LG')) this.stallPosition = 'Lowerground';
      else this.stallPosition = 'Prasmanan';
      bookingService.getTotalBookingByDate(this.stall.stall_id).subscribe(res=>{
        this.allBookArray=res.body;
        const INQ_BOOK=res.body;
        this.dataSourceBooking = new MatTableDataSource<BookingResult>(INQ_BOOK);
        this.dataSourceBooking.paginator = this.paginator.toArray()[0];

      })

      ///////////////////////
      this.staffService.getTotalTraineeToday().subscribe(res=>{
        this.totalTrainee = res.body;
      });
      this.staffService.getTotalTrainerToday().subscribe(res=>{
        this.totalTrainer = res.body;
      });

      this.totalVisitor = this.totalTrainee.valueOf() + this.totalTrainer.valueOf();

      /////////////////////
      this.bookingService.getStallRank(this.stall.stall_id).subscribe(res=>{
        this.stallRank = res.body;
      });
      this.bookingService.getFirstRank().subscribe(res=>{
        this.firstRank = res.body;
      });
      this.bookingService.getAvgRating(this.stall.stall_id).subscribe(res=>{
        this.rating = res.body;
      });
      this.bookingService.getTotalBookMonth(this.stall.stall_id).subscribe(res=>{
        this.totalBookMonth = res.body;
      });
      this.bookingService.getTotalBook(this.stall.stall_id).subscribe(res=>{
        this.totalBook = res.body;
      });
      this.stallService.getSoldOut(this.stall.stall_id).subscribe(res=>{
        this.totalSoldOut = res.body;
      });
    });

    this.updateStall = new Stall();
  }
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();

  ngOnInit() {
    
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }

    this.newForm = new FormGroup({
        stallName: new FormControl('', [
        Validators.required
      ]),
        menuMakanan: new FormControl('', [
        Validators.required
      ]),
        stockBook: new FormControl('', [
        Validators.required,
        Validators.pattern("^[0-9]*$")
      ]),
        stockQueue: new FormControl('', [
        Validators.required,
        Validators.pattern("^[0-9]*$")
      ])
      });

  }

  updateVendor(stall){
    this.updateStall.stall_id = this.stall.stall_id;
    this.updateStall.location = this.stall.location;
    this.updateStall.name = stall.stallName;
    this.updateStall.description = stall.menuMakanan;
    this.updateStall.book_stock = stall.stockBook;
    this.updateStall.queue_stock = stall.stockQueue;
    this.updateStall.vendor_staff_nip = this.stall.vendor.vendor_staff_nip;
    this.stallService.updateStall(this.updateStall).subscribe(res=>{
      console.log(res.body);
    })
  }  
  
  navMenu(){document.getElementById("mat-tab-label-0-0").click()}
  
  tabClick(){
    document.getElementById("menuSbmt").removeAttribute("disabled");
  }
}