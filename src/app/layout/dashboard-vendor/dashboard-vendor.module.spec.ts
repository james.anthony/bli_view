import { DashboardVendorModule } from './dashboard-vendor.module';

describe('DashboardVendorModule', () => {
  let dashboardVendorModule: DashboardVendorModule;

  beforeEach(() => {
    dashboardVendorModule = new DashboardVendorModule();
  });

  it('should create an instance', () => {
    expect(dashboardVendorModule).toBeTruthy();
  });
});
