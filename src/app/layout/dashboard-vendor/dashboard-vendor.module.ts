import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  MatButtonModule, 
  MatCardModule, 
  MatIconModule, 
  MatTableModule,
  MatPaginatorModule, 
  MatFormFieldModule, 
  MatGridListModule,
  MatInputModule,
  MatTabsModule,
} from '@angular/material';

import { DashboardVendorRoutingModule } from './dashboard-vendor-routing.module';
import { DashboardVendorComponent } from './dashboard-vendor.component';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardVendorRoutingModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    FlexLayoutModule,
    MatPaginatorModule,
    MatCardModule,
    FormsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    StatModule,
    MatFormFieldModule,
    MatTabsModule
  ],
  declarations: [DashboardVendorComponent]
})
export class DashboardVendorModule { }
