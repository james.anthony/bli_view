import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShuttleComponent } from './shuttle.component';

const routes: Routes = [
  {
    path: '',
    component: ShuttleComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShuttleRoutingModule { }
