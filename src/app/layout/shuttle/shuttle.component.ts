import { Component, OnInit } from '@angular/core';

export class Shuttle {
  title: string;
  img: string;
  shuttle_point: string;
  departure_time: string;
  pic: string;
}
export class ExpansionStepsExample {
  step = 0;
  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}

@Component({
  selector: 'app-shuttle',
  templateUrl: './shuttle.component.html',
  styleUrls: ['./shuttle.component.scss']
})
export class ShuttleComponent implements OnInit {
  shuttle: Shuttle[];
  img = "assets/images/blilokasi2.PNG";
  closedCounter = 6;

  constructor() {
      this.shuttle = [
          {title: 'Wisma Asia', img: 'assets/images/shuttle_wsa2.PNG', shuttle_point: 'Wisma Asia', departure_time: '06:00 AM', pic: 'Om Yoh (14045)'},
          {title: 'Alam Sutera', img: 'assets/images/shuttle_alsut.PNG', shuttle_point: 'Alam Sutera', departure_time: '06:00 AM', pic: 'Om Yoh (14045)'},
          {title: 'Kelapa Gading', img: 'assets/images/shuttle_kelapagading.PNG', shuttle_point: 'Kelapa Gading', departure_time: '06:00 AM', pic: 'Om Yoh (14045)'},
          {title: 'Bekasi', img: 'assets/images/shuttle_bekasi.PNG', shuttle_point: 'Bekasi', departure_time: '06:00 AM', pic: 'Om Yoh (14045)'},
          {title: 'Bogor', img: 'assets/images/shuttle_bogor.PNG', shuttle_point: 'Bogor', departure_time: '06:00 AM', pic: 'Om Yoh (14045)'},
          {title: 'Pondok Indah', img: 'assets/images/shuttle_pondokindah.PNG', shuttle_point: 'Pondok Indah', departure_time: '06:00 AM', pic: 'Om Yoh (14045)'}  
      ];
  
      
  }

  ngOnInit() {
      console.log(this.shuttle);

      let top = document.getElementById('top');
      if (top !== null) {
      top.scrollIntoView();
      top = null;
      }

  }

  setImg(url) {
      this.img = url;
      this.closedCounter--;
  }

  resetImg() {
      this.closedCounter++;
      if(this.closedCounter === 6)
          this.img = "assets/images/blilokasi2.PNG";
  }
}
