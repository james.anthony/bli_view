import { Component, OnInit } from '@angular/core';
import { BookingService } from 'src/app/services/booking.service';
import { Booking } from '../food-stall/booking';
import { RollOutAnimation } from './animation';
import { Router } from '@angular/router';


@Component({
  selector: 'app-confirmation-page',
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.scss'],
  animations: [RollOutAnimation]
})
export class ConfirmationPageComponent implements OnInit {
  animationStateFront = 'start';
  animationStateBack = 'end';
  booking:Booking;
  bookingFoodImg:String;
  nip:String;
  Arr = Array;
  starNumber:Number = 5;
  receiveFlag:Number = 1;
  is_confirmed:Number = 0;
  starTxt:String = "star_border";

  constructor( private bookingService:BookingService, private router: Router) {
    this.bookingFoodImg = "assets/images/batagor.jpg";
    this.nip = localStorage.getItem('nip');
    bookingService.getBookingByNip(this.nip).subscribe(res=>{
      this.booking = res.body;
      this.is_confirmed = this.booking.is_confirmed;
      if(this.is_confirmed == 1)
      {
        this.animationStateFront = this.animationStateFront === 'end' ? 'start' : 'end';
        this.animationStateBack = this.animationStateBack === 'start' ? 'end' : 'start';
      }
    })
  }

  ngOnInit() {
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
}

  changeStar(num){
    var index = num+1;
    for(var i = 0; i <= num; i++){
      document.getElementById(""+i+"").innerHTML = "star";
      document.getElementById(""+i+"").style.color = "gold";
    }
  }
  changeStarBorder(){
    for(var i = 0; i <= this.starNumber; i++){
      document.getElementById(""+i+"").innerHTML = "star_border";
      document.getElementById(""+i+"").style.color = "unset";
    }
  }
  rating(index){
    this.booking.rating = index+1;
    this.bookingService.updateRating(this.booking).subscribe(res=>{
      if(res.body==true){
        //go to url
        this.receiveFlag = 1;
        this.router.navigate(['/food-stall']);
      }
      else{
        alert('Service error');
      }
    });
  }
  confirmFood(booking){
    this.receiveFlag = 2;
    this.animationStateFront = this.animationStateFront === 'end' ? 'start' : 'end';
    this.animationStateBack = this.animationStateBack === 'start' ? 'end' : 'start';
    this.booking = booking;
    this.booking.is_confirmed = 1;
    this.bookingService.updateConfirmation(this.booking).subscribe(res=>{
      if(res.body==true){
        this.receiveFlag = 2;
      }
      else{
        alert('Service error');
      }
    });
  }
}