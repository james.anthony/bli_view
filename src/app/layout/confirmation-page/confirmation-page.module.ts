import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule, MatFormFieldModule } from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatListModule} from '@angular/material/list';

import { ConfirmationPageRoutingModule } from './confirmation-page-routing.module';
import { ConfirmationPageComponent } from './confirmation-page.component';
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    ConfirmationPageRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    FlexLayoutModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ConfirmationPageComponent]
})
export class ConfirmationPageModule { }
