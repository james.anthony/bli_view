import { Component, OnInit } from '@angular/core';
import { Stall } from 'src/app/services/stall';
import { FoodStallService } from 'src/app/services/food-stall.service';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Vendor } from 'src/app/services/vendor';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor-detail',
  templateUrl: './vendor-detail.component.html',
  styleUrls: ['./vendor-detail.component.scss']
})

export class VendorDetailComponent implements OnInit {

  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;
  updateVendor: FormGroup;
  vendor: Vendor;
  stall: Stall;
  updateStall: Stall;
  stall_id:Number;
  constructor(private router: Router, private route: ActivatedRoute, private stallService:FoodStallService,  private dialog: MatDialog) {
    this.updateStall = new Stall();
    this.vendor = new Vendor();
    this.route.params.subscribe( params => {
      this.stall_id = params.stall_id;
      this.stallService.getOneStallById(this.stall_id).subscribe(res=>{
        this.stall = res.body;
      })
    });
  }

  ngOnInit() {
    this.updateVendor = new FormGroup({
      email: new FormControl(),
      address: new FormControl(),
      phone: new FormControl()
    });

    let top = document.getElementById('top');
    if (top !== null) {
    top.scrollIntoView();
    top = null;
    }

  }

  toggleClass(){
    document.getElementById("btnLeft").classList.toggle('btn-active-green');
    document.getElementById("btnRight").classList.toggle('btn-active-red');
  }
  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.toggleClass();
    }
  }
  leftFunction(updateVendor){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
      this.toggleClass();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      
      this.vendor.vendor_staff_nip = this.stall.vendor.vendor_staff_nip;
      this.vendor.address = updateVendor.address;
      this.vendor.email = updateVendor.email;
      this.vendor.phone = updateVendor.phone;
      console.log(this.updateStall);
      console.log(this.vendor);
      this.stallService.updateVendor(this.vendor).subscribe(res=>{
        if(res.status == 200 || res.status == 201){
          this.infoDialog("Detail vendor berhasil diubah");
          this.router.navigate(['/admin-page']);
        }
        else{
          this.infoDialog("Detail vendor gagal diubah. Silahkan mencoba kembali");
        }
      });
    }
  }

  openDialog(bookingData) {
    const dialogConfig = new MatDialogConfig();
  
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
  
    dialogConfig.data = {
        data: bookingData
    };
    
    const dialogRef = this.dialog.open(CustomDialogComponent, dialogConfig);
  }
  
  infoDialog(text) {
    const dialogConfig = new MatDialogConfig();
  
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
  
    dialogConfig.data = {
        data: text
    };
    
    const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);  
  }
}