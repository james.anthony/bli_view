import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatInputModule, MatIconModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { VendorDetailRoutingModule } from './vendor-detail-routing.module';
import { VendorDetailComponent } from './vendor-detail.component';

import { StatModule } from 'src/app/shared/modules/stat/stat.module';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    VendorDetailRoutingModule,
    MatButtonModule,
    StatModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [VendorDetailComponent],
  entryComponents:  [CustomDialogComponent, InfoDialogComponent]
})
export class VendorDetailModule { }
