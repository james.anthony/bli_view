import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-custom-dialog',
  templateUrl: './custom-dialog.component.html',
  styleUrls: ['./custom-dialog.component.scss']
})
export class CustomDialogComponent implements OnInit {

  listData: any;

  constructor(
    private dialogRef: MatDialogRef<CustomDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {

    this.listData = data;
  }

  ngOnInit() {

  }

  save() {
    this.dialogRef.close(this.listData)
  }

  close() {
    this.dialogRef.close();
  }
}
