import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import {  MatTabsModule, MatCardModule, MatFormFieldModule,MatPaginatorModule, MatButtonModule,MatInputModule,
  MatDatepickerModule,MatNativeDateModule,MatTableModule,
  MatSelectModule } from '@angular/material';
import { ClassRoutingModule } from './class-routing.module';
import { ClassComponent } from './class.component';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    MatCardModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatButtonModule,
    MatFormFieldModule,
    ClassRoutingModule
  ],
  declarations: [ClassComponent]
})
export class ClassModule { }
