import { Component, OnInit, ViewChild, ViewChildren, AfterViewInit, QueryList  } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { HttpClient, HttpEventType} from '@angular/common/http';
import { ClassService } from 'src/app/services/class.service';
import { StaffService } from 'src/app/services/staff.service';
import { Staff } from 'src/app/services/staff';
import { Schedule } from 'src/app/services/schedule';
import { Material } from 'src/app/services/material';

export interface JadwalKelas {
  tanggal: string;
  namaTopik: string;
  kelas: string;
}
const INQ_PESERTA: Staff[] = [];
const INQ_SCHEDULE: Schedule[] = [];

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.scss']
})

export class ClassComponent implements OnInit, AfterViewInit  {

  nip: String;
  staff: Staff;
  trainer:Staff;
  pesertaTraining: Staff[];
  schedule: Schedule;
  scheduleList: Schedule[];
  material: Material;
  dataSource1: MatTableDataSource<JadwalKelas>;
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  dataSourceStaff = new MatTableDataSource<Staff>(INQ_PESERTA);
  dataSourceSchedule = new MatTableDataSource<Schedule>(INQ_SCHEDULE);


  selectedFile: File = null;
  displayedColumnsSchedule = ['training_date', 'material_code', 'class_code'];
  displayedColumnsPeserta = ['nip', 'name', 'division_name'];
  trainerFlag: boolean;
  traineeFlag: boolean;
  progFlag: boolean = true;
  kodeMateri: String;
  namaMateri: String;
  tahunMateri: Number;
  urlMateri:String;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
}
  constructor(private http:HttpClient, private classService: ClassService, private staffService: StaffService) {
    this.nip = localStorage.getItem('nip');
    this.staffService.getStaffById(this.nip).subscribe(res=>{
      this.staff = res.body;
      if(this.staff.program == 'NONPR')
      {
        this.progFlag = false;
      }
      if(this.staff.flag_trainee==true){
        this.traineeFlag = true;
        this.trainerFlag = false;
        classService.getScheduleByProgram(this.staff.program).subscribe(res=>{
          this.schedule = res.body;
          
          this.classService.getMaterialById(this.schedule.material_code).subscribe(res=>{
            this.material=res.body;
            this.kodeMateri = this.material.code;
            this.namaMateri = this.material.name;
            this.tahunMateri = this.material.year;

          })
          staffService.getStaffById(this.schedule.trainer_nip).subscribe(res=>{
            this.trainer = res.body;
          })
        })
        staffService.getAllTrainingStaffsByProgram(this.staff.program).subscribe(res=>{
          this.pesertaTraining = res.body;
          const INQ_PESERTA = res.body;
          this.dataSourceStaff = new MatTableDataSource<Staff>(INQ_PESERTA);
          this.dataSourceStaff.paginator = this.paginator.toArray()[0];
        })
      }
      else if(this.staff.flag_trainer==true){
        this.trainerFlag = true;
        this.traineeFlag = false;
        classService.getScheduleByTrainerNip(this.staff.nip).subscribe(res=>{
          this.schedule = res.body;
          this.classService.getMaterialById(this.schedule.material_code).subscribe(res=>{
            this.material = res.body;
            this.kodeMateri = this.material.code;
            this.namaMateri = this.material.name;
            this.tahunMateri = this.material.year;
          })
          this.staffService.getAllTrainingStaffsByProgram(this.schedule.program_code).subscribe(res=>{
            this.pesertaTraining = res.body;
            const INQ_PESERTA = res.body;
            this.dataSourceStaff = new MatTableDataSource<Staff>(INQ_PESERTA);
            this.dataSourceStaff.paginator = this.paginator.toArray()[0];
          })
        })
      }
      this.classService.getListScheduleByProgram(this.staff.program).subscribe(res=>{
        this.scheduleList = res.body;
        const INQ_SCHEDULE = res.body;
        this.dataSourceSchedule = new MatTableDataSource<Schedule>(INQ_SCHEDULE);
        this.dataSourceSchedule.paginator = this.paginator.toArray()[1];
        console.log(this.scheduleList);
      })
    })
  }

  ngOnInit() {
    this.trainerFlag = false;
    this.traineeFlag = false;
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

  ngAfterViewInit(){
  }
  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      console.log(target.files[0]);
    };
    reader.readAsBinaryString(target.files[0]);
 }
 onUpload(){ 
  const file = new FormData();
  file.append('file', this.selectedFile);
  console.log(file.get("file"));
  this.material.code = this.kodeMateri;
  this.material.name = this.namaMateri;
  this.material.year = this.tahunMateri;
  this.material.url = this.selectedFile.name;
  this.classService.uploadMaterial(file)
  .subscribe(event=>{
      if(event.type === HttpEventType.UploadProgress) {
          console.log('Upload Progress : ' + Math.round(event.loaded / event.total * 100) + '%'); 
      } else if (event.type === HttpEventType.Response) {
          console.log(event);
      }
  });

  this.classService.updateMaterial(this.material).subscribe(res=>{
    console.log(res.body);
  });
  }
  download(url){
    this.classService.downloadMaterial(url).subscribe(res=>{
     var newBlob = new Blob([res], { type: "application" });
     
     // For other browsers: Create a link pointing to the ObjectURL containing the blob.
     const data = window.URL.createObjectURL(newBlob);
 
     var link = document.createElement('a');
     link.href = data;
     link.download = url;
     // this is necessary as link.click() does not work on the latest firefox
     link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
 
     setTimeout(function () {
         // For Firefox it is necessary to delay revoking the ObjectURL
         window.URL.revokeObjectURL(data);
         link.remove();
     }, 100);
    })
  }
}
