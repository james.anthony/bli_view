import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import {   
  MatButtonModule, 
  MatCardModule,
  MatSortModule,
  MatSelectModule, 
  MatIconModule, 
  MatTableModule, 
  MatRadioModule,
  MatDatepickerModule,
  MatPaginatorModule,
  MatExpansionModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatTabsModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { AdminPageRoutingModule } from './admin-page-routing.module';
import { AdminPageComponent } from './admin-page.component';
import { StatModule } from 'src/app/shared/modules/stat/stat.module';
import { MatDialogModule } from '@angular/material';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
@NgModule({
  imports: [
    CommonModule,
    AdminPageRoutingModule,
    MatRadioModule,
    MatInputModule,
    MatNativeDateModule,
    MatDialogModule,
    MatTabsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatSortModule,
    MatGridListModule,
    MatCardModule,
    FlexLayoutModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    StatModule
  ],
  declarations: [AdminPageComponent],
  entryComponents:  [CustomDialogComponent, InfoDialogComponent]
})
export class AdminPageModule { }
