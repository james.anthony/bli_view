import { Component, OnInit, ViewChild, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Staff } from 'src/app/services/staff';
import { Stall } from 'src/app/services/stall';
import { Vendor } from 'src/app/services/vendor';
import { ClassService } from '../../services/class.service';
import { BookingService } from '../../services/booking.service';
import { StaffService } from '../../services/staff.service';
import { Booking } from '../food-stall/booking';
import { Division } from 'src/app/services/division';
import { FoodStallService } from '../../services/food-stall.service';
import { Program } from '../../services/program';
import { Class } from '../../services/class';
import { Material } from '../../services/material';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { CustomDialogComponent } from '../custom-dialog/custom-dialog.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { HttpClient, HttpEventType , HttpHeaders} from '@angular/common/http';

const INQ_BOOK: Booking[] = [];
const INQ_STAFF: Staff[] = [];
const INQ_STALL: Stall[] = [];
const INQ_TRAINER: Staff[] = [];
const INQ_TRAINEE: Staff[] = [];
const INQ_PROGRAM: Program[] = [];
const INQ_CLASS: Class[] = [];

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit, AfterViewInit {

    enroll_khusus_form:FormGroup;
    @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
    @ViewChildren(MatSort) sort = new QueryList<MatSort>();
    create_program_form:FormGroup;
    enroll_vend_khusus_form:FormGroup;
    create_materi_form:FormGroup;
    newForm: FormGroup;
    nip: String;
    loginStaff: Staff;
    displayedColumnsBook = ['booking_id', 'stall_id', 'nip'];
    displayedColumnsStaff = ['name', 'division', 'program'];
    displayedColumnsStall = [ 'stall_id', 'food', 'location'];
    displayedColumnsTrainer = ['nip', 'name', 'division'];
    displayedColumnsTrainee = ['nip', 'name', 'division', 'program'];
    displayedColumnsProgram = ['code', 'start_date', 'end_date'];
    displayedColumnsClass = ['code', 'availability'];
    dataSourceBooking = new MatTableDataSource<Booking>(INQ_BOOK);
    dataSourceStaff = new MatTableDataSource(INQ_STAFF);
  
    dataSourceStall = new MatTableDataSource(INQ_STALL);
    dataSourceTrainer = new MatTableDataSource(INQ_TRAINER);
    dataSourceTrainee = new MatTableDataSource(INQ_TRAINEE);
    dataSourceProgram = new MatTableDataSource(INQ_PROGRAM);
    dataSourceClass = new MatTableDataSource(INQ_CLASS);
    
    allBookArray: Booking[];
    allStaffArray: Staff[];
    allTrainerArray: Staff[];
    allTraineeArray: Staff[];
    allStallArray: Stall[];
    allProgramArray: Program[];
    allClassArray: Class[];

    inqBookFlag: boolean;
    inqStaffFlag: boolean;
    inqVendorFlag: boolean;
    staffFlag: boolean;
    vendorFlag: boolean;
    insertStaff: Staff;
    updateStaff: Staff;
    insertVendor: Vendor;
    insertStall: Stall;
    divisionList: Division[];
    insertProgram: Program;
    insertMateri: Material;
    selectedFile: File = null;

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        
        this.dataSourceBooking.filter = filterValue;
        this.dataSourceStaff.filter = filterValue;
        this.dataSourceTrainer.filter = filterValue;
        this.dataSourceTrainee.filter = filterValue;
        this.dataSourceProgram.filter = filterValue;
        this.dataSourceClass.filter = filterValue;

        this.dataSourceStaff.filterPredicate = function(data, filter: string): boolean{
          return data.nip.toLowerCase().includes(filter) || data.division.name.toLowerCase().includes(filter) || data.name.toLowerCase().includes(filter);
        };
        this.dataSourceTrainer.filterPredicate = function(data, filter: string): boolean{
          return data.nip.toLowerCase().includes(filter) || data.division.name.toLowerCase().includes(filter) || data.name.toLowerCase().includes(filter);
        };
        this.dataSourceTrainee.filterPredicate = function(data, filter: string): boolean{
          return data.nip.toLowerCase().includes(filter) || data.division.name.toLowerCase().includes(filter) || data.name.toLowerCase().includes(filter) || data.program.toLowerCase().includes(filter);
        };
    }
    
    constructor(private router: Router, private formBuilder: FormBuilder, private staffService: StaffService, private stallService: FoodStallService, private classService:ClassService, private bookingService: BookingService, private dialog: MatDialog) {
      this.nip = localStorage.getItem('nip');
      staffService.getStaffById(this.nip).subscribe(res=>{
        this.loginStaff = res.body;
      })
      staffService.getAllDivisions().subscribe(res=>{
        this.divisionList = res.body;
      })
  

      this.insertStaff={nip: '', name: '', dob: null, gender: '', program: '-', role: '', domain: '', division:{id:1, name:"-"}, 
      internal: true, flag_trainee: false, flag_trainer: false, created_at: null, created_by: '', 
      is_deleted: false, updated_at: null, updated_by: null, status: null};
      this.updateStaff=new Staff();
      this.insertVendor={vendor_staff_nip: '', address: '', email: '', phone: '', start_date: null, 
      end_date: null, created_by: '-', updated_by: '-'};
      this.insertStall={stall_id: 0, location: '-', name: '-', description: '-', book_stock: 0, queue_stock: 0, 
      created_by: '-', updated_by: '-', is_deleted: false, food_id: 0, vendor_staff_nip: '-', vendor: new Vendor()};
      this.insertProgram=new Program();
      this.insertMateri=new Material();
      

      this.allBookArray = [];
      bookingService.getListBooking().subscribe(
      res=>{
        this.allBookArray=res.body;
        const INQ_BOOK=res.body;
        this.dataSourceBooking = new MatTableDataSource<Booking>(INQ_BOOK);
        this.dataSourceBooking.sort = this.sort.toArray()[0];
        this.dataSourceBooking.paginator = this.paginator.toArray()[0];
      }
    );

    staffService.getAllStaffs().subscribe(
      res=>{
        this.allStaffArray=res.body;
        const INQ_STAFF=res.body;
        this.dataSourceStaff = new MatTableDataSource(INQ_STAFF);
        this.dataSourceStaff.sort = this.sort.toArray()[1];
        this.dataSourceStaff.paginator = this.paginator.toArray()[1];
      }
    );

    stallService.getInqStalls().subscribe(
      res=>{
        this.allStallArray=res.body;
        const INQ_STALL=res.body;
        this.dataSourceStall = new MatTableDataSource(INQ_STALL);
        this.dataSourceStall.sort = this.sort.toArray()[2];
        this.dataSourceStall.paginator = this.paginator.toArray()[2];
      }
    );

    staffService.getAllTrainers().subscribe(
      res=>{
        this.allTrainerArray=res.body;
        const INQ_TRAINER=res.body;
        this.dataSourceTrainer = new MatTableDataSource(INQ_TRAINER);
        this.dataSourceTrainer.sort = this.sort.toArray()[3];
        this.dataSourceTrainer.paginator = this.paginator.toArray()[3];
        
      }
    );

    staffService.getAllTrainees().subscribe(
      res=>{
        this.allTraineeArray=res.body;
        const INQ_TRAINEE=res.body;
        this.dataSourceTrainee = new MatTableDataSource(INQ_TRAINEE);
        this.dataSourceTrainee.sort = this.sort.toArray()[4];
        this.dataSourceTrainee.paginator = this.paginator.toArray()[4];
      }
    );

    stallService.getInqStalls().subscribe(
      res=>{
        this.allStallArray=res.body;
        const INQ_STALL=res.body;
        this.dataSourceStall = new MatTableDataSource<Stall>(INQ_STALL);
        this.dataSourceStall.sort = this.sort.toArray()[4];
        this.dataSourceStall.paginator = this.paginator.toArray()[4];
      }
    );

    classService.getListProgram().subscribe(
      res=>{
        this.allProgramArray=res.body;
        const INQ_PROGRAM=res.body;
        this.dataSourceProgram = new MatTableDataSource<Program>(INQ_PROGRAM);
        this.dataSourceProgram.sort = this.sort.toArray()[5];
        this.dataSourceProgram.paginator = this.paginator.toArray()[5];
      }
    );

    classService.getListAvailableClass().subscribe(
      res=>{
        res.body.forEach(element => {
          INQ_CLASS.push(element);
        });
        classService.getListUnavailableClass().subscribe(
          res=>{
            res.body.forEach(element => {
              INQ_CLASS.push(element);

              this.dataSourceClass = new MatTableDataSource<Class>(INQ_CLASS);
              this.dataSourceClass.sort = this.sort.toArray()[6];
              this.dataSourceClass.paginator = this.paginator.toArray()[6];
            });
          }
        )        
      }
    );
  }

  ngOnInit() {
    this.inqBookFlag = false;
    this.inqStaffFlag = false;
    this.inqVendorFlag = false;
    this.staffFlag = false;
    this.vendorFlag = false;

    

    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }

    this.newForm = new FormGroup({
          nip: new FormControl('', [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(6),
          Validators.pattern("^[0-9]*$")
      ]),
        name: new FormControl('', [
        Validators.required
      ]),
        dob: new FormControl(''),
        gender: new FormControl(''),
        divisi: new FormControl('', [
          Validators.required
        ]),
        role: new FormControl()
  });

    this.enroll_khusus_form = new FormGroup({
      det_program: new FormControl('', [
      Validators.required,
    ]),
      det_status: new FormControl('', [
      Validators.required,
    ]),
  });

    this.enroll_vend_khusus_form = new FormGroup({
      det_addr: new FormControl('', [
      Validators.required,
    ]),
      det_email: new FormControl('', [
      Validators.required,
    ]),
      det_phone: new FormControl('', [
      Validators.required,
    ]),
      det_lokasi: new FormControl('', [
      Validators.required,
    ]),
  });
    this.create_program_form = new FormGroup({
      prog_nama: new FormControl('', [
      Validators.required,
    ]),
      prog_desc: new FormControl('', [
      Validators.required,
    ]),
      prog_start: new FormControl('', [
      Validators.required,
    ]),
      prog_end: new FormControl('', [
      Validators.required,
    ]),
  });

  this.create_materi_form = new FormGroup({
    materi_nama: new FormControl('', [
    Validators.required,
  ]),
    materi_code: new FormControl('', [
    Validators.required,
  ]),
    materi_tahun: new FormControl('')
});
}

ngAfterViewInit(){
//  this.dataPageBook.paginator = this.paginator.toArray()[0];
}

  onInqDataBook() {
    this.inqBookFlag = !this.inqBookFlag;
    this.inqStaffFlag = false;
    this.inqVendorFlag = false;
  }
  onInqDataStaff() {
    this.inqStaffFlag = !this.inqStaffFlag;
    this.inqBookFlag = false;
    this.inqVendorFlag = false;
  }  
  onInqDataVendor() {
    this.inqVendorFlag = !this.inqVendorFlag;
    this.inqBookFlag = false;
    this.inqStaffFlag = false;
  }

  onSubmitUmum(insertStaff){
    this.insertStaff.nip = insertStaff.nip;
    this.insertStaff.name = insertStaff.name;
    this.insertStaff.dob = insertStaff.dob;
    this.insertStaff.gender = insertStaff.gender;
    this.insertStaff.role = insertStaff.role;
    this.insertStaff.division = insertStaff.divisi;
    this.insertStaff.created_by = this.loginStaff.name;
    
    this.staffService.createStaff(this.insertStaff).subscribe(res=>{
      console.log(res.status);
      if(res.status == 200 || res.status == 201)
      {
        this.infoDialog("Staff telah berhasil di daftarkan, lanjutkan proses detail khusus");
      }
      else if(res.status == 500){
        this.infoDialog("Staff gagal didaftarkan");
      }
      console.log(res.body);
    });
    if(insertStaff.role === '3')
    {
      this.staffFlag = true;
      this.vendorFlag = false;
    }
    else if(insertStaff.role === '2')
    {
      this.staffFlag = false;
      this.vendorFlag = true;
    }
    
  }

  onSubmitProgram(insertProgram){
    this.insertProgram.code = insertProgram.prog_nama;
    this.insertProgram.description = insertProgram.prog_desc;
    this.insertProgram.start_date = insertProgram.prog_start;
    this.insertProgram.end_date = insertProgram.prog_end;
    this.insertProgram.is_deleted = false;
    this.classService.createProgram(this.insertProgram).subscribe(res=>{
      console.log(res.status);
      if(res.status == 200 || res.status == 201)
      {
        this.infoDialog("Program berhasil dibuat");
        window.location.reload();
      }
      else
      {
        this.infoDialog("Program gagal dibuat silahkan coba kembali");
      }
    });
  }

    onSubmitCreateMateri(insertMateri){
      this.insertMateri.code = insertMateri.materi_code;
      this.insertMateri.name = insertMateri.materi_name;
      this.insertMateri.year = insertMateri.materi_tahun;
      this.insertMateri.url = this.selectedFile.name;
      
      this.classService.createMaterial(this.insertMateri).subscribe(res=>{
        console.log(res.status);
        if(res.status == 200 || res.status == 201){
          this.infoDialog("Materi berhasil dibuat");
          window.location.reload();
        }
        else{
          this.infoDialog("Materi gagal dibuat silahkan coba kembali");
        }
      });
      this.onUpload();
    }

  onSubmitDetailStaff(updateStaff){
    this.updateStaff = this.insertStaff;
    this.updateStaff.program = updateStaff.det_program;
    if(updateStaff.det_status=='true')this.updateStaff.internal = true;
    else if(updateStaff.det_status=='false')this.updateStaff.internal = false;
    this.updateStaff.updated_by = this.loginStaff.name;
    this.staffService.updateStaff(this.updateStaff).subscribe(res=>{
      console.log(res.status);
      if(res.status == 200 || res.status == 201){
        this.infoDialog("Detail khusus staff telah berhasil ditambahkan");
        window.location.reload();
      }
      else{
        this.infoDialog("Detail khusus staff gagal ditambahkan silahkan coba kembali");
      }
    });;
    
  }

  onSubmitDetailVendor(insertVendor){
    this.insertVendor.vendor_staff_nip = this.insertStaff.nip;
    this.insertVendor.address = insertVendor.det_addr;
    this.insertVendor.email = insertVendor.det_email;
    this.insertVendor.phone = insertVendor.det_phone;
    this.insertStall.vendor_staff_nip = this.insertStaff.nip;
    this.insertStall.location = insertVendor.det_lokasi;
    console.log(this.insertVendor);
    console.log(this.insertStaff);
    this.stallService.createVendor(this.insertVendor).subscribe();
    this.stallService.createStall(this.insertStall).subscribe();
  }

  //---------------------double-click---------------------//
  private touchTime = 0;
  doubleClick(data, num){

    if (this.touchTime == 0) {
        this.touchTime = new Date().getTime();
    } else {
        if (((new Date().getTime()) - this.touchTime) < 800) {
          
          //edit here
          if(num == 1){
            this.router.navigate(['/booking-detail', data.booking_id, data.stall_id, data.nip]);
          }else if(num == 2){
            this.router.navigate(['/staff-detail', data.nip]);
          }else if(num == 3){
            this.router.navigate(['/vendor-detail', data.stall_id]);
          }else if(num == 4){
            this.router.navigate(['/program-detail', data.code]);
          }else if(num == 5){
            this.router.navigate(['/schedule-detail', data.code, data.availability]);
          }
          //edit end here

            this.touchTime = 0;
        } else {
            this.touchTime = new Date().getTime();
        }
    }
  }

  openDialog(bookingData) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: bookingData
    };
    
    const dialogRef = this.dialog.open(CustomDialogComponent, dialogConfig);
  }
  
  infoDialog(text) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        data: text
    };
    
    const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);  
  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      console.log(target.files[0]);
    };
    reader.readAsBinaryString(target.files[0]);
 }
 onUpload(){ 
  const file = new FormData();
  file.append('file', this.selectedFile);
  console.log(file.get("file"));
  
  this.classService.uploadMaterial(file)
  .subscribe(event=>{
      if(event.type === HttpEventType.UploadProgress) {
          console.log('Upload Progress : ' + Math.round(event.loaded / event.total * 100) + '%'); 
      } else if (event.type === HttpEventType.Response) {
          console.log(event);
      }
  });
  
}
}