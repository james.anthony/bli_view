import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StaffService } from 'src/app/services/staff.service';
import { FoodStallService } from 'src/app/services/food-stall.service';
import { BookingService } from 'src/app/services/booking.service';
import { Staff } from 'src/app/services/staff';
import { Stall } from 'src/app/services/stall';
import { Booking } from '../food-stall/booking';

export class Model{
  booking_id: string;
  nip: string;
  name: string;
  program: string;
  stall_id: string;
  stall_name: string;
  stall_content: string;
  booking_time: string;
  rating: string;
}

@Component({
  selector: 'app-booking-detail',
  templateUrl: './booking-detail.component.html',
  styleUrls: ['./booking-detail.component.scss']
})

export class BookingDetailComponent implements OnInit {

  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;
  staff:Staff;
  stall:Stall;
  booking:Booking;
  constructor(private route: ActivatedRoute, private staffService:StaffService, private stallService:FoodStallService, private bookingService:BookingService) {
    this.route.params.subscribe( params => {
      staffService.getStaffById(params.nip).subscribe(res=>{
        this.staff = res.body;
        console.log(this.staff);
      })
      stallService.getOneStallById(params.stall_id).subscribe(res=>{
        this.stall = res.body;
        console.log(this.stall);
      })
      bookingService.getBookingById(params.booking_id).subscribe(res=>{
        this.booking = res.body;
        console.log(this.booking);
      })
      console.log(params);
    });
      
  }

  ngOnInit() {
    let top = document.getElementById('top');
    if (top !== null) {
    top.scrollIntoView();
    top = null;
    }
  }

  noEmpty(arr){
    for(var i = 0; i<arr.length; i++){
      if(arr[i] == "" || arr[i] == null){
        arr[i] = "-";
      }
    }
  }

  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
    }
  }
  leftFunction(){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
    }
  }

}
