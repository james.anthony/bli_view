import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    showMenu: string = '';
    parent_active: boolean = false;
    first_active: boolean = false;
    upper_active: boolean = false;
    lower_active: boolean = false;
    user_role: String;
    constructor() {
        this.user_role = localStorage.getItem('role');
    }

    ngOnInit() {}

    addExpandClass(element: any) {
        if (element === this.showMenu 
            || this.showMenu === 'subpages1' 
            || this.showMenu === 'subpages2'
            || this.showMenu === 'subpages3') {
            this.showMenu = '0';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        } else {
            this.showMenu = element;
            this.parent_active = true;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        }
    }
    addExpandSubClass_first(element: any) {
        if (element === this.showMenu) {
            this.showMenu = 'pages';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        } else {
            this.showMenu = element;
            this.parent_active = false;
            this.first_active = true;
            this.upper_active = false;
            this.lower_active = false;
        }
    }
    addExpandSubClass_upper(element: any) {
        if (element === this.showMenu) {
            this.showMenu = 'pages';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        } else {
            this.showMenu = element;
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = true;
            this.lower_active = false;
        }
    }
    addExpandSubClass_lower(element: any) {
        if (element === this.showMenu) {
            this.showMenu = 'pages';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        } else {
            this.showMenu = element;
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = true;
        }
    }
}