import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Booking } from '../layout/food-stall/booking';
import * as jwt_decode from "jwt-decode";
import { StaffService } from '../services/staff.service';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    
    constructor(private router: Router, private formBuilder: FormBuilder, 
        private loginService: LoginService, private staffService: StaffService) {}
    newForm: FormGroup;
    NIP: String;
    headers : String[];
    role: String;

    ngOnInit() {
        
        this.newForm = new FormGroup({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(6)
            ]),
            password: new FormControl('', [
                Validators.required
            ]),
        });
    }

    onLogin(newForm) {
        this.loginService.login(newForm.value).subscribe(res=>{
            const keys = res.headers.keys();
            this.headers = keys.map(key => `${key}: ${res.headers.get(key)}`);
            let tokenInfo = this.getDecodedAccessToken(res.headers.get('Authorization'));
            localStorage.setItem('token', res.headers.get('Authorization'));
            localStorage.setItem('nip', tokenInfo.sub);
            this.NIP = tokenInfo.sub;
            this.role = tokenInfo.authorities[0];
            this.staffService.getStaffById(this.NIP).subscribe();
            localStorage.setItem('role', this.role.toString());
            if(this.role == "ROLE_SUPERADMIN")
            {
                localStorage.setItem('isLoggedin', 'true');
                this.router.navigate(['/admin-page']);
            }
            else if(this.role == "ROLE_ADMIN")
            {
                localStorage.setItem('isLoggedin', 'true');
                this.router.navigate(['/admin-page']);
            }
            else if(this.role == "ROLE_STAFF")
            {
                localStorage.setItem('isLoggedin', 'true');
                this.router.navigate(['/dashboard']);
            }
            else if(this.role == "ROLE_VENDOR")
            {
                localStorage.setItem('isLoggedin', 'true');
                this.router.navigate(['/dashboard-vendor']);
            }
        });
    }

    getDecodedAccessToken(token: String): any{
        try{
            return jwt_decode(token);
        }
        catch(Error){
            return null;
        }
    }
}