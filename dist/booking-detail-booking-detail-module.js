(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["booking-detail-booking-detail-module"],{

/***/ "./src/app/layout/booking-detail/booking-detail-routing.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/layout/booking-detail/booking-detail-routing.module.ts ***!
  \************************************************************************/
/*! exports provided: BookingDetailRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingDetailRoutingModule", function() { return BookingDetailRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _booking_detail_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./booking-detail.component */ "./src/app/layout/booking-detail/booking-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _booking_detail_component__WEBPACK_IMPORTED_MODULE_2__["BookingDetailComponent"]
    }
];
var BookingDetailRoutingModule = /** @class */ (function () {
    function BookingDetailRoutingModule() {
    }
    BookingDetailRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BookingDetailRoutingModule);
    return BookingDetailRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/booking-detail/booking-detail.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/layout/booking-detail/booking-detail.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"top\"></div>\r\n\r\n<div class=\"container\">\r\n    <h2>Detail Booking</h2>\r\n  \r\n      <div class=\"container-content\">\r\n        <table>\r\n          <tr>\r\n            <td>Booking ID</td>\r\n            <td>:</td>\r\n            <td>{{booking?.booking_id}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Booking Time</td>\r\n            <td>:</td>\r\n            <td>{{booking?.booking_time | date:'medium'}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>NIP</td>\r\n            <td>:</td>\r\n            <td>{{staff?.nip}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Name</td>\r\n            <td>:</td>\r\n            <td>{{staff?.name | titlecase}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Program</td>\r\n            <td>:</td>\r\n            <td>{{staff?.program}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Stall ID</td>\r\n            <td>:</td>\r\n            <td>{{stall?.stall_id}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Stall Location</td>\r\n            <td>:</td>\r\n            <td>{{stall?.food.name | titlecase}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Stall Food</td>\r\n            <td>:</td>\r\n            <td>{{stall?.food.description | titlecase}}</td>\r\n          </tr>\r\n        </table>\r\n      </div>\r\n  \r\n    <div class=\"btn-area\">\r\n        <button mat-raised-button class=\"back-btn\" (click)=\"rightFunction()\">\r\n            <mat-icon>{{rightIcon}}</mat-icon>\r\n        </button>\r\n        <button mat-raised-button class=\"back-btn\" (click)=\"leftFunction()\" disabled>\r\n            <mat-icon>{{leftIcon}}</mat-icon>\r\n        </button>\r\n      </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/layout/booking-detail/booking-detail.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/layout/booking-detail/booking-detail.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#top {\n  height: 20px;\n  width: 100%; }\n\n.container {\n  position: relative;\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12);\n  background-color: white; }\n\n.container h2 {\n  margin: 0;\n  padding: 20px;\n  border-bottom: 1px solid lightgray; }\n\n.container-content {\n  padding: 10px; }\n\n.btn-area {\n  position: absolute;\n  top: 15px;\n  right: 10px; }\n\n.back-btn {\n  float: right;\n  background-color: #3f51b5;\n  color: white; }\n\n.back-btn:first-child {\n  margin-left: 5px; }\n\ntable {\n  width: 100%; }\n\ntable tr {\n  vertical-align: baseline; }\n\ntable tr td {\n  padding-bottom: 5px; }\n\ntable tr td:first-child {\n  color: grey; }\n\ntable tr td:last-child {\n  max-width: 400px; }\n\n@media screen and (max-width: 600px) {\n  .back-btn {\n    border-radius: 100%;\n    min-width: unset;\n    padding: 0;\n    width: 36px; }\n  table {\n    margin-bottom: 10px; }\n  table tr td:first-child {\n    min-width: 120px; } }\n"

/***/ }),

/***/ "./src/app/layout/booking-detail/booking-detail.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/booking-detail/booking-detail.component.ts ***!
  \*******************************************************************/
/*! exports provided: Model, BookingDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Model", function() { return Model; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingDetailComponent", function() { return BookingDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/staff.service */ "./src/app/services/staff.service.ts");
/* harmony import */ var src_app_services_food_stall_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/food-stall.service */ "./src/app/services/food-stall.service.ts");
/* harmony import */ var src_app_services_booking_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/booking.service */ "./src/app/services/booking.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Model = /** @class */ (function () {
    function Model() {
    }
    return Model;
}());

var BookingDetailComponent = /** @class */ (function () {
    function BookingDetailComponent(route, staffService, stallService, bookingService) {
        var _this = this;
        this.route = route;
        this.staffService = staffService;
        this.stallService = stallService;
        this.bookingService = bookingService;
        this.rightIcon = "chevron_left";
        this.leftIcon = "create";
        this.flag = true;
        this.route.params.subscribe(function (params) {
            staffService.getStaffById(params.nip).subscribe(function (res) {
                _this.staff = res.body;
                console.log(_this.staff);
            });
            stallService.getOneStallById(params.stall_id).subscribe(function (res) {
                _this.stall = res.body;
                console.log(_this.stall);
            });
            bookingService.getBookingById(params.booking_id).subscribe(function (res) {
                _this.booking = res.body;
                console.log(_this.booking);
            });
            console.log(params);
        });
    }
    BookingDetailComponent.prototype.ngOnInit = function () {
        var top = document.getElementById('top');
        if (top !== null) {
            top.scrollIntoView();
            top = null;
        }
    };
    BookingDetailComponent.prototype.noEmpty = function (arr) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == "" || arr[i] == null) {
                arr[i] = "-";
            }
        }
    };
    BookingDetailComponent.prototype.rightFunction = function () {
        if (this.flag) {
            window.history.back();
        }
        else {
            this.rightIcon = "chevron_left";
            this.leftIcon = "create";
            this.flag = true;
        }
    };
    BookingDetailComponent.prototype.leftFunction = function () {
        if (this.flag) {
            this.flag = false;
            this.leftIcon = "check";
            this.rightIcon = "close";
        }
    };
    BookingDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-booking-detail',
            template: __webpack_require__(/*! ./booking-detail.component.html */ "./src/app/layout/booking-detail/booking-detail.component.html"),
            styles: [__webpack_require__(/*! ./booking-detail.component.scss */ "./src/app/layout/booking-detail/booking-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_2__["StaffService"], src_app_services_food_stall_service__WEBPACK_IMPORTED_MODULE_3__["FoodStallService"], src_app_services_booking_service__WEBPACK_IMPORTED_MODULE_4__["BookingService"]])
    ], BookingDetailComponent);
    return BookingDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/booking-detail/booking-detail.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/booking-detail/booking-detail.module.ts ***!
  \****************************************************************/
/*! exports provided: BookingDetailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingDetailModule", function() { return BookingDetailModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _booking_detail_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./booking-detail-routing.module */ "./src/app/layout/booking-detail/booking-detail-routing.module.ts");
/* harmony import */ var _booking_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./booking-detail.component */ "./src/app/layout/booking-detail/booking-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var BookingDetailModule = /** @class */ (function () {
    function BookingDetailModule() {
    }
    BookingDetailModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _booking_detail_routing_module__WEBPACK_IMPORTED_MODULE_3__["BookingDetailRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"]
            ],
            declarations: [_booking_detail_component__WEBPACK_IMPORTED_MODULE_4__["BookingDetailComponent"]]
        })
    ], BookingDetailModule);
    return BookingDetailModule;
}());



/***/ }),

/***/ "./src/app/services/staff.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/staff.service.ts ***!
  \*******************************************/
/*! exports provided: StaffService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffService", function() { return StaffService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StaffService = /** @class */ (function () {
    function StaffService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'staff/';
        this.token = localStorage.getItem('token');
    }
    StaffService.prototype.getAllStaffs = function () {
        return this.http.get(this.url + 'all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainingStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/training/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainees = function () {
        return this.http.get(this.url + 'trainees', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainers = function () {
        return this.http.get(this.url + 'trainers', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllDivisions = function () {
        return this.http.get(this.url + 'division', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getStaffById = function (nip) {
        return this.http.get(this.url + 'detail/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.createStaff = function (staff) {
        console.log(staff);
        return this.http.post(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.updateStaff = function (staff) {
        console.log(staff);
        return this.http.put(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.deleteStaff = function (nip) {
        return this.http.delete(this.url + 'delete/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainingBadge = function (nip) {
        return this.http.get(this.url + 'training/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainerBadge = function (nip) {
        return this.http.get(this.url + 'trainer/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTrainerToday = function () {
        return this.http.get(this.url + 'trainer/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTraineeToday = function () {
        return this.http.get(this.url + 'trainee/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StaffService);
    return StaffService;
}());



/***/ })

}]);
//# sourceMappingURL=booking-detail-booking-detail-module.js.map