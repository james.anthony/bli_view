(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["staff-detail-staff-detail-module"],{

/***/ "./src/app/error/show-errors.component.ts":
/*!************************************************!*\
  !*** ./src/app/error/show-errors.component.ts ***!
  \************************************************/
/*! exports provided: ShowErrorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowErrorsComponent", function() { return ShowErrorsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShowErrorsComponent = /** @class */ (function () {
    function ShowErrorsComponent() {
    }
    ShowErrorsComponent_1 = ShowErrorsComponent;
    ShowErrorsComponent.prototype.shouldShowErrors = function () {
        return this.control &&
            this.control.errors &&
            (this.control.dirty || this.control.touched);
    };
    ShowErrorsComponent.prototype.listOfErrors = function () {
        var _this = this;
        return Object.keys(this.control.errors)
            .map(function (field) { return _this.getMessage(field, _this.control.errors[field]); });
    };
    ShowErrorsComponent.prototype.getMessage = function (type, params) {
        return ShowErrorsComponent_1.errorMessages[type](params);
    };
    ShowErrorsComponent.errorMessages = {
        'required': function () { return 'This field is required'; },
        'minlength': function (params) { return 'The min number of characters is ' + params.requiredLength; },
        'maxlength': function (params) { return 'The max allowed number of characters is ' + params.requiredLength; },
        'ipaddress': function (params) { return params.message; },
        'pattern': function (params) { return 'Numbers only'; }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ShowErrorsComponent.prototype, "control", void 0);
    ShowErrorsComponent = ShowErrorsComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-show-errors',
            template: "\n        <div *ngIf=\"shouldShowErrors()\">\n            <p style=\"margin-left:10px; font-size:0.8em; margin-top:-10px;\" *ngFor=\"let error of listOfErrors()\">{{error}}</p>\n        </div>\n    "
        })
    ], ShowErrorsComponent);
    return ShowErrorsComponent;
    var ShowErrorsComponent_1;
}());



/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title *ngIf=\"listData.data.location != '100'\">Booking {{listData.data.food.description | titlecase}}?</h2>\r\n<h2 mat-dialog-title *ngIf=\"listData.data.location == '100'\">Booking Prasmanan?</h2>\r\n\r\n<mat-dialog-actions>\r\n    <button class=\"mat-raised-button\"(click)=\"close()\">No</button>\r\n    <button class=\"mat-raised-button mat-primary\"(click)=\"save()\">Yes</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n"

/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.ts ***!
  \*****************************************************************/
/*! exports provided: CustomDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomDialogComponent", function() { return CustomDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var CustomDialogComponent = /** @class */ (function () {
    function CustomDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.listData = data;
    }
    CustomDialogComponent.prototype.ngOnInit = function () {
    };
    CustomDialogComponent.prototype.save = function () {
        this.dialogRef.close(this.listData);
    };
    CustomDialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    CustomDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-custom-dialog',
            template: __webpack_require__(/*! ./custom-dialog.component.html */ "./src/app/layout/custom-dialog/custom-dialog.component.html"),
            styles: [__webpack_require__(/*! ./custom-dialog.component.scss */ "./src/app/layout/custom-dialog/custom-dialog.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], CustomDialogComponent);
    return CustomDialogComponent;
}());



/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.html":
/*!***************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>{{info.data}}</h2>\r\n\r\n<mat-dialog-actions>\r\n    <button class=\"mat-raised-button mat-primary\"(click)=\"close()\">Ok</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n"

/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.ts ***!
  \*************************************************************/
/*! exports provided: InfoDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoDialogComponent", function() { return InfoDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var InfoDialogComponent = /** @class */ (function () {
    function InfoDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.info = data;
        console.log(this.info);
    }
    InfoDialogComponent.prototype.ngOnInit = function () {
    };
    InfoDialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    InfoDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-info-dialog',
            template: __webpack_require__(/*! ./info-dialog.component.html */ "./src/app/layout/info-dialog/info-dialog.component.html"),
            styles: [__webpack_require__(/*! ./info-dialog.component.scss */ "./src/app/layout/info-dialog/info-dialog.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], InfoDialogComponent);
    return InfoDialogComponent;
}());



/***/ }),

/***/ "./src/app/layout/staff-detail/staff-detail-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/layout/staff-detail/staff-detail-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: StaffDetailRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffDetailRoutingModule", function() { return StaffDetailRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _staff_detail_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./staff-detail.component */ "./src/app/layout/staff-detail/staff-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _staff_detail_component__WEBPACK_IMPORTED_MODULE_2__["StaffDetailComponent"]
    }
];
var StaffDetailRoutingModule = /** @class */ (function () {
    function StaffDetailRoutingModule() {
    }
    StaffDetailRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], StaffDetailRoutingModule);
    return StaffDetailRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/staff-detail/staff-detail.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/layout/staff-detail/staff-detail.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"top\"></div>\r\n<form class=\"form\" [formGroup]=\"updateStaff\">\r\n<div class=\"container\">\r\n    <div class=\"header\">\r\n      <h2>Detail Staff</h2>\r\n      <div>\r\n        <button mat-raised-button class=\"bli-btn bli-btn-responsive\" (click)=\"leftFunction(updateStaff.value)\" id=\"btnLeft\">\r\n            <mat-icon>{{leftIcon}}</mat-icon>\r\n        </button>\r\n        <button mat-raised-button class=\"bli-btn bli-btn-responsive\" (click)=\"rightFunction($this)\" id=\"btnRight\">\r\n            <mat-icon>{{rightIcon}}</mat-icon>\r\n        </button>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-content\">\r\n      <table class=\"staff-table\">\r\n        <tbody>\r\n          <tr>\r\n            <td>NIP</td>\r\n            <td>:</td>\r\n            <td>{{staff?.nip}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Name</td>\r\n            <td>:</td>\r\n            <td>{{staff?.name | titlecase}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Gender</td>\r\n            <td>:</td>\r\n            <td>{{staff?.gender | titlecase}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>DOB</td>\r\n            <td>:</td>\r\n            <td>{{staff?.dob}}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>Status</td>\r\n            <td>:</td>\r\n            <td>\r\n              <mat-form-field>\r\n                <mat-select [ngModel]=\"staff?.status\" formControlName=\"flag_status\"\r\n                class=\"form-control\" [disabled]=\"flag\">\r\n                  <mat-option value=\"None\">-None-</mat-option>\r\n                  <mat-option value=\"Trainee\">Trainee</mat-option>\r\n                  <mat-option value=\"Trainer\">Trainer</mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td>Program</td>\r\n            <td>:</td>\r\n            <td>\r\n                <mat-form-field>\r\n                  <mat-select [ngModel]=\"staff?.program\" [disabled]=\"flag\" formControlName=\"program\"\r\n                  class=\"form-control\">\r\n                    <mat-option *ngFor=\"let prog of program\" [value]=\"prog.code\">{{prog.code}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n            </td>\r\n          </tr>\r\n          <tr style=\"display:none;\">\r\n            <td>Division</td>\r\n            <td>:</td>\r\n            <td>\r\n              <mat-form-field>\r\n                <mat-select [ngModel]=\"staff?.division\" [disabled]=\"flag\" formControlName=\"division\"\r\n                class=\"form-control\">\r\n                  <mat-option *ngFor=\"let div of division\" [value]=\"div\">{{div.name}}</mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td>Source</td>\r\n            <td>:</td>\r\n            <td>{{staff?.internal===true?'Internal':'External'}}</td>\r\n          </tr>\r\n          <tr *ngIf=\"staff?.flag_trainer==true\" >\r\n            <td>Kompetensi</td>\r\n            <td>:</td>\r\n            <td><p *ngFor=\"let com of competency\">{{com.material_code}}</p></td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n</div>\r\n</form>\r\n\r\n<div class=\"container\" *ngIf=\"staff?.flag_trainer==true\">\r\n  <div class=\"header\">\r\n    <h2>Tambahkan Kompetensi</h2>\r\n  </div>\r\n  <div class=\"container-content\">\r\n    <table>\r\n      <tr>\r\n        <td>Materi</td>\r\n        <td>:</td>\r\n        <td>\r\n          <mat-form-field>\r\n            <mat-select [(ngModel)]=\"material_code\">\r\n              <mat-option *ngFor=\"let select of material\" [value]=\"select.code\">{{select.name | titlecase}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </td>\r\n      </tr>\r\n    </table>\r\n    \r\n    <div style=\"text-align:right\">\r\n      <button mat-raised-button class=\"bli-btn\" (click)=\"addCompetency(material_code)\">Tambahkan</button>\r\n    </div>\r\n  </div>  \r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/staff-detail/staff-detail.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/layout/staff-detail/staff-detail.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#top {\n  height: 20px;\n  width: 100%; }\n\n.container {\n  position: relative;\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12);\n  background-color: white;\n  vertical-align: top; }\n\n.header {\n  border-bottom: 1px solid lightgray;\n  display: flex;\n  align-items: center;\n  padding: 20px;\n  justify-content: space-between; }\n\n.container:last-child {\n  margin-top: 10px; }\n\n.container h2 {\n  margin: 0; }\n\n.container-content {\n  padding: 20px; }\n\n.container:last-child .container-content {\n  padding-top: 0; }\n\n.btn-area {\n  position: absolute;\n  top: 15px;\n  right: 10px; }\n\n.back-btn:first-child {\n  margin-left: 5px; }\n\ntable {\n  width: 100%; }\n\ntable tr td {\n  padding-bottom: 5px; }\n\ntable tr td:first-child {\n  width: 20%; }\n\n.btn-active-red {\n  background-color: red; }\n\n.btn-active-green {\n  background-color: green; }\n\n.bli-btn {\n  background-color: #3f51b5;\n  color: white;\n  margin-left: 5px; }\n\nmat-form-field {\n  width: 95%; }\n\n@media screen and (max-width: 600px) {\n  .bli-btn-responsive {\n    border-radius: 100%;\n    min-width: unset;\n    padding: 0;\n    width: 36px; }\n  .container {\n    width: 100%; }\n  .container-content {\n    padding: 10px; }\n  mat-form-field {\n    width: 80%; } }\n"

/***/ }),

/***/ "./src/app/layout/staff-detail/staff-detail.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/layout/staff-detail/staff-detail.component.ts ***!
  \***************************************************************/
/*! exports provided: StaffDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffDetailComponent", function() { return StaffDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/staff.service */ "./src/app/services/staff.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_class_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/class.service */ "./src/app/services/class.service.ts");
/* harmony import */ var src_app_services_competency__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/competency */ "./src/app/services/competency.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../custom-dialog/custom-dialog.component */ "./src/app/layout/custom-dialog/custom-dialog.component.ts");
/* harmony import */ var _info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../info-dialog/info-dialog.component */ "./src/app/layout/info-dialog/info-dialog.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var StaffDetailComponent = /** @class */ (function () {
    function StaffDetailComponent(router, route, formBuilder, dialog, staffService, classService) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.dialog = dialog;
        this.staffService = staffService;
        this.classService = classService;
        this.rightIcon = "chevron_left";
        this.leftIcon = "create";
        this.flag = true;
        this.editable = false;
        this.newCompetency = new src_app_services_competency__WEBPACK_IMPORTED_MODULE_4__["Competency"]();
        this.route.params.subscribe(function (params) {
            _this.nip = params.nip;
            _this.staffService.getStaffById(_this.nip).subscribe(function (res) {
                _this.staff = res.body;
                if (_this.staff.flag_trainer == true)
                    _this.staff.status = 'Trainer';
                else if (_this.staff.flag_trainee == true)
                    _this.staff.status = 'Trainee';
                else
                    _this.staff.status = 'None';
                console.log(_this.staff);
                if (_this.staff.flag_trainer == true) {
                    _this.classService.getCompetencyByTrainerNip(_this.nip).subscribe(function (res) {
                        _this.competency = res.body;
                        console.log(_this.competency);
                    });
                }
            });
        });
        this.classService.getListProgram().subscribe(function (res) {
            _this.program = res.body;
        });
        this.classService.getListMaterial().subscribe(function (res) {
            _this.material = res.body;
        });
        this.staffService.getAllDivisions().subscribe(function (res) {
            _this.division = res.body;
        });
    }
    StaffDetailComponent.prototype.ngOnInit = function () {
        this.updateStaff = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            flag_status: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](),
            program: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](),
            division: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]()
        });
        var top = document.getElementById('top');
        if (top !== null) {
            top.scrollIntoView();
            top = null;
        }
    };
    StaffDetailComponent.prototype.toggleClass = function () {
        document.getElementById("btnLeft").classList.toggle('btn-active-green');
        document.getElementById("btnRight").classList.toggle('btn-active-red');
    };
    StaffDetailComponent.prototype.rightFunction = function () {
        if (this.flag) {
            window.history.back();
        }
        else {
            this.rightIcon = "chevron_left";
            this.leftIcon = "create";
            this.flag = true;
            this.toggleClass();
        }
    };
    StaffDetailComponent.prototype.leftFunction = function (staff) {
        var _this = this;
        if (this.flag) {
            this.flag = false;
            this.leftIcon = "check";
            this.rightIcon = "close";
            this.toggleClass();
        }
        else {
            this.rightIcon = "chevron_left";
            this.leftIcon = "create";
            this.flag = true;
            this.staff.program = staff.program;
            this.staff.division = staff.division;
            if (staff.flag_status == 'Trainee') {
                this.staff.flag_trainee = true;
                this.staff.flag_trainer = false;
            }
            else if (staff.flag_status == 'Trainer') {
                this.staff.flag_trainee = false;
                this.staff.flag_trainer = true;
            }
            else {
                this.staff.flag_trainee = false;
                this.staff.flag_trainer = false;
            }
            this.staffService.updateStaff(this.staff).subscribe(function (res) {
                console.log(res.status);
                if (res.status == 200 || res.status == 201) {
                    _this.infoDialog("Detail staff telah berhasil diubah");
                    _this.router.navigate(['/admin-page']);
                }
                else {
                    _this.infoDialog("Detail staff gagal diubah. Silahkan mencoba kembali");
                }
            });
        }
    };
    StaffDetailComponent.prototype.addCompetency = function (material_code) {
        this.newCompetency.trainer_nip = this.staff.nip;
        this.newCompetency.material_code = material_code;
        this.classService.createCompetency(this.newCompetency).subscribe(function (res) {
            console.log(res.status);
        });
    };
    StaffDetailComponent.prototype.openDialog = function (bookingData) {
        var dialogConfig = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogConfig"]();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
            data: bookingData
        };
        var dialogRef = this.dialog.open(_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_6__["CustomDialogComponent"], dialogConfig);
    };
    StaffDetailComponent.prototype.infoDialog = function (text) {
        var dialogConfig = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogConfig"]();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
            data: text
        };
        var dialogRef = this.dialog.open(_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_7__["InfoDialogComponent"], dialogConfig);
    };
    StaffDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-staff-detail',
            template: __webpack_require__(/*! ./staff-detail.component.html */ "./src/app/layout/staff-detail/staff-detail.component.html"),
            styles: [__webpack_require__(/*! ./staff-detail.component.scss */ "./src/app/layout/staff-detail/staff-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialog"],
            src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_1__["StaffService"], src_app_services_class_service__WEBPACK_IMPORTED_MODULE_3__["ClassService"]])
    ], StaffDetailComponent);
    return StaffDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/staff-detail/staff-detail.module.ts":
/*!************************************************************!*\
  !*** ./src/app/layout/staff-detail/staff-detail.module.ts ***!
  \************************************************************/
/*! exports provided: StaffDetailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffDetailModule", function() { return StaffDetailModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _staff_detail_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./staff-detail-routing.module */ "./src/app/layout/staff-detail/staff-detail-routing.module.ts");
/* harmony import */ var _staff_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./staff-detail.component */ "./src/app/layout/staff-detail/staff-detail.component.ts");
/* harmony import */ var src_app_shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/modules/stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
/* harmony import */ var _custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../custom-dialog/custom-dialog.component */ "./src/app/layout/custom-dialog/custom-dialog.component.ts");
/* harmony import */ var _info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../info-dialog/info-dialog.component */ "./src/app/layout/info-dialog/info-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var StaffDetailModule = /** @class */ (function () {
    function StaffDetailModule() {
    }
    StaffDetailModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _staff_detail_routing_module__WEBPACK_IMPORTED_MODULE_4__["StaffDetailRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_6__["StatModule"]
            ],
            declarations: [_staff_detail_component__WEBPACK_IMPORTED_MODULE_5__["StaffDetailComponent"]],
            entryComponents: [_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_7__["CustomDialogComponent"], _info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_8__["InfoDialogComponent"]]
        })
    ], StaffDetailModule);
    return StaffDetailModule;
}());



/***/ }),

/***/ "./src/app/services/class.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/class.service.ts ***!
  \*******************************************/
/*! exports provided: ClassService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassService", function() { return ClassService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ClassService = /** @class */ (function () {
    function ClassService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'class/';
        this.token = localStorage.getItem('token');
    }
    ClassService.prototype.getListClass = function () {
        return this.http.get(this.url + 'class/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListAvailableClass = function () {
        return this.http.get(this.url + 'class/available', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListUnavailableClass = function () {
        return this.http.get(this.url + 'class/unavailable', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getClassByCode = function (code) {
        return this.http.get(this.url + 'class/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createClass = function (classEntity) {
        return this.http.post(this.url + 'class/' + classEntity, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateClass = function (classEntity) {
        return this.http.put(this.url + 'class/' + classEntity, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListMaterial = function () {
        return this.http.get(this.url + 'material/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getMaterialById = function (code) {
        return this.http.get(this.url + 'material/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createMaterial = function (material) {
        return this.http.post(this.url + 'material/', material, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateMaterial = function (material) {
        return this.http.put(this.url + 'material/', material, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListProgram = function () {
        return this.http.get(this.url + 'program/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getProgramByCode = function (code) {
        return this.http.get(this.url + 'program/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createProgram = function (program) {
        return this.http.post(this.url + 'program/', program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateProgram = function (program) {
        return this.http.put(this.url + 'program/', program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.deleteProgram = function (code) {
        return this.http.delete(this.url + 'program/delete/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListCompetency = function () {
        return this.http.get(this.url + 'competency/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListCompetencybyCode = function (materialCode) {
        return this.http.get(this.url + 'competency/material/' + materialCode, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getCompetencyByTrainerNip = function (trainerNip) {
        return this.http.get(this.url + 'competency/detail/' + trainerNip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getCompetencyByCode = function (material_code) {
        return this.http.get(this.url + 'competency/material/' + material_code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createCompetency = function (competency) {
        return this.http.post(this.url + 'competency/', competency, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListSchedule = function () {
        return this.http.get(this.url + 'schedule/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListScheduleByProgram = function (program) {
        return this.http.get(this.url + 'schedule/all/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleById = function (id) {
        return this.http.get(this.url + 'schedule/detail/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByCode = function (code) {
        return this.http.get(this.url + 'schedule/class/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByProgram = function (program) {
        return this.http.get(this.url + 'schedule/program/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByTrainerNip = function (nip) {
        return this.http.get(this.url + 'schedule/trainer/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createSchedule = function (schedule) {
        return this.http.post(this.url + 'schedule/', schedule, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateSchedule = function (schedule) {
        return this.http.put(this.url + 'schedule/', schedule, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createScheduleNonProgram = function (scheduleNonProgram) {
        return this.http.post(this.url + 'schedule/nonprogram', scheduleNonProgram, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.uploadMaterial = function (file) {
        return this.http.post(this.url + 'material/upload/', file, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            reportProgress: true,
            observe: 'events'
        });
    };
    ClassService.prototype.downloadMaterial = function (filename) {
        return this.http.get(this.url + 'material/download/' + filename, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'blob'
        });
    };
    ClassService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ClassService);
    return ClassService;
}());



/***/ }),

/***/ "./src/app/services/competency.ts":
/*!****************************************!*\
  !*** ./src/app/services/competency.ts ***!
  \****************************************/
/*! exports provided: Competency */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Competency", function() { return Competency; });
var Competency = /** @class */ (function () {
    function Competency() {
    }
    return Competency;
}());



/***/ }),

/***/ "./src/app/services/staff.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/staff.service.ts ***!
  \*******************************************/
/*! exports provided: StaffService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffService", function() { return StaffService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StaffService = /** @class */ (function () {
    function StaffService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'staff/';
        this.token = localStorage.getItem('token');
    }
    StaffService.prototype.getAllStaffs = function () {
        return this.http.get(this.url + 'all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainingStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/training/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainees = function () {
        return this.http.get(this.url + 'trainees', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainers = function () {
        return this.http.get(this.url + 'trainers', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllDivisions = function () {
        return this.http.get(this.url + 'division', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getStaffById = function (nip) {
        return this.http.get(this.url + 'detail/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.createStaff = function (staff) {
        console.log(staff);
        return this.http.post(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.updateStaff = function (staff) {
        console.log(staff);
        return this.http.put(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.deleteStaff = function (nip) {
        return this.http.delete(this.url + 'delete/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainingBadge = function (nip) {
        return this.http.get(this.url + 'training/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainerBadge = function (nip) {
        return this.http.get(this.url + 'trainer/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTrainerToday = function () {
        return this.http.get(this.url + 'trainer/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTraineeToday = function () {
        return this.http.get(this.url + 'trainee/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StaffService);
    return StaffService;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.html":
/*!*********************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card [ngClass]=\"bgClass\">\r\n    <mat-card-header>\r\n        <div mat-card-avatar>\r\n            <mat-icon class=\"icon-lg\">{{icon}}</mat-icon>\r\n        </div>\r\n        <mat-card-title>{{count}}</mat-card-title>\r\n        <mat-card-subtitle>{{label}}</mat-card-subtitle>\r\n    </mat-card-header>\r\n    <mat-card-actions>\r\n        <a href=\"javascript:void(0)\" class=\"float-right card-inverse\">\r\n            View Details\r\n        </a>\r\n    </mat-card-actions>\r\n</mat-card>"

/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host /deep/ .mat-card-header-text {\n  width: 100%;\n  text-align: right; }\n.icon-lg {\n  font-size: 40px; }\n.mat-card {\n  color: #fff; }\n.mat-card .mat-card-header {\n    width: 100%; }\n.mat-card .mat-card-title {\n    font-size: 40px !important; }\n.mat-card .mat-card-subtitle {\n    color: #fff; }\n.mat-card .mat-card-actions a {\n    text-decoration: none;\n    cursor: pointer;\n    color: #fff; }\n.mat-card.danger {\n  background: linear-gradient(60deg, #ec407a, #d81b60); }\n.mat-card.warn {\n  background: linear-gradient(60deg, #ffa726, #fb8c00); }\n.mat-card.success {\n  background: linear-gradient(60deg, #66bb6a, #43a047); }\n.mat-card.info {\n  background: linear-gradient(60deg, #26c6da, #00acc1); }\n"

/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.ts ***!
  \*******************************************************/
/*! exports provided: StatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatComponent", function() { return StatComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatComponent = /** @class */ (function () {
    function StatComponent() {
    }
    StatComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "bgClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "icon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "count", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "label", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "data", void 0);
    StatComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stat',
            template: __webpack_require__(/*! ./stat.component.html */ "./src/app/shared/modules/stat/stat.component.html"),
            styles: [__webpack_require__(/*! ./stat.component.scss */ "./src/app/shared/modules/stat/stat.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StatComponent);
    return StatComponent;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.module.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.module.ts ***!
  \****************************************************/
/*! exports provided: StatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatModule", function() { return StatModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _stat_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stat.component */ "./src/app/shared/modules/stat/stat.component.ts");
/* harmony import */ var _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../error/show-errors.component */ "./src/app/error/show-errors.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../layout/custom-dialog/custom-dialog.component */ "./src/app/layout/custom-dialog/custom-dialog.component.ts");
/* harmony import */ var _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../layout/info-dialog/info-dialog.component */ "./src/app/layout/info-dialog/info-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var StatModule = /** @class */ (function () {
    function StatModule() {
    }
    StatModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"]],
            declarations: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"], _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__["ShowErrorsComponent"], _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__["CustomDialogComponent"], _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__["InfoDialogComponent"]],
            exports: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"], _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__["ShowErrorsComponent"], _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__["CustomDialogComponent"], _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__["InfoDialogComponent"]]
        })
    ], StatModule);
    return StatModule;
}());



/***/ })

}]);
//# sourceMappingURL=staff-detail-staff-detail-module.js.map