(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["shuttle-shuttle-module"],{

/***/ "./src/app/layout/shuttle/shuttle-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/shuttle/shuttle-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: ShuttleRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShuttleRoutingModule", function() { return ShuttleRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shuttle_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shuttle.component */ "./src/app/layout/shuttle/shuttle.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _shuttle_component__WEBPACK_IMPORTED_MODULE_2__["ShuttleComponent"]
    }
];
var ShuttleRoutingModule = /** @class */ (function () {
    function ShuttleRoutingModule() {
    }
    ShuttleRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ShuttleRoutingModule);
    return ShuttleRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/shuttle/shuttle.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/shuttle/shuttle.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"top\"></div>\r\n\r\n<div fxLayout='column' class=\"container\">\r\n  \r\n    <div fxFlex=\"50\" fxLayout=\"column\" fxLayout.lt-md=\"column\" class=\"shuttle-title\">\r\n      <mat-card fxLayoutAlign=\"center center\" class=\"shuttle_header\">\r\n        <mat-card-header>Shuttle Information</mat-card-header>\r\n      </mat-card>\r\n    </div>\r\n    \r\n    <div fxFlex=\"50\" fxLayout=\"row\" fxLayout.lt-md=\"column\" fxLayoutGap=\"1%\">        \r\n      <div fxFlex=\"30\" class=\"shuttle-info\">     \r\n        <!-- <mat-card fxFlexFill > -->\r\n          <mat-accordion class=\"example-headers-align\">\r\n              <mat-expansion-panel class=\"accordion-panel\" (opened)=\"setImg(shuttle_initial.img)\" (closed)=\"resetImg()\" *ngFor=\"let shuttle_initial of shuttle\">\r\n                <mat-expansion-panel-header>\r\n                  <mat-panel-title >\t\t\t\t\t\t\t\t\t\r\n                    <mat-icon class=\"shuttle-icon\">directions_bus</mat-icon>\r\n                    <div class=\"accordion-title\">\r\n                      {{shuttle_initial.title}}\r\n                    </div>\t\t\t\t\t\t\t\t\t\r\n                  </mat-panel-title>\r\n                  <!-- <mat-panel-description>\r\n                    Detail alamat shuttle point \r\n                  </mat-panel-description> -->\r\n                </mat-expansion-panel-header>\r\n            \r\n                <div class=\"shuttle-card\">\r\n                  <div class=\"shuttle-card-center\">\r\n                    <div class=\"shuttle-card-img\">\r\n                      <!-- <img mat-card-image src=\"{{shuttle_initial.img}}\" alt=\"Photo of a Shuttle Info\"> -->\r\n                    </div>\r\n                    <div class=\"shuttle-card-info\">\r\n                      <table>\r\n                        <tr>\r\n                          <td>Shuttle point</td>\r\n                          <td> : </td>\r\n                          <td>{{shuttle_initial.shuttle_point}}</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>Departure time</td>\r\n                          <td> : </td>\r\n                          <td>{{shuttle_initial.departure_time}}</td>\r\n                        </tr>\r\n                        <tr>\r\n                          <td>PIC (contact)</td>\r\n                          <td> : </td>\r\n                          <td>{{shuttle_initial.pic}}</td>\r\n                        </tr>\r\n                      </table>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                \r\n              </mat-expansion-panel>\r\n              \r\n          </mat-accordion>\r\n        <!-- </mat-card> -->\r\n      </div>\r\n      <div fxFlex=\"70\" class=\"shuttle-img\">\r\n        <div class=\"shuttle-img\">\r\n          <img src=\"{{img}}\" alt=\"Lokasi BLI\">\t\r\n        </div>          \r\n      </div>\r\n    </div>\r\n  </div>\r\n  \r\n  <!-- <h1>Shuttle Information</h1>\r\n  \r\n  <mat-accordion class=\"example-headers-align\">\r\n    <mat-expansion-panel  [expanded]=\"step === 0\" (opened)=\"setStep(0)\" hideToggle *ngFor=\"let shuttle_initial of shuttle\">\r\n      <mat-expansion-panel-header>\r\n        <mat-panel-title>\r\n          \r\n          <mat-icon>directions_bus</mat-icon>\r\n          {{shuttle_initial.title}}\r\n        </mat-panel-title>\r\n        <mat-panel-description>\r\n          Detail alamat shuttle point \r\n        </mat-panel-description>\r\n      </mat-expansion-panel-header>\r\n  \r\n      <div class=\"shuttle-card\">\r\n        <div class=\"shuttle-card-center\">\r\n          <div class=\"shuttle-card-img\">\r\n            <img mat-card-image src=\"{{shuttle_initial.img}}\" alt=\"Photo of a Shuttle Info\">\r\n          </div>\r\n          <div class=\"shuttle-card-info\">\r\n            <table>\r\n              <tr>\r\n                <td>Shuttle point</td>\r\n                <td> : </td>\r\n                <td>{{shuttle_initial.shuttle_point}}</td>\r\n              </tr>\r\n              <tr>\r\n                <td>Departure time</td>\r\n                <td> : </td>\r\n                <td>{{shuttle_initial.departure_time}}</td>\r\n              </tr>\r\n              <tr>\r\n                <td>PIC (contact)</td>\r\n                <td> : </td>\r\n                <td>{{shuttle_initial.pic}}</td>\r\n              </tr>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      \r\n    </mat-expansion-panel>\r\n    \r\n  </mat-accordion>\r\n   -->\r\n  "

/***/ }),

/***/ "./src/app/layout/shuttle/shuttle.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/layout/shuttle/shuttle.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#top {\n  height: 20px;\n  width: 100%; }\n\n.shuttle_header {\n  padding: 12px 8px;\n  background-color: #c7cce6;\n  color: white; }\n\n.example-card {\n  max-width: 400px; }\n\n.example-header-image {\n  background-size: cover; }\n\n.example-headers-align .mat-expansion-panel-header-title,\n.example-headers-align .mat-expansion-panel-header-description {\n  flex-basis: 0; }\n\n.example-headers-align .mat-expansion-panel-header-description {\n  justify-content: space-between;\n  align-items: center; }\n\n/*Custom CSS*/\n\n.shuttle-card {\n  width: 100%; }\n\n.shuttle-card-center {\n  width: 90%; }\n\n.shuttle-card-img {\n  display: inline-block; }\n\n.shuttle-card-info {\n  display: inline-block;\n  height: 100%;\n  vertical-align: top; }\n\n.shuttle-card-img img {\n  width: 100%;\n  max-width: 400px;\n  display: block;\n  margin: auto; }\n\n.shuttle {\n  width: 50%; }\n\n.accordion-panel:hover {\n  background-color: rgba(139, 141, 142, 0.5); }\n\n.shuttle-img {\n  width: 100%;\n  box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.2), 0 1px 1px 0 rgba(0, 0, 0, 0.14), 0 1px 3px 0 rgba(0, 0, 0, 0.12);\n  border-radius: 4px; }\n\n.shuttle-img img {\n  width: 100%; }\n\n.shuttle-title {\n  margin-bottom: 1%; }\n\n@media (max-width: 600px) {\n  .shuttle-info {\n    order: 2; }\n  .shuttle-img {\n    order: 1; } }\n\n.shuttle-icon {\n  margin-right: 10px; }\n\nmat-card img {\n  -o-object-fit: cover;\n     object-fit: cover;\n  height: 100%; }\n\n.accordion-title {\n  padding: 1%;\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/layout/shuttle/shuttle.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/shuttle/shuttle.component.ts ***!
  \*****************************************************/
/*! exports provided: Shuttle, ExpansionStepsExample, ShuttleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Shuttle", function() { return Shuttle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpansionStepsExample", function() { return ExpansionStepsExample; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShuttleComponent", function() { return ShuttleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Shuttle = /** @class */ (function () {
    function Shuttle() {
    }
    return Shuttle;
}());

var ExpansionStepsExample = /** @class */ (function () {
    function ExpansionStepsExample() {
        this.step = 0;
    }
    ExpansionStepsExample.prototype.setStep = function (index) {
        this.step = index;
    };
    ExpansionStepsExample.prototype.nextStep = function () {
        this.step++;
    };
    ExpansionStepsExample.prototype.prevStep = function () {
        this.step--;
    };
    return ExpansionStepsExample;
}());

var ShuttleComponent = /** @class */ (function () {
    function ShuttleComponent() {
        this.img = "assets/images/blilokasi2.PNG";
        this.closedCounter = 6;
        this.shuttle = [
            { title: 'Wisma Asia', img: 'assets/images/shuttle_wsa2.PNG', shuttle_point: 'Wisma Asia', departure_time: '06:00 AM', pic: 'Om Yoh (14045)' },
            { title: 'Alam Sutera', img: 'assets/images/shuttle_alsut.PNG', shuttle_point: 'Alam Sutera', departure_time: '06:00 AM', pic: 'Om Yoh (14045)' },
            { title: 'Kelapa Gading', img: 'assets/images/shuttle_kelapagading.PNG', shuttle_point: 'Kelapa Gading', departure_time: '06:00 AM', pic: 'Om Yoh (14045)' },
            { title: 'Bekasi', img: 'assets/images/shuttle_bekasi.PNG', shuttle_point: 'Bekasi', departure_time: '06:00 AM', pic: 'Om Yoh (14045)' },
            { title: 'Bogor', img: 'assets/images/shuttle_bogor.PNG', shuttle_point: 'Bogor', departure_time: '06:00 AM', pic: 'Om Yoh (14045)' },
            { title: 'Pondok Indah', img: 'assets/images/shuttle_pondokindah.PNG', shuttle_point: 'Pondok Indah', departure_time: '06:00 AM', pic: 'Om Yoh (14045)' }
        ];
    }
    ShuttleComponent.prototype.ngOnInit = function () {
        console.log(this.shuttle);
        var top = document.getElementById('top');
        if (top !== null) {
            top.scrollIntoView();
            top = null;
        }
    };
    ShuttleComponent.prototype.setImg = function (url) {
        this.img = url;
        this.closedCounter--;
    };
    ShuttleComponent.prototype.resetImg = function () {
        this.closedCounter++;
        if (this.closedCounter === 6)
            this.img = "assets/images/blilokasi2.PNG";
    };
    ShuttleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-shuttle',
            template: __webpack_require__(/*! ./shuttle.component.html */ "./src/app/layout/shuttle/shuttle.component.html"),
            styles: [__webpack_require__(/*! ./shuttle.component.scss */ "./src/app/layout/shuttle/shuttle.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ShuttleComponent);
    return ShuttleComponent;
}());



/***/ }),

/***/ "./src/app/layout/shuttle/shuttle.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/shuttle/shuttle.module.ts ***!
  \**************************************************/
/*! exports provided: ShuttleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShuttleModule", function() { return ShuttleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _shuttle_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shuttle-routing.module */ "./src/app/layout/shuttle/shuttle-routing.module.ts");
/* harmony import */ var _shuttle_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shuttle.component */ "./src/app/layout/shuttle/shuttle.component.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var ShuttleModule = /** @class */ (function () {
    function ShuttleModule() {
    }
    ShuttleModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _shuttle_routing_module__WEBPACK_IMPORTED_MODULE_6__["ShuttleRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_3__["MatTabsModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_8__["FlexLayoutModule"]
            ],
            declarations: [_shuttle_component__WEBPACK_IMPORTED_MODULE_7__["ShuttleComponent"]]
        })
    ], ShuttleModule);
    return ShuttleModule;
}());



/***/ })

}]);
//# sourceMappingURL=shuttle-shuttle-module.js.map