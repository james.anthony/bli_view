(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["confirmation-page-confirmation-page-module"],{

/***/ "./src/app/layout/confirmation-page/animation.ts":
/*!*******************************************************!*\
  !*** ./src/app/layout/confirmation-page/animation.ts ***!
  \*******************************************************/
/*! exports provided: SlideInOutAnimation, RollOutAnimation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SlideInOutAnimation", function() { return SlideInOutAnimation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RollOutAnimation", function() { return RollOutAnimation; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var SlideInOutAnimation = [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('slideInOut', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
            'max-height': '500px', 'opacity': '1', 'visibility': 'visible'
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
            'max-height': '0px', 'opacity': '0', 'visibility': 'hidden'
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('in => out', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'opacity': '0'
                })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('600ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'max-height': '0px'
                })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('700ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'visibility': 'hidden'
                }))
            ])]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('out => in', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('1ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'visibility': 'visible'
                })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('600ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'max-height': '500px'
                })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('800ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'opacity': '1'
                }))
            ])])
    ]),
];
var RollOutAnimation = [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('rollOut', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('start', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
            'opacity': '1'
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('end', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
            'opacity': '0',
            'visibility': 'hidden'
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('start => end', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('500ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'opacity': '1'
                })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('1300ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'opacity': '0',
                    'transform': 'translate3d(-100%, 0, 0) rotate3d(0, 0, 1, 60deg)'
                }))
            ])]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('end => start', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('1ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'visibility': 'visible'
                })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('200ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'max-height': '500px'
                })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    'opacity': '1'
                }))
            ])])
    ]),
];


/***/ }),

/***/ "./src/app/layout/confirmation-page/confirmation-page-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/layout/confirmation-page/confirmation-page-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: ConfirmationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationPageRoutingModule", function() { return ConfirmationPageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _confirmation_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./confirmation-page.component */ "./src/app/layout/confirmation-page/confirmation-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _confirmation_page_component__WEBPACK_IMPORTED_MODULE_2__["ConfirmationPageComponent"]
    }
];
var ConfirmationPageRoutingModule = /** @class */ (function () {
    function ConfirmationPageRoutingModule() {
    }
    ConfirmationPageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ConfirmationPageRoutingModule);
    return ConfirmationPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/confirmation-page/confirmation-page.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/layout/confirmation-page/confirmation-page.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"top\"></div>\r\n<div class=\"ticket-group\">\r\n\r\n</div>\r\n\r\n\r\n<div class=\"container\">\r\n  <div class=\"ticket-group\" >\r\n    <div class=\"ticket-upper\">\r\n      <div fxFlex fxLayout=\"row\">\r\n        \r\n          <div fxFlex=\"40\" class=\"food-img\" *ngIf=\"receiveFlag == 1 || receiveFlag == 2\" id=\"screen1\">\r\n              <img src=\"{{bookingFoodImg}}\" alt=\"\">\r\n          </div>\r\n          <div fxFlex=\"60\" class=\"order\" *ngIf=\"receiveFlag == 1 || receiveFlag == 2\" id=\"screen1\">\r\n                <h3>Bukti Pemesanan</h3>\r\n                <hr>\r\n                <table>\r\n                  <tr>\r\n                    <td><b>Booking ID</b></td>\r\n                    <td>:</td>\r\n                    <td>{{booking?.booking_id}}</td>\r\n                  </tr>\r\n                  <tr>\r\n                      <td><b>Menu</b></td>\r\n                      <td>:</td>\r\n                      <td>{{booking?.stall_name}}</td>\r\n                  </tr>\r\n                  <tr>\r\n                      <td><b>Booking Time</b></td>\r\n                      <td>:</td>\r\n                      <td>{{booking?.booking_time | date:'medium'}}</td>\r\n                  </tr>\r\n                </table>          \r\n          </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"ticket-bottom\" id=\"screen1\" [@rollOut]=\"animationStateFront\">\r\n\r\n      <div class=\"divider\" >\r\n          <div class=\"red-clr\" >\r\n              <mat-icon class=\"vertical-align\">info</mat-icon>\r\n              <span>Pastikan yang menekan tombol ini adalah petugas makanan yang punya otoritas ya!</span>\r\n          </div>\r\n          <button mat-raised-button class=\"btn-1\" color=\"primary\" (click)=\"confirmFood(booking)\">Terima</button>          \r\n      </div>      \r\n     \r\n      <div class=\"ticket-border\">\r\n        <div class=\"ticket-border-style\" *ngFor='let in of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19];let i = index'>\t\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"masukan\" id=\"screen2\" [@rollOut]=\"animationStateBack\">\r\n\r\n      <h3 style=\"margin-top:0; padding-top:10px;\">Berikan Masukan</h3>\r\n      \r\n          <div>\r\n            <mat-icon *ngFor=\"let star of Arr(starNumber); let i = index\" \r\n            class=\"icon-rating\" \r\n            (mouseover)=\"changeStar(i)\" \r\n            (mouseleave)=\"changeStarBorder(i)\"\r\n            (click)=\"rating(i);\"\r\n            id=\"{{i}}\">{{starTxt}}</mat-icon>\r\n          </div> \r\n     \r\n      <div class=\"ticket-border\">\r\n      </div>\r\n    </div>\r\n  </div>\r\n  \r\n</div>"

/***/ }),

/***/ "./src/app/layout/confirmation-page/confirmation-page.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/layout/confirmation-page/confirmation-page.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#top {\n  height: 20px;\n  width: 100%; }\n\n.background_container {\n  width: 100%;\n  height: 100%;\n  margin: -20px;\n  position: absolute; }\n\n.image_container {\n  overflow: hidden;\n  width: 100%;\n  height: 100vh; }\n\n.food_img {\n  width: 100%;\n  height: 100vh; }\n\n.bli-so-box-blue {\n  background: linear-gradient(60deg, #b3bbff, #8476ff);\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12);\n  border-radius: 4px;\n  z-index: 2;\n  padding: 10px;\n  position: absolute;\n  max-width: 400px;\n  top: 0;\n  left: 0;\n  right: 0;\n  margin: auto; }\n\n.container {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.bli-si-box {\n  background-color: white;\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12);\n  border-radius: 2px;\n  padding: 15px;\n  text-align: center; }\n\n.bli-content-box {\n  padding: 5px; }\n\n.btn-1 {\n  width: 100%; }\n\n.btn-2 {\n  background-color: lightgray;\n  margin-right: 10px; }\n\n.sv-btn-row {\n  text-align: right;\n  padding: 10px; }\n\n.max-width {\n  width: 100%; }\n\n.red-clr {\n  color: red;\n  font-size: 12px;\n  padding: 10px; }\n\n.blue-clr {\n  color: #8476ff; }\n\n.vertical-align {\n  vertical-align: middle; }\n\n.dline {\n  display: inline-block;\n  width: 50%;\n  text-align: left; }\n\n.desc-box {\n  text-align: left;\n  padding: 0 20px 0 20px; }\n\n.icon-rating {\n  width: unset;\n  font-size: 50px;\n  padding: 2px;\n  height: unset; }\n\n.icon-rating:hover {\n  cursor: pointer; }\n\n.ticket-group {\n  width: 410px; }\n\n@media (max-width: 600px) {\n  .blue-clr {\n    display: block; }\n  .dline {\n    width: unset; }\n  .desc-box {\n    text-align: unset; }\n  h1 {\n    text-align: center; }\n  .icon-rating {\n    font-size: 40px; }\n  .ticket-group {\n    width: 90%; }\n  .order {\n    font-size: 10px; } }\n\n.ticket-upper {\n  width: 100%;\n  background-color: #121e33;\n  border-radius: 15px 15px 0px 0px;\n  color: white;\n  padding: 3%;\n  border-bottom: white;\n  border-bottom-style: dashed; }\n\n.ticket-bottom {\n  width: 106%;\n  height: 120px;\n  background-color: #b4b5ba;\n  position: relative;\n  text-align: center; }\n\n.ticket-border {\n  width: 100%;\n  height: 10px;\n  position: absolute;\n  bottom: 0; }\n\n.ticket-border-style {\n  width: 10px;\n  margin-left: 10px;\n  border-radius: 10px 10px 0px 0px;\n  background-color: ghostwhite;\n  height: 10px;\n  display: inline-block; }\n\n.food-img {\n  border-radius: 15px;\n  justify-content: center;\n  align-items: center;\n  overflow: hidden;\n  display: flex; }\n\n.food-img img {\n  width: 100%;\n  flex: none;\n  border-radius: 5px; }\n\n.spacer {\n  height: 10%;\n  width: 100%; }\n\n.order {\n  display: inline-block;\n  margin-left: 3%;\n  align-items: center;\n  font-family: Consolas;\n  color: white; }\n\n.order table {\n  font-size: 12px; }\n\n.order p {\n  text-align: right;\n  font-size: 1em;\n  line-height: 0.5em;\n  position: relative; }\n\n.booking-id {\n  width: 60%;\n  margin-left: 5%;\n  text-align: center;\n  font-size: 4em; }\n\n.masukan {\n  width: 100%;\n  text-align: center; }\n\n#screen2 {\n  margin-top: -120px; }\n"

/***/ }),

/***/ "./src/app/layout/confirmation-page/confirmation-page.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/layout/confirmation-page/confirmation-page.component.ts ***!
  \*************************************************************************/
/*! exports provided: ConfirmationPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationPageComponent", function() { return ConfirmationPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_booking_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/booking.service */ "./src/app/services/booking.service.ts");
/* harmony import */ var _animation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./animation */ "./src/app/layout/confirmation-page/animation.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConfirmationPageComponent = /** @class */ (function () {
    function ConfirmationPageComponent(bookingService, router) {
        var _this = this;
        this.bookingService = bookingService;
        this.router = router;
        this.animationStateFront = 'start';
        this.animationStateBack = 'end';
        this.Arr = Array;
        this.starNumber = 5;
        this.receiveFlag = 1;
        this.is_confirmed = 0;
        this.starTxt = "star_border";
        this.bookingFoodImg = "assets/images/batagor.jpg";
        this.nip = localStorage.getItem('nip');
        bookingService.getBookingByNip(this.nip).subscribe(function (res) {
            _this.booking = res.body;
            _this.is_confirmed = _this.booking.is_confirmed;
            if (_this.is_confirmed == 1) {
                _this.animationStateFront = _this.animationStateFront === 'end' ? 'start' : 'end';
                _this.animationStateBack = _this.animationStateBack === 'start' ? 'end' : 'start';
            }
        });
    }
    ConfirmationPageComponent.prototype.ngOnInit = function () {
        var top = document.getElementById('top');
        if (top !== null) {
            top.scrollIntoView();
            top = null;
        }
    };
    ConfirmationPageComponent.prototype.changeStar = function (num) {
        var index = num + 1;
        for (var i = 0; i <= num; i++) {
            document.getElementById("" + i + "").innerHTML = "star";
            document.getElementById("" + i + "").style.color = "gold";
        }
    };
    ConfirmationPageComponent.prototype.changeStarBorder = function () {
        for (var i = 0; i <= this.starNumber; i++) {
            document.getElementById("" + i + "").innerHTML = "star_border";
            document.getElementById("" + i + "").style.color = "unset";
        }
    };
    ConfirmationPageComponent.prototype.rating = function (index) {
        var _this = this;
        this.booking.rating = index + 1;
        this.bookingService.updateRating(this.booking).subscribe(function (res) {
            if (res.body == true) {
                //go to url
                _this.receiveFlag = 1;
                _this.router.navigate(['/food-stall']);
            }
            else {
                alert('Service error');
            }
        });
    };
    ConfirmationPageComponent.prototype.confirmFood = function (booking) {
        var _this = this;
        this.receiveFlag = 2;
        this.animationStateFront = this.animationStateFront === 'end' ? 'start' : 'end';
        this.animationStateBack = this.animationStateBack === 'start' ? 'end' : 'start';
        this.booking = booking;
        this.booking.is_confirmed = 1;
        this.bookingService.updateConfirmation(this.booking).subscribe(function (res) {
            if (res.body == true) {
                _this.receiveFlag = 2;
            }
            else {
                alert('Service error');
            }
        });
    };
    ConfirmationPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-confirmation-page',
            template: __webpack_require__(/*! ./confirmation-page.component.html */ "./src/app/layout/confirmation-page/confirmation-page.component.html"),
            styles: [__webpack_require__(/*! ./confirmation-page.component.scss */ "./src/app/layout/confirmation-page/confirmation-page.component.scss")],
            animations: [_animation__WEBPACK_IMPORTED_MODULE_2__["RollOutAnimation"]]
        }),
        __metadata("design:paramtypes", [src_app_services_booking_service__WEBPACK_IMPORTED_MODULE_1__["BookingService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ConfirmationPageComponent);
    return ConfirmationPageComponent;
}());



/***/ }),

/***/ "./src/app/layout/confirmation-page/confirmation-page.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/layout/confirmation-page/confirmation-page.module.ts ***!
  \**********************************************************************/
/*! exports provided: ConfirmationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationPageModule", function() { return ConfirmationPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _confirmation_page_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./confirmation-page-routing.module */ "./src/app/layout/confirmation-page/confirmation-page-routing.module.ts");
/* harmony import */ var _confirmation_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./confirmation-page.component */ "./src/app/layout/confirmation-page/confirmation-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var ConfirmationPageModule = /** @class */ (function () {
    function ConfirmationPageModule() {
    }
    ConfirmationPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _confirmation_page_routing_module__WEBPACK_IMPORTED_MODULE_8__["ConfirmationPageRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__["FlexLayoutModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material_divider__WEBPACK_IMPORTED_MODULE_5__["MatDividerModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"]
            ],
            declarations: [_confirmation_page_component__WEBPACK_IMPORTED_MODULE_9__["ConfirmationPageComponent"]]
        })
    ], ConfirmationPageModule);
    return ConfirmationPageModule;
}());



/***/ })

}]);
//# sourceMappingURL=confirmation-page-confirmation-page-module.js.map