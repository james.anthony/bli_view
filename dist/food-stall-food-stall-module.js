(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["food-stall-food-stall-module"],{

/***/ "./src/app/error/show-errors.component.ts":
/*!************************************************!*\
  !*** ./src/app/error/show-errors.component.ts ***!
  \************************************************/
/*! exports provided: ShowErrorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowErrorsComponent", function() { return ShowErrorsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShowErrorsComponent = /** @class */ (function () {
    function ShowErrorsComponent() {
    }
    ShowErrorsComponent_1 = ShowErrorsComponent;
    ShowErrorsComponent.prototype.shouldShowErrors = function () {
        return this.control &&
            this.control.errors &&
            (this.control.dirty || this.control.touched);
    };
    ShowErrorsComponent.prototype.listOfErrors = function () {
        var _this = this;
        return Object.keys(this.control.errors)
            .map(function (field) { return _this.getMessage(field, _this.control.errors[field]); });
    };
    ShowErrorsComponent.prototype.getMessage = function (type, params) {
        return ShowErrorsComponent_1.errorMessages[type](params);
    };
    ShowErrorsComponent.errorMessages = {
        'required': function () { return 'This field is required'; },
        'minlength': function (params) { return 'The min number of characters is ' + params.requiredLength; },
        'maxlength': function (params) { return 'The max allowed number of characters is ' + params.requiredLength; },
        'ipaddress': function (params) { return params.message; },
        'pattern': function (params) { return 'Numbers only'; }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ShowErrorsComponent.prototype, "control", void 0);
    ShowErrorsComponent = ShowErrorsComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-show-errors',
            template: "\n        <div *ngIf=\"shouldShowErrors()\">\n            <p style=\"margin-left:10px; font-size:0.8em; margin-top:-10px;\" *ngFor=\"let error of listOfErrors()\">{{error}}</p>\n        </div>\n    "
        })
    ], ShowErrorsComponent);
    return ShowErrorsComponent;
    var ShowErrorsComponent_1;
}());



/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title *ngIf=\"listData.data.location != '100'\">Booking {{listData.data.food.description | titlecase}}?</h2>\r\n<h2 mat-dialog-title *ngIf=\"listData.data.location == '100'\">Booking Prasmanan?</h2>\r\n\r\n<mat-dialog-actions>\r\n    <button class=\"mat-raised-button\"(click)=\"close()\">No</button>\r\n    <button class=\"mat-raised-button mat-primary\"(click)=\"save()\">Yes</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n"

/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.ts ***!
  \*****************************************************************/
/*! exports provided: CustomDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomDialogComponent", function() { return CustomDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var CustomDialogComponent = /** @class */ (function () {
    function CustomDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.listData = data;
    }
    CustomDialogComponent.prototype.ngOnInit = function () {
    };
    CustomDialogComponent.prototype.save = function () {
        this.dialogRef.close(this.listData);
    };
    CustomDialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    CustomDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-custom-dialog',
            template: __webpack_require__(/*! ./custom-dialog.component.html */ "./src/app/layout/custom-dialog/custom-dialog.component.html"),
            styles: [__webpack_require__(/*! ./custom-dialog.component.scss */ "./src/app/layout/custom-dialog/custom-dialog.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], CustomDialogComponent);
    return CustomDialogComponent;
}());



/***/ }),

/***/ "./src/app/layout/food-stall/food-stall-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/food-stall/food-stall-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: FoodStallRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodStallRoutingModule", function() { return FoodStallRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _food_stall_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./food-stall.component */ "./src/app/layout/food-stall/food-stall.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _food_stall_component__WEBPACK_IMPORTED_MODULE_2__["FoodStallComponent"]
    }
];
var FoodStallRoutingModule = /** @class */ (function () {
    function FoodStallRoutingModule() {
    }
    FoodStallRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], FoodStallRoutingModule);
    return FoodStallRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/food-stall/food-stall.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/food-stall/food-stall.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"top\"></div>\r\n\r\n<div class=\"map-box-container\" id=\"prasmanan_container\" (click)=\"openDialog(prasmananArray[0])\">\r\n    <img src=\"assets/images/prasmanan.jpg\" alt=\"\" class=\"bkground-img\">\r\n    <div class=\"overlay-dark\" id=\"prasmanan_overlay\"></div>\r\n    <div class=\"map-box-content\">\r\n        <h2>Prasmanan Menu<span style=\"color: rgb(220, 0, 0);\"> ®</span></h2>\r\n        <div class=\"pres-desc\">{{prasmananArray[0]?.food.description | titlecase}}</div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"map-box-container\">\r\n    <img src=\"assets/images/ug.jpg\" alt=\"\" class=\"bkground-img\">\r\n    <div class=\"overlay-dark\"></div>\r\n    <div class=\"map-box-content\">\r\n        <h2>Upperground Menu<span style=\"color: rgb(220, 0, 0);\"> ®</span></h2>\r\n\r\n        <div class=\"pRelative dFlex\">\r\n            <div class=\"quarterWidth pRelative\" *ngFor=\"let stall of ugArray\" (click)=\"openDialog(stall)\">\r\n                <h4>Name : \r\n                    <div class=\" redClr\">{{stall?.food.name | titlecase}}</div>\r\n                </h4>\r\n                <h4>Stock : \r\n                    <div class=\" redClr\">{{stall?.book_stock}}</div>\r\n                </h4>\r\n                <h4>Queue : \r\n                    <div class=\" redClr\">{{stall?.queue_stock}}</div>\r\n                </h4>\r\n                <mat-icon class=\"breakLine\" matTooltip=\"info\" \r\n                aria-label=\"stallTip\" matTooltipPosition=\"above\" \r\n                class=\"qIcon\">help_outline</mat-icon>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"map-box-container\">\r\n    <img src=\"assets/images/lg.jpg\" alt=\"\" class=\"bkground-img\">\r\n    <div class=\"overlay-dark\"></div>\r\n    <div class=\"map-box-content\">\r\n        <h2>Lowerground Menu<span style=\"color: rgb(220, 0, 0);\"> ®</span></h2>\r\n    \r\n        <div class=\"pRelative dFlex\">\r\n            <div class=\"quarterWidth pRelative\" *ngFor=\"let stall of lgArray\" (click)=\"openDialog(stall)\">\r\n                <h4>Name :\r\n                    <div class=\"redClr\">{{stall?.food.name | titlecase}}</div>\r\n                </h4>\r\n                <h4>Stock :\r\n                    <div class=\"redClr\">{{stall?.book_stock}}</div>\r\n                </h4>\r\n                <h4>Queue :\r\n                    <div class=\"redClr\">{{stall?.queue_stock}}</div>\r\n                </h4>\r\n                <mat-icon class=\"breakLine\" matTooltip=\"info\" \r\n                aria-label=\"stallTip2\" matTooltipPosition=\"above\" \r\n                class=\"qIcon\">help_outline</mat-icon>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/layout/food-stall/food-stall.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/food-stall/food-stall.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#top {\n  height: 20px;\n  width: 100%; }\n\ndiv {\n  color: white; }\n\n.no-margin-top {\n  margin-top: 0; }\n\n.container {\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12); }\n\n#prasmanan_container {\n  height: 150px; }\n\n.map-box-container {\n  margin-bottom: 20px;\n  border-radius: 2px;\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12);\n  position: relative;\n  padding-top: 20px;\n  padding-bottom: 10px; }\n\n.map-box-container #prasmanan_overlay:hover {\n    background: linear-gradient(0deg, #000000c1, #0000007e);\n    cursor: pointer; }\n\n.map-box-content {\n  padding: 0.5vw;\n  display: flex;\n  flex-direction: column; }\n\n.map-box-content h2 {\n  margin: 0;\n  padding: 0 20px 10px 20px;\n  margin-bottom: 10px;\n  text-align: center;\n  border-radius: 100%;\n  border-bottom: 5px white solid;\n  box-shadow: 0 3px 4px rgba(255, 255, 255, 0.4);\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  -ms-grid-row-align: center;\n      align-self: center;\n  position: relative; }\n\n.quarterWidth {\n  flex-grow: 1;\n  border-radius: 4px;\n  margin: 0.5vw;\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px white;\n  height: 175px;\n  text-align: center; }\n\n.dFlex {\n  display: flex; }\n\n@media screen and (max-width: 600px) {\n    .dFlex {\n      flex-direction: column;\n      width: unset; } }\n\n.pName {\n  position: absolute;\n  right: 2px;\n  bottom: 2px;\n  background-color: white;\n  padding: 5px;\n  border: 1px solid dotted;\n  border-radius: 2px;\n  min-width: 100px; }\n\n.redClr {\n  color: white; }\n\n.noDisplay {\n  display: none; }\n\n.quarterWidth:hover {\n  background: linear-gradient(10deg, #000000, #0000001b);\n  cursor: pointer; }\n\n.pRelative {\n  position: relative; }\n\n.qIcon {\n  position: absolute;\n  top: 5px;\n  right: 5px;\n  border-radius: 100%; }\n\n.breakLine {\n  white-space: pre-line; }\n\n.bkground-img {\n  position: absolute;\n  top: 0;\n  left: 0;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 2px;\n  width: 100%;\n  height: 100%; }\n\n.overlay-dark {\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.5);\n  position: absolute;\n  left: 0;\n  top: 0; }\n\n.qwText {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0; }\n\n.pres-desc {\n  text-align: center;\n  padding: 1vw;\n  padding-top: 30px;\n  position: relative; }\n"

/***/ }),

/***/ "./src/app/layout/food-stall/food-stall.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/food-stall/food-stall.component.ts ***!
  \***********************************************************/
/*! exports provided: FoodStallComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodStallComponent", function() { return FoodStallComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_food_stall_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/food-stall.service */ "./src/app/services/food-stall.service.ts");
/* harmony import */ var _services_booking_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/booking.service */ "./src/app/services/booking.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../custom-dialog/custom-dialog.component */ "./src/app/layout/custom-dialog/custom-dialog.component.ts");
/* harmony import */ var _info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../info-dialog/info-dialog.component */ "./src/app/layout/info-dialog/info-dialog.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FoodStallComponent = /** @class */ (function () {
    function FoodStallComponent(router, foodStallService, bookingService, dialog) {
        var _this = this;
        this.router = router;
        this.foodStallService = foodStallService;
        this.bookingService = bookingService;
        this.dialog = dialog;
        this.nip = "";
        this.allMenuArray = [];
        this.prasmananArray = [];
        this.ugArray = [];
        this.lgArray = [];
        this.nip = localStorage.getItem('nip');
        this.booking = { booking_id: null, nip: this.nip, stall_id: null, stall_name: '', stall_content: '', rating: null, is_confirmed: null };
        this.nip = localStorage.getItem("nip");
        foodStallService.getAllStalls().subscribe(function (res) {
            _this.allMenuArray = res.body;
            _this.allMenuArray.forEach(function (element) {
                if (element.location.includes('UG')) {
                    _this.ugArray.push(element);
                }
                else if (element.location.includes('LG')) {
                    _this.lgArray.push(element);
                }
                else if (element.location.includes('100')) {
                    _this.prasmananArray.push(element);
                }
            });
        });
        this.bookingService.getBookingByNip(this.nip).subscribe(function (res) {
            console.log(res.body);
            _this.booking = res.body;
            if (_this.booking.is_confirmed == 1 && _this.booking.rating == 0) {
                _this.router.navigate(['/confirmation-page']);
            }
        });
    }
    FoodStallComponent.prototype.bookingAction = function (data) {
        var _this = this;
        if (data) {
            if (data.data.book_stock != 0) {
                this.booking.stall_id = data.data.stall_id;
                this.booking.stall_name = data.data.food.name;
                this.booking.stall_content = data.data.food.description;
                this.bookingService.isBooked(this.nip).subscribe(function (res) {
                    if (res.body == false) {
                        _this.foodStallService.checkStock(data.location).subscribe(function (res) {
                            if (res.body[0] != 0) {
                                _this.foodStallService.reduceStock(data.data.stall_id).subscribe(function (res) {
                                    _this.bookingService.createBooking(_this.booking).subscribe(function (res) {
                                        if (res.status == 200 || res.status == 201) {
                                            _this.infoDialog("proses booking berhasil");
                                            _this.router.navigate(['/confirmation-page']);
                                        }
                                        else {
                                            _this.infoDialog("proses booking gagal");
                                        }
                                    });
                                });
                            }
                            else {
                                _this.infoDialog("stok telah habis");
                            }
                        });
                    }
                    else {
                        _this.infoDialog("Hari ini sudah booking");
                    }
                });
            }
            else {
                this.infoDialog("makanan tidak bisa di book lagi!");
            }
        }
    };
    FoodStallComponent.prototype.ngOnInit = function () {
        var top = document.getElementById('top');
        if (top !== null) {
            top.scrollIntoView();
            top = null;
        }
    };
    FoodStallComponent.prototype.openDialog = function (bookingData) {
        var _this = this;
        var dialogConfig = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogConfig"]();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
            data: bookingData
        };
        var dialogRef = this.dialog.open(_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_4__["CustomDialogComponent"], dialogConfig);
        dialogRef.afterClosed().subscribe(function (data) { return _this.bookingAction(data); });
    };
    FoodStallComponent.prototype.infoDialog = function (text) {
        var dialogConfig = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogConfig"]();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
            data: text
        };
        var dialogRef = this.dialog.open(_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_5__["InfoDialogComponent"], dialogConfig);
    };
    FoodStallComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-food-stall',
            template: __webpack_require__(/*! ./food-stall.component.html */ "./src/app/layout/food-stall/food-stall.component.html"),
            styles: [__webpack_require__(/*! ./food-stall.component.scss */ "./src/app/layout/food-stall/food-stall.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _services_food_stall_service__WEBPACK_IMPORTED_MODULE_1__["FoodStallService"], _services_booking_service__WEBPACK_IMPORTED_MODULE_2__["BookingService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]])
    ], FoodStallComponent);
    return FoodStallComponent;
}());



/***/ }),

/***/ "./src/app/layout/food-stall/food-stall.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/food-stall/food-stall.module.ts ***!
  \********************************************************/
/*! exports provided: FoodStallModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodStallModule", function() { return FoodStallModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _food_stall_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./food-stall-routing.module */ "./src/app/layout/food-stall/food-stall-routing.module.ts");
/* harmony import */ var _food_stall_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./food-stall.component */ "./src/app/layout/food-stall/food-stall.component.ts");
/* harmony import */ var _custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../custom-dialog/custom-dialog.component */ "./src/app/layout/custom-dialog/custom-dialog.component.ts");
/* harmony import */ var _info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../info-dialog/info-dialog.component */ "./src/app/layout/info-dialog/info-dialog.component.ts");
/* harmony import */ var _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/modules/stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var FoodStallModule = /** @class */ (function () {
    function FoodStallModule() {
    }
    FoodStallModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _food_stall_routing_module__WEBPACK_IMPORTED_MODULE_3__["FoodStallRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_7__["StatModule"]
            ],
            declarations: [_food_stall_component__WEBPACK_IMPORTED_MODULE_4__["FoodStallComponent"]],
            entryComponents: [_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__["CustomDialogComponent"], _info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__["InfoDialogComponent"]]
        })
    ], FoodStallModule);
    return FoodStallModule;
}());



/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.html":
/*!***************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>{{info.data}}</h2>\r\n\r\n<mat-dialog-actions>\r\n    <button class=\"mat-raised-button mat-primary\"(click)=\"close()\">Ok</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n"

/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.ts ***!
  \*************************************************************/
/*! exports provided: InfoDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoDialogComponent", function() { return InfoDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var InfoDialogComponent = /** @class */ (function () {
    function InfoDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.info = data;
        console.log(this.info);
    }
    InfoDialogComponent.prototype.ngOnInit = function () {
    };
    InfoDialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    InfoDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-info-dialog',
            template: __webpack_require__(/*! ./info-dialog.component.html */ "./src/app/layout/info-dialog/info-dialog.component.html"),
            styles: [__webpack_require__(/*! ./info-dialog.component.scss */ "./src/app/layout/info-dialog/info-dialog.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], InfoDialogComponent);
    return InfoDialogComponent;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.html":
/*!*********************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card [ngClass]=\"bgClass\">\r\n    <mat-card-header>\r\n        <div mat-card-avatar>\r\n            <mat-icon class=\"icon-lg\">{{icon}}</mat-icon>\r\n        </div>\r\n        <mat-card-title>{{count}}</mat-card-title>\r\n        <mat-card-subtitle>{{label}}</mat-card-subtitle>\r\n    </mat-card-header>\r\n    <mat-card-actions>\r\n        <a href=\"javascript:void(0)\" class=\"float-right card-inverse\">\r\n            View Details\r\n        </a>\r\n    </mat-card-actions>\r\n</mat-card>"

/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host /deep/ .mat-card-header-text {\n  width: 100%;\n  text-align: right; }\n.icon-lg {\n  font-size: 40px; }\n.mat-card {\n  color: #fff; }\n.mat-card .mat-card-header {\n    width: 100%; }\n.mat-card .mat-card-title {\n    font-size: 40px !important; }\n.mat-card .mat-card-subtitle {\n    color: #fff; }\n.mat-card .mat-card-actions a {\n    text-decoration: none;\n    cursor: pointer;\n    color: #fff; }\n.mat-card.danger {\n  background: linear-gradient(60deg, #ec407a, #d81b60); }\n.mat-card.warn {\n  background: linear-gradient(60deg, #ffa726, #fb8c00); }\n.mat-card.success {\n  background: linear-gradient(60deg, #66bb6a, #43a047); }\n.mat-card.info {\n  background: linear-gradient(60deg, #26c6da, #00acc1); }\n"

/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.ts ***!
  \*******************************************************/
/*! exports provided: StatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatComponent", function() { return StatComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatComponent = /** @class */ (function () {
    function StatComponent() {
    }
    StatComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "bgClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "icon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "count", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "label", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "data", void 0);
    StatComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stat',
            template: __webpack_require__(/*! ./stat.component.html */ "./src/app/shared/modules/stat/stat.component.html"),
            styles: [__webpack_require__(/*! ./stat.component.scss */ "./src/app/shared/modules/stat/stat.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StatComponent);
    return StatComponent;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.module.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.module.ts ***!
  \****************************************************/
/*! exports provided: StatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatModule", function() { return StatModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _stat_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stat.component */ "./src/app/shared/modules/stat/stat.component.ts");
/* harmony import */ var _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../error/show-errors.component */ "./src/app/error/show-errors.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../layout/custom-dialog/custom-dialog.component */ "./src/app/layout/custom-dialog/custom-dialog.component.ts");
/* harmony import */ var _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../layout/info-dialog/info-dialog.component */ "./src/app/layout/info-dialog/info-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var StatModule = /** @class */ (function () {
    function StatModule() {
    }
    StatModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"]],
            declarations: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"], _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__["ShowErrorsComponent"], _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__["CustomDialogComponent"], _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__["InfoDialogComponent"]],
            exports: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"], _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__["ShowErrorsComponent"], _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__["CustomDialogComponent"], _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__["InfoDialogComponent"]]
        })
    ], StatModule);
    return StatModule;
}());



/***/ })

}]);
//# sourceMappingURL=food-stall-food-stall-module.js.map