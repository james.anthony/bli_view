(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["class-class-module"],{

/***/ "./src/app/layout/class/class-routing.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/class/class-routing.module.ts ***!
  \******************************************************/
/*! exports provided: ClassRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassRoutingModule", function() { return ClassRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _class_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./class.component */ "./src/app/layout/class/class.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _class_component__WEBPACK_IMPORTED_MODULE_2__["ClassComponent"]
    }
];
var ClassRoutingModule = /** @class */ (function () {
    function ClassRoutingModule() {
    }
    ClassRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ClassRoutingModule);
    return ClassRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/class/class.component.html":
/*!***************************************************!*\
  !*** ./src/app/layout/class/class.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"top\"></div>\r\n<mat-card class=\"class_header\">\r\n  <mat-card-header>Class Schedule</mat-card-header>\r\n</mat-card>\r\n<div class=\"spacer_5\"></div>\r\n<div class=\"mb-20\" fxFlex fxLayout=\"row\" fxLayout.lt-md=\"column\" fxLayoutGap=\"10px\">\r\n    <div fxFlex>\r\n        <div class=\"left_forms_container\">\r\n            <table class=\"jadwal_kelas_trainee\" *ngIf=\"traineeFlag\">\r\n                <tr>\r\n                    <td class =\"wdth_20\">Nama Trainer:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"{{staff?.name}}\">\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Nomor Kelas:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"{{schedule?.class_code}}\">\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Nama Program:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"{{schedule?.program_code}}\">\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Topik Pembelajaran:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"{{material?.name}}\">\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>  \r\n                <br>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Materi Pembelajaran:</td>\r\n                    <td>\r\n                        {{material?.url}}<button mat-raised-button color=\"primary\" (click)=\"download(material.url)\">Download</button>\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n\r\n            <table class=\"jadwal_kelas_trainer\" *ngIf=\"trainerFlag\">\r\n                <tr>\r\n                    <td class =\"wdth_20\">NIP:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"NIP\" value=\"{{staff?.nip}}\" readonly>\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Nama Trainer:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"Nama\" value=\"{{staff?.name}}\" readonly>\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Nama Program:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"Nama Program\" value=\"{{schedule?.program_code}}\" readonly>\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Topik Hari ini:</td>\r\n                    <td>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"Nama Topik\" value=\"{{material?.name}}\" readonly>\r\n                        </mat-form-field>\r\n                    </td>\r\n                </tr>  \r\n                <br>\r\n                <tr>\r\n                    <td class =\"wdth_20\">Materi Pembelajaran:</td>\r\n                    <td>\r\n                        <div class=\"file-upload\">\r\n                            <input type=\"file\" (change)=\"onFileSelected($event)\" name=\"file\">\r\n                        </div>\r\n                        \r\n                    </td>\r\n                </tr>       \r\n            </table>\r\n            <button mat-raised-button color=\"primary\" class=\"w-101\" (click)=\"onUpload()\" *ngIf=\"trainerFlag\">Submit</button>\r\n        </div>\r\n\r\n    </div>\r\n    <div fxFlex *ngIf=\"progFlag\">\r\n        <div class=\"right_forms_container\">\r\n            <a>Jadwal Kelas</a>    \r\n            <table mat-table [dataSource]=\"dataSourceSchedule\" matSort class=\"mat-elevation-z8\">\r\n                    <!-- Name Column -->\r\n                    <ng-container matColumnDef=\"training_date\">\r\n                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Tanggal</th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.training_date | date:'mediumDate'}} </td>\r\n                    </ng-container>\r\n                    \r\n                    <!-- Weight Column -->\r\n                    <ng-container matColumnDef=\"material_code\">\r\n                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Topik </th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.material_code}} </td>\r\n                    </ng-container>\r\n    \r\n                     <!-- Symbol Column -->\r\n                    <ng-container matColumnDef=\"class_code\">\r\n                        <th mat-header-cell *matHeaderCellDef mat-sort-header> Ruang Kelas </th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.class_code}} </td>\r\n                    </ng-container>\r\n                    \r\n                    <tr mat-header-row *matHeaderRowDef=\"displayedColumnsSchedule\"></tr>\r\n                    <tr mat-row *matRowDef=\"let row; columns: displayedColumnsSchedule;\"></tr>\r\n                </table> \r\n            <mat-paginator [pageSizeOptions]=\"[5,10,15]\" showFirstLastButtons></mat-paginator>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"mb-20\" fxFlex fxLayout=\"row\" fxLayout.lt-md=\"column\" fxLayoutGap=\"10px\">\r\n     \r\n    <div fxFlex>\r\n            <div class=\"left_forms_container\">\r\n                <a>Peserta Training Hari Ini</a>   \r\n                <table mat-table [dataSource]=\"dataSourceStaff\" matSort class=\"mat-elevation-z8\">\r\n                        <!-- Name Column -->\r\n                        <ng-container matColumnDef=\"nip\">\r\n                            <th mat-header-cell *matHeaderCellDef mat-sort-header> NIP</th>\r\n                            <td mat-cell *matCellDef=\"let element\"> {{element.nip}} </td>\r\n                        </ng-container>\r\n                        \r\n                        <!-- Weight Column -->\r\n                        <ng-container matColumnDef=\"name\">\r\n                            <th mat-header-cell *matHeaderCellDef mat-sort-header> Nama </th>\r\n                            <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n                        </ng-container>\r\n\r\n                        <ng-container matColumnDef=\"division_name\">\r\n                                <th mat-header-cell *matHeaderCellDef mat-sort-header> Divisi </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.division_name}} </td>\r\n                            </ng-container>\r\n        \r\n                      \r\n                        <tr mat-header-row *matHeaderRowDef=\"displayedColumnsPeserta\"></tr>\r\n                        <tr mat-row *matRowDef=\"let row; columns: displayedColumnsPeserta;\"></tr>\r\n                    </table> \r\n                    <mat-paginator [pageSizeOptions]=\"[10,15,20]\" showFirstLastButtons></mat-paginator>\r\n            </div>\r\n    \r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./src/app/layout/class/class.component.scss":
/*!***************************************************!*\
  !*** ./src/app/layout/class/class.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#top {\n  height: 20px;\n  width: 100%; }\n\n.class_header {\n  padding: 12px 8px;\n  background-color: #c7cce6;\n  color: white; }\n\n.spacer_5 {\n  height: 5px;\n  width: 100%; }\n\n.mb-20 .left_forms_container, .mb-20 .right_forms_container {\n  box-shadow: 1px 3px 6px rgba(0, 0, 0, 0.2);\n  min-height: 400px;\n  background-color: white;\n  padding: 20px;\n  position: relative; }\n\n.mb-20 .left_forms_container a, .mb-20 .right_forms_container a {\n    margin-left: 20px;\n    margin-top: 10px; }\n\n.mb-20 table {\n  width: 100%;\n  box-shadow: 0 0 0 rgba(0, 0, 0, 0); }\n\n.w-101 {\n  font-family: Fontil Sans, monospace;\n  font-size: 1em;\n  position: relative;\n  width: 20%;\n  margin-left: 80%;\n  margin-top: 20px;\n  margin-bottom: 20px; }\n\n@media only screen and (max-width: 425px) {\n  .mb-20 .left_forms_container, .mb-20 .right_forms_container {\n    box-shadow: 1px 3px 6px rgba(0, 0, 0, 0.2);\n    min-height: 200px; }\n  .jadwal_kelas_trainee td, .jadwal_kelas_trainer td {\n    font-size: 0.8em; }\n    .jadwal_kelas_trainee td .mat-form-field, .jadwal_kelas_trainer td .mat-form-field {\n      max-width: 300px; }\n  .jadwal_kelas_trainee input, .jadwal_kelas_trainer input {\n    font-size: 0.85em; }\n  .mat-paginator {\n    font-size: 0.70em; }\n  .w-101 {\n    width: 20%;\n    margin-left: 75%;\n    margin-top: 20px; }\n  .mat-paginator-icon {\n    font-size: 0.7em; }\n  .mat-elevation-z8 th, .mat-elevation-z8 td {\n    font-size: 0.8em; }\n  .mat-paginator-range-actions .mat-paginator-range-label {\n    width: 20px; } }\n\n@media only screen and (max-width: 375px) {\n  .mat-elevation-z8 th, .mat-elevation-z8 td {\n    font-size: 0.7em; }\n  .w-101 {\n    width: 20%;\n    margin-left: 70%;\n    margin-top: 20px; } }\n\n@media only screen and (max-width: 320px) {\n  .left_forms_container, .right_forms_container {\n    min-height: 100px; }\n  .jadwal_kelas_trainee td, .jadwal_kelas_trainer td {\n    font-size: 0.7em; }\n    .jadwal_kelas_trainee td .mat-form-field, .jadwal_kelas_trainer td .mat-form-field {\n      max-width: 150px; }\n  .jadwal_kelas_trainee input, .jadwal_kelas_trainer input {\n    font-size: 0.75em; }\n  .mat-elevation-z8 th, .mat-elevation-z8 td {\n    font-size: 0.6em; }\n  .w-101 {\n    width: 20%;\n    margin-left: 60%;\n    margin-top: 20px; }\n  .mat-paginator {\n    font-size: 0.70em; }\n  .mat-paginator-icon {\n    font-size: 0.7em; }\n  .mat-paginator-range-actions .mat-paginator-range-label {\n    min-width: 20px !important; } }\n"

/***/ }),

/***/ "./src/app/layout/class/class.component.ts":
/*!*************************************************!*\
  !*** ./src/app/layout/class/class.component.ts ***!
  \*************************************************/
/*! exports provided: ClassComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassComponent", function() { return ClassComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_services_class_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/class.service */ "./src/app/services/class.service.ts");
/* harmony import */ var src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/staff.service */ "./src/app/services/staff.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var INQ_PESERTA = [];
var INQ_SCHEDULE = [];
var ClassComponent = /** @class */ (function () {
    function ClassComponent(http, classService, staffService) {
        var _this = this;
        this.http = http;
        this.classService = classService;
        this.staffService = staffService;
        this.paginator = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["QueryList"]();
        this.dataSourceStaff = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](INQ_PESERTA);
        this.dataSourceSchedule = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](INQ_SCHEDULE);
        this.selectedFile = null;
        this.displayedColumnsSchedule = ['training_date', 'material_code', 'class_code'];
        this.displayedColumnsPeserta = ['nip', 'name', 'division_name'];
        this.progFlag = true;
        this.nip = localStorage.getItem('nip');
        this.staffService.getStaffById(this.nip).subscribe(function (res) {
            _this.staff = res.body;
            if (_this.staff.program == 'NONPR') {
                _this.progFlag = false;
            }
            if (_this.staff.flag_trainee == true) {
                _this.traineeFlag = true;
                _this.trainerFlag = false;
                classService.getScheduleByProgram(_this.staff.program).subscribe(function (res) {
                    _this.schedule = res.body;
                    _this.classService.getMaterialById(_this.schedule.material_code).subscribe(function (res) {
                        _this.material = res.body;
                        _this.kodeMateri = _this.material.code;
                        _this.namaMateri = _this.material.name;
                        _this.tahunMateri = _this.material.year;
                    });
                    staffService.getStaffById(_this.schedule.trainer_nip).subscribe(function (res) {
                        _this.trainer = res.body;
                    });
                });
                staffService.getAllTrainingStaffsByProgram(_this.staff.program).subscribe(function (res) {
                    _this.pesertaTraining = res.body;
                    var INQ_PESERTA = res.body;
                    _this.dataSourceStaff = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](INQ_PESERTA);
                    _this.dataSourceStaff.paginator = _this.paginator.toArray()[0];
                });
            }
            else if (_this.staff.flag_trainer == true) {
                _this.trainerFlag = true;
                _this.traineeFlag = false;
                classService.getScheduleByTrainerNip(_this.staff.nip).subscribe(function (res) {
                    _this.schedule = res.body;
                    _this.classService.getMaterialById(_this.schedule.material_code).subscribe(function (res) {
                        _this.material = res.body;
                        _this.kodeMateri = _this.material.code;
                        _this.namaMateri = _this.material.name;
                        _this.tahunMateri = _this.material.year;
                    });
                    _this.staffService.getAllTrainingStaffsByProgram(_this.schedule.program_code).subscribe(function (res) {
                        _this.pesertaTraining = res.body;
                        var INQ_PESERTA = res.body;
                        _this.dataSourceStaff = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](INQ_PESERTA);
                        _this.dataSourceStaff.paginator = _this.paginator.toArray()[0];
                    });
                });
            }
            _this.classService.getListScheduleByProgram(_this.staff.program).subscribe(function (res) {
                _this.scheduleList = res.body;
                var INQ_SCHEDULE = res.body;
                _this.dataSourceSchedule = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](INQ_SCHEDULE);
                _this.dataSourceSchedule.paginator = _this.paginator.toArray()[1];
                console.log(_this.scheduleList);
            });
        });
    }
    ClassComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    };
    ClassComponent.prototype.ngOnInit = function () {
        this.trainerFlag = false;
        this.traineeFlag = false;
        var top = document.getElementById('top');
        if (top !== null) {
            top.scrollIntoView();
            top = null;
        }
    };
    ClassComponent.prototype.ngAfterViewInit = function () {
    };
    ClassComponent.prototype.onFileSelected = function (event) {
        this.selectedFile = event.target.files[0];
        var target = (event.target);
        if (target.files.length !== 1)
            throw new Error('Cannot use multiple files');
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(target.files[0]);
        };
        reader.readAsBinaryString(target.files[0]);
    };
    ClassComponent.prototype.onUpload = function () {
        var file = new FormData();
        file.append('file', this.selectedFile);
        console.log(file.get("file"));
        this.material.code = this.kodeMateri;
        this.material.name = this.namaMateri;
        this.material.year = this.tahunMateri;
        this.material.url = this.selectedFile.name;
        this.classService.uploadMaterial(file)
            .subscribe(function (event) {
            if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].UploadProgress) {
                console.log('Upload Progress : ' + Math.round(event.loaded / event.total * 100) + '%');
            }
            else if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].Response) {
                console.log(event);
            }
        });
        this.classService.updateMaterial(this.material).subscribe(function (res) {
            console.log(res.body);
        });
    };
    ClassComponent.prototype.download = function (url) {
        this.classService.downloadMaterial(url).subscribe(function (res) {
            var newBlob = new Blob([res], { type: "application" });
            // For other browsers: Create a link pointing to the ObjectURL containing the blob.
            var data = window.URL.createObjectURL(newBlob);
            var link = document.createElement('a');
            link.href = data;
            link.download = url;
            // this is necessary as link.click() does not work on the latest firefox
            link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
            setTimeout(function () {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
                link.remove();
            }, 100);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChildren"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", Object)
    ], ClassComponent.prototype, "paginator", void 0);
    ClassComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-class',
            template: __webpack_require__(/*! ./class.component.html */ "./src/app/layout/class/class.component.html"),
            styles: [__webpack_require__(/*! ./class.component.scss */ "./src/app/layout/class/class.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_services_class_service__WEBPACK_IMPORTED_MODULE_3__["ClassService"], src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_4__["StaffService"]])
    ], ClassComponent);
    return ClassComponent;
}());



/***/ }),

/***/ "./src/app/layout/class/class.module.ts":
/*!**********************************************!*\
  !*** ./src/app/layout/class/class.module.ts ***!
  \**********************************************/
/*! exports provided: ClassModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassModule", function() { return ClassModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _class_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./class-routing.module */ "./src/app/layout/class/class-routing.module.ts");
/* harmony import */ var _class_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./class.component */ "./src/app/layout/class/class.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ClassModule = /** @class */ (function () {
    function ClassModule() {
    }
    ClassModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _class_routing_module__WEBPACK_IMPORTED_MODULE_4__["ClassRoutingModule"]
            ],
            declarations: [_class_component__WEBPACK_IMPORTED_MODULE_5__["ClassComponent"]]
        })
    ], ClassModule);
    return ClassModule;
}());



/***/ }),

/***/ "./src/app/services/class.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/class.service.ts ***!
  \*******************************************/
/*! exports provided: ClassService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassService", function() { return ClassService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ClassService = /** @class */ (function () {
    function ClassService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'class/';
        this.token = localStorage.getItem('token');
    }
    ClassService.prototype.getListClass = function () {
        return this.http.get(this.url + 'class/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListAvailableClass = function () {
        return this.http.get(this.url + 'class/available', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListUnavailableClass = function () {
        return this.http.get(this.url + 'class/unavailable', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getClassByCode = function (code) {
        return this.http.get(this.url + 'class/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createClass = function (classEntity) {
        return this.http.post(this.url + 'class/' + classEntity, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateClass = function (classEntity) {
        return this.http.put(this.url + 'class/' + classEntity, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListMaterial = function () {
        return this.http.get(this.url + 'material/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getMaterialById = function (code) {
        return this.http.get(this.url + 'material/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createMaterial = function (material) {
        return this.http.post(this.url + 'material/', material, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateMaterial = function (material) {
        return this.http.put(this.url + 'material/', material, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListProgram = function () {
        return this.http.get(this.url + 'program/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getProgramByCode = function (code) {
        return this.http.get(this.url + 'program/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createProgram = function (program) {
        return this.http.post(this.url + 'program/', program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateProgram = function (program) {
        return this.http.put(this.url + 'program/', program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.deleteProgram = function (code) {
        return this.http.delete(this.url + 'program/delete/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListCompetency = function () {
        return this.http.get(this.url + 'competency/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListCompetencybyCode = function (materialCode) {
        return this.http.get(this.url + 'competency/material/' + materialCode, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getCompetencyByTrainerNip = function (trainerNip) {
        return this.http.get(this.url + 'competency/detail/' + trainerNip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getCompetencyByCode = function (material_code) {
        return this.http.get(this.url + 'competency/material/' + material_code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createCompetency = function (competency) {
        return this.http.post(this.url + 'competency/', competency, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListSchedule = function () {
        return this.http.get(this.url + 'schedule/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListScheduleByProgram = function (program) {
        return this.http.get(this.url + 'schedule/all/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleById = function (id) {
        return this.http.get(this.url + 'schedule/detail/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByCode = function (code) {
        return this.http.get(this.url + 'schedule/class/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByProgram = function (program) {
        return this.http.get(this.url + 'schedule/program/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByTrainerNip = function (nip) {
        return this.http.get(this.url + 'schedule/trainer/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createSchedule = function (schedule) {
        return this.http.post(this.url + 'schedule/', schedule, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateSchedule = function (schedule) {
        return this.http.put(this.url + 'schedule/', schedule, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createScheduleNonProgram = function (scheduleNonProgram) {
        return this.http.post(this.url + 'schedule/nonprogram', scheduleNonProgram, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.uploadMaterial = function (file) {
        return this.http.post(this.url + 'material/upload/', file, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            reportProgress: true,
            observe: 'events'
        });
    };
    ClassService.prototype.downloadMaterial = function (filename) {
        return this.http.get(this.url + 'material/download/' + filename, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'blob'
        });
    };
    ClassService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ClassService);
    return ClassService;
}());



/***/ }),

/***/ "./src/app/services/staff.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/staff.service.ts ***!
  \*******************************************/
/*! exports provided: StaffService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffService", function() { return StaffService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StaffService = /** @class */ (function () {
    function StaffService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'staff/';
        this.token = localStorage.getItem('token');
    }
    StaffService.prototype.getAllStaffs = function () {
        return this.http.get(this.url + 'all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainingStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/training/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainees = function () {
        return this.http.get(this.url + 'trainees', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainers = function () {
        return this.http.get(this.url + 'trainers', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllDivisions = function () {
        return this.http.get(this.url + 'division', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getStaffById = function (nip) {
        return this.http.get(this.url + 'detail/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.createStaff = function (staff) {
        console.log(staff);
        return this.http.post(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.updateStaff = function (staff) {
        console.log(staff);
        return this.http.put(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.deleteStaff = function (nip) {
        return this.http.delete(this.url + 'delete/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainingBadge = function (nip) {
        return this.http.get(this.url + 'training/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainerBadge = function (nip) {
        return this.http.get(this.url + 'trainer/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTrainerToday = function () {
        return this.http.get(this.url + 'trainer/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTraineeToday = function () {
        return this.http.get(this.url + 'trainee/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StaffService);
    return StaffService;
}());



/***/ })

}]);
//# sourceMappingURL=class-class-module.js.map