(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin-page/admin-page.module": [
		"./src/app/layout/admin-page/admin-page.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"admin-page-admin-page-module~create-schedule-create-schedule-module",
		"common",
		"admin-page-admin-page-module"
	],
	"./black-page/black-page.module": [
		"./src/app/layout/black-page/black-page.module.ts",
		"black-page-black-page-module"
	],
	"./blank-page/blank-page.module": [
		"./src/app/layout/blank-page/blank-page.module.ts",
		"blank-page-blank-page-module"
	],
	"./booking-detail/booking-detail.module": [
		"./src/app/layout/booking-detail/booking-detail.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"booking-detail-booking-detail-module"
	],
	"./charts/charts.module": [
		"./src/app/layout/charts/charts.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"charts-charts-module"
	],
	"./class/class.module": [
		"./src/app/layout/class/class.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"class-class-module"
	],
	"./confirmation-page/confirmation-page.module": [
		"./src/app/layout/confirmation-page/confirmation-page.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"confirmation-page-confirmation-page-module"
	],
	"./create-schedule/create-schedule.module": [
		"./src/app/layout/create-schedule/create-schedule.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~create-schedule-create-schedule-module",
		"common",
		"create-schedule-create-schedule-module"
	],
	"./dashboard-vendor/dashboard-vendor.module": [
		"./src/app/layout/dashboard-vendor/dashboard-vendor.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"dashboard-vendor-dashboard-vendor-module"
	],
	"./dashboard/dashboard.module": [
		"./src/app/layout/dashboard/dashboard.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"dashboard-dashboard-module"
	],
	"./food-stall/food-stall.module": [
		"./src/app/layout/food-stall/food-stall.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"food-stall-food-stall-module"
	],
	"./forms/forms.module": [
		"./src/app/layout/forms/forms.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"forms-forms-module"
	],
	"./grid/grid.module": [
		"./src/app/layout/grid/grid.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"grid-grid-module"
	],
	"./layout/layout.module": [
		"./src/app/layout/layout.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"layout-layout-module"
	],
	"./login/login.module": [
		"./src/app/login/login.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"login-login-module"
	],
	"./material-components/material-components.module": [
		"./src/app/layout/material-components/material-components.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"material-components-material-components-module"
	],
	"./program-detail/program-detail.module": [
		"./src/app/layout/program-detail/program-detail.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"program-detail-program-detail-module"
	],
	"./schedule-detail/schedule-detail.module": [
		"./src/app/layout/schedule-detail/schedule-detail.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"schedule-detail-schedule-detail-module"
	],
	"./shuttle/shuttle.module": [
		"./src/app/layout/shuttle/shuttle.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"admin-page-admin-page-module~charts-charts-module~class-class-module~confirmation-page-confirmation-~af52d161",
		"common",
		"shuttle-shuttle-module"
	],
	"./staff-detail/staff-detail.module": [
		"./src/app/layout/staff-detail/staff-detail.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"staff-detail-staff-detail-module"
	],
	"./tables/tables.module": [
		"./src/app/layout/tables/tables.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"tables-tables-module"
	],
	"./vendor-detail/vendor-detail.module": [
		"./src/app/layout/vendor-detail/vendor-detail.module.ts",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~charts-charts-module~class-class-m~e8ceb1ea",
		"admin-page-admin-page-module~booking-detail-booking-detail-module~class-class-module~confirmation-pa~36660312",
		"common",
		"vendor-detail-vendor-detail-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error('Cannot find module "' + req + '".');
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/guard/auth.guard */ "./src/app/shared/guard/auth.guard.ts");
/* harmony import */ var _shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/guard/role-guard.service */ "./src/app/shared/guard/role-guard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        loadChildren: './layout/layout.module#LayoutModule',
        canActivate: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]]
    },
    {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            providers: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"], _shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  -webkit-user-select: none;\n  -moz-user-select: -moz-none;\n  -ms-user-selet: none;\n  -moz-user-select: none;\n   -ms-user-select: none;\n       user-select: none; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(translate) {
        this.translate = translate;
        translate.setDefaultLang('en');
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: createTranslateLoader, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function() { return createTranslateLoader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/esm5/ngx-translate-http-loader.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// AoT requires an exported function for factories
var createTranslateLoader = function (http) {
    // for development
    /*return new TranslateHttpLoader(
        http,
        '/start-javascript/sb-admin-material/master/dist/assets/i18n/',
        '.json'
    );*/
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_7__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_0__["LayoutModule"],
                _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_1__["OverlayModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateLoader"],
                        useFactory: createTranslateLoader,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]]
                    }
                })
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/shared/guard/auth.guard.ts":
/*!********************************************!*\
  !*** ./src/app/shared/guard/auth.guard.ts ***!
  \********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/guard/role-guard.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/guard/role-guard.service.ts ***!
  \****************************************************/
/*! exports provided: RoleGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleGuardService", function() { return RoleGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RoleGuardService = /** @class */ (function () {
    function RoleGuardService(router) {
        this.router = router;
    }
    RoleGuardService.prototype.canActivate = function (next, state) {
        var userRole = localStorage.getItem('role');
        var allowedRole = next.data.role;
        if (userRole == allowedRole) {
            return true;
        }
        // navigate to not found page
        if (userRole == 'ROLE_VENDOR') {
            this.router.navigate(['/dashboard-vendor']);
        }
        else if (userRole == 'ROLE_ADMIN') {
            this.router.navigate(['/admin-page']);
        }
        else if (userRole == 'ROLE_STAFF') {
            this.router.navigate(['/dashboard']);
        }
        else if (userRole == 'ROLE_BO') {
            this.router.navigateByUrl('https://10.20.212.231');
        }
        else {
            this.router.navigate(['/login']);
        }
        return false;
    };
    RoleGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], RoleGuardService);
    return RoleGuardService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiUrl: 'http://localhost:8762/'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Repository\view_bli\bli_view\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map