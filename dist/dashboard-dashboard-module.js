(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./src/app/error/show-errors.component.ts":
/*!************************************************!*\
  !*** ./src/app/error/show-errors.component.ts ***!
  \************************************************/
/*! exports provided: ShowErrorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowErrorsComponent", function() { return ShowErrorsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShowErrorsComponent = /** @class */ (function () {
    function ShowErrorsComponent() {
    }
    ShowErrorsComponent_1 = ShowErrorsComponent;
    ShowErrorsComponent.prototype.shouldShowErrors = function () {
        return this.control &&
            this.control.errors &&
            (this.control.dirty || this.control.touched);
    };
    ShowErrorsComponent.prototype.listOfErrors = function () {
        var _this = this;
        return Object.keys(this.control.errors)
            .map(function (field) { return _this.getMessage(field, _this.control.errors[field]); });
    };
    ShowErrorsComponent.prototype.getMessage = function (type, params) {
        return ShowErrorsComponent_1.errorMessages[type](params);
    };
    ShowErrorsComponent.errorMessages = {
        'required': function () { return 'This field is required'; },
        'minlength': function (params) { return 'The min number of characters is ' + params.requiredLength; },
        'maxlength': function (params) { return 'The max allowed number of characters is ' + params.requiredLength; },
        'ipaddress': function (params) { return params.message; },
        'pattern': function (params) { return 'Numbers only'; }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ShowErrorsComponent.prototype, "control", void 0);
    ShowErrorsComponent = ShowErrorsComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-show-errors',
            template: "\n        <div *ngIf=\"shouldShowErrors()\">\n            <p style=\"margin-left:10px; font-size:0.8em; margin-top:-10px;\" *ngFor=\"let error of listOfErrors()\">{{error}}</p>\n        </div>\n    "
        })
    ], ShowErrorsComponent);
    return ShowErrorsComponent;
    var ShowErrorsComponent_1;
}());



/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title *ngIf=\"listData.data.location != '100'\">Booking {{listData.data.food.description | titlecase}}?</h2>\r\n<h2 mat-dialog-title *ngIf=\"listData.data.location == '100'\">Booking Prasmanan?</h2>\r\n\r\n<mat-dialog-actions>\r\n    <button class=\"mat-raised-button\"(click)=\"close()\">No</button>\r\n    <button class=\"mat-raised-button mat-primary\"(click)=\"save()\">Yes</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n"

/***/ }),

/***/ "./src/app/layout/custom-dialog/custom-dialog.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/custom-dialog/custom-dialog.component.ts ***!
  \*****************************************************************/
/*! exports provided: CustomDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomDialogComponent", function() { return CustomDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var CustomDialogComponent = /** @class */ (function () {
    function CustomDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.listData = data;
    }
    CustomDialogComponent.prototype.ngOnInit = function () {
    };
    CustomDialogComponent.prototype.save = function () {
        this.dialogRef.close(this.listData);
    };
    CustomDialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    CustomDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-custom-dialog',
            template: __webpack_require__(/*! ./custom-dialog.component.html */ "./src/app/layout/custom-dialog/custom-dialog.component.html"),
            styles: [__webpack_require__(/*! ./custom-dialog.component.scss */ "./src/app/layout/custom-dialog/custom-dialog.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], CustomDialogComponent);
    return CustomDialogComponent;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/layout/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.html":
/*!***********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"top\"></div>\r\n\r\n<div class=\"mb-20\" fxLayout=\"row\" fxLayout.lt-md=\"column\" fxFlex fxLayoutGap=\"20px\">\r\n    <div fxFlex>\r\n        <mat-card class=\"next_class_schedule_container\">\r\n            <img src=\"assets/images/ruang_kelas.jpg\" class=\"class-img\" alt=\"\">\r\n            <div class=\"dark_overlay\">\r\n                    \r\n            </div>\r\n            <p>Nama Ruang: {{schedule?.class_code}}</p>\r\n            <p>Judul Materi: {{material?.name}}</p>\r\n            <p>Jam: 8:30 - 16:30</p>\r\n            <a>Today Class Schedule</a>\r\n        </mat-card>\r\n    </div>\r\n    <div fxFlex>\r\n        <mat-card class=\"next_shuttle_schedule_container\">\r\n            <img src=\"assets/images/bus_stop.jpg\" class=\"shuttle-img\" alt=\"\">\r\n            <div class=\"dark_overlay\">\r\n                    \r\n            </div>\r\n            <p>Jam: 06:00 - 06:30</p>\r\n            <a>Next Shuttle Schedule</a>\r\n        </mat-card>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n<div class=\"mb-20\" fxLayout=\"row\" fxLayout.lt-md=\"column\" fxFlex fxLayoutGap=\"20px\">\r\n    <div class=\"left-container\">\r\n        <mat-card>  \r\n            <mat-card-header>\r\n                <mat-card-content>\r\n                    <h2>My Profile</h2>\r\n                    <img src=\"assets/images/kokoh.png\" class=\"login-img\" alt=\"\">\r\n                    <p>Nama : {{staff?.name}}</p>\r\n                    <p>Program: {{staff?.program}}</p>\r\n                    <p>Divisi : {{staff?.division.name}}</p>\r\n                    <p>Status di BLI : {{staff?.flag_trainee === true? 'Trainee' : staff?.flag_trainer === true? 'Trainer' : 'INACTIVE'}}</p>\r\n                </mat-card-content>\r\n            </mat-card-header>          \r\n        </mat-card>\r\n    </div>\r\n\r\n    <div class=\"right-container\">\r\n        <div fxFlex>\r\n            <mat-card>\r\n                <img src=\"assets/images/medal_bg.jpg\" class=\"bg_img\" alt=\"\">\r\n                <div class=\"dark_overlay\">\r\n                \r\n                </div>\r\n                <img src=\"assets/images/medal_bronze.png\" id=\"medal\" alt=\"\" *ngIf=\"enthusiast == 1\">\r\n                <img src=\"assets/images/medal_silver.png\" id=\"medal\" alt=\"\" *ngIf=\"enthusiast == 2\">\r\n                <img src=\"assets/images/medal_gold.png\" id=\"medal\" alt=\"\" *ngIf=\"enthusiast == 3\">\r\n                <a>Food Enthusiast</a>\r\n            </mat-card>\r\n            <mat-card style=\"margin-top:20px;\">\r\n                <img src=\"assets/images/medal_bg.jpg\" class=\"bg_img\" alt=\"\">\r\n                <div class=\"dark_overlay\">\r\n                \r\n                </div>\r\n                <img src=\"assets/images/medal_bronze.png\" id=\"medal\" alt=\"\" *ngIf=\"rare == 1\">\r\n                <img src=\"assets/images/medal_silver.png\" id=\"medal\" alt=\"\" *ngIf=\"rare == 2\">\r\n                <img src=\"assets/images/medal_gold.png\" id=\"medal\" alt=\"\" *ngIf=\"rare == 3\">\r\n                <a>Rare Food Hunter</a>\r\n            </mat-card>\r\n        </div>\r\n        <div fxFlex style=\"margin-left:20px;\">\r\n            <mat-card>\r\n                <img src=\"assets/images/medal_bg.jpg\" class=\"bg_img\" alt=\"\">\r\n                <div class=\"dark_overlay\">\r\n                \r\n                </div>\r\n                <img src=\"assets/images/medal_bronze.png\" id=\"medal\" alt=\"\" *ngIf=\"healthy == 1\">\r\n                <img src=\"assets/images/medal_silver.png\" id=\"medal\" alt=\"\" *ngIf=\"healthy == 2\">\r\n                <img src=\"assets/images/medal_gold.png\" id=\"medal\" alt=\"\" *ngIf=\"healthy == 3\">\r\n                <a>Healthy Food for Healthy Life</a>\r\n            </mat-card>\r\n            <mat-card style=\"margin-top:20px;\">\r\n                <img src=\"assets/images/medal_bg.jpg\" class=\"bg_img\" alt=\"\">\r\n                <div class=\"dark_overlay\">\r\n                \r\n                </div>\r\n                <img src=\"assets/images/medal_bronze.png\" id=\"medal\" alt=\"\" *ngIf=\"training == 1\">\r\n                <img src=\"assets/images/medal_silver.png\" id=\"medal\" alt=\"\" *ngIf=\"training == 2\">\r\n                <img src=\"assets/images/medal_gold.png\" id=\"medal\" alt=\"\" *ngIf=\"training == 3\">\r\n                <a>BLI Honor Guest</a>\r\n            </mat-card>\r\n        </div>\r\n        <div fxFlex style=\"margin-left:20px;\">\r\n            <mat-card>\r\n                <img src=\"assets/images/medal_bg.jpg\" class=\"bg_img\" alt=\"\">\r\n                <div class=\"dark_overlay\">\r\n                \r\n                </div>\r\n                <img src=\"assets/images/medal_bronze.png\" id=\"medal\" alt=\"\" *ngIf=\"feast == 1\">\r\n                <img src=\"assets/images/medal_silver.png\" id=\"medal\" alt=\"\" *ngIf=\"feast == 2\">\r\n                <img src=\"assets/images/medal_gold.png\" id=\"medal\" alt=\"\" *ngIf=\"feast == 3\">\r\n                <a>Feast at BLI</a>\r\n            </mat-card>\r\n            <mat-card style=\"margin-top:20px;\">\r\n                <img src=\"assets/images/medal_bg.jpg\" class=\"bg_img\" alt=\"\">\r\n                <div class=\"dark_overlay\">\r\n                \r\n                </div>\r\n                <img src=\"assets/images/medal_bronze.png\" id=\"medal\" alt=\"\" *ngIf=\"trainer == 1\">\r\n                <img src=\"assets/images/medal_silver.png\" id=\"medal\" alt=\"\" *ngIf=\"trainer == 2\">\r\n                <img src=\"assets/images/medal_gold.png\" id=\"medal\" alt=\"\" *ngIf=\"trainer == 3\">\r\n                <a>Share to Learn</a>\r\n            </mat-card>\r\n        </div>\r\n    </div>\r\n    \r\n    \r\n    \r\n</div>\r\n\r\n<div class=\"bottom_container\" fxLayout=\"row\" fxLayout.lt-md=\"column\" fxFlex fxLayoutGap=\"20px\">\r\n   \r\n    <div fxFlex class=\"flip3D\">\r\n        <mat-card class=\"thefront\">\r\n                <mat-icon class=\"sidenav-icon\">favorite</mat-icon>\r\n                <p class=\"words\" style=\"font-size: 1.2em\">{{allFavoriteArray[0]}}</p>\r\n            <a>Your favorite food stall</a>\r\n        </mat-card>\r\n        <mat-card class=\"theback\">\r\n                <mat-icon class=\"sidenav-icon\">favorite</mat-icon>\r\n                <p class=\"words\">{{allFavoriteArray[0]}}</p>\r\n                <p class=\"words\">{{allFavoriteArray[1]}}</p>\r\n                <p class=\"words\">{{allFavoriteArray[2]}}</p>\r\n        </mat-card>\r\n    </div>\r\n\r\n    <div fxFlex class=\"flip3D\">\r\n        <mat-card class=\"thefront\">\r\n                <mat-icon class=\"sidenav-icon\">beenhere</mat-icon>\r\n                <p class=\"words\"style=\"font-size: 1.2em\">{{allVisitArray[0]}}</p>\r\n            <a>Visit us again!</a>\r\n        </mat-card>\r\n        <mat-card class=\"theback\">\r\n                <mat-icon class=\"sidenav-icon\">beenhere</mat-icon>\r\n                <p class=\"words\">{{allVisitArray[0]}}</p>\r\n                <p class=\"words\">{{allVisitArray[1]}}</p>\r\n                <p class=\"words\">{{allVisitArray[2]}}</p>\r\n        </mat-card>\r\n    </div>\r\n    <div fxFlex>\r\n        <mat-card>\r\n            <mat-icon class=\"sidenav-icon\">format_list_numbered</mat-icon>\r\n                <p class=\"numb\">{{totalBook}}</p>\r\n            <a>Your total book record</a>\r\n        </mat-card>\r\n    </div>\r\n\r\n    <div fxFlex class=\"flip3D\">\r\n        <mat-card class=\"thefront\">\r\n            <mat-icon class=\"sidenav-icon\">trending_up</mat-icon>\r\n            <p class=\"words\" style=\"font-size: 1.2em\">{{allTrendingArray[0]}}</p>\r\n            <a>BLI trending pick</a>\r\n        </mat-card>\r\n        <mat-card class=\"theback\">\r\n            <mat-icon class=\"sidenav-icon\">trending_up</mat-icon>\r\n            <p class=\"words\">{{allTrendingArray[0]}}</p>\r\n            <p class=\"words\">{{allTrendingArray[1]}}</p>\r\n            <p class=\"words\">{{allTrendingArray[2]}}</p>\r\n        </mat-card>\r\n    </div>\r\n\r\n    <div fxFlex class=\"flip3D\">\r\n        <mat-card class=\"thefront\">\r\n            <mat-icon class=\"sidenav-icon\">thumb_up</mat-icon>\r\n            <p class=\"words\" style=\"font-size: 1.2em\">{{topRec.result[0] | titlecase}}</p>\r\n            <a>Recommended pick</a>\r\n        </mat-card>\r\n        <mat-card class=\"theback\">\r\n            <mat-icon class=\"sidenav-icon\">thumb_up</mat-icon>\r\n            <p *ngFor=\"let rec of topRec.result\" class=\"words\">{{rec | titlecase}}</p>\r\n        </mat-card>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"mb-20\" fxLayout=\"row\" fxLayout.lt-md=\"column\" fxFlex fxLayoutGap=\"20px\">\r\n    <div fxFlex *ngFor=\"let item of places\">\r\n        <mat-card class=\"news_card\">\r\n            <img mat-card-image [src]=\"item.imgSrc\">\r\n            <mat-card-header style=\"justify-content: center\">\r\n                <mat-card-title>\r\n                    <h3 class=\"m-0\">{{item.place}}</h3>\r\n                </mat-card-title>\r\n            </mat-card-header>\r\n            <mat-card-content>\r\n                <p>\r\n                    {{item.description}}\r\n                </p>\r\n            </mat-card-content>\r\n\r\n            <mat-card-actions>\r\n                <button mat-button>{{item.charge}}</button>\r\n                <button mat-button style=\"pointer-events: none\">\r\n                    <mat-icon class=\"nav-icon\">location_on</mat-icon> {{item.location}}\r\n                </button>\r\n            </mat-card-actions>\r\n        </mat-card>\r\n    </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#top {\n  height: 20px;\n  width: 100%; }\n\n.next_class_schedule_container, .next_shuttle_schedule_container {\n  position: relative;\n  height: 120px;\n  overflow: hidden;\n  padding: 0; }\n\n.next_class_schedule_container p, .next_shuttle_schedule_container p {\n    position: relative;\n    text-align: left;\n    color: white;\n    padding-left: 10px;\n    z-index: 3;\n    line-height: 1px; }\n\n.next_class_schedule_container .class-img, .next_shuttle_schedule_container .class-img {\n    position: absolute;\n    top: 0;\n    left: 0;\n    margin: 0;\n    -o-object-fit: cover;\n       object-fit: cover;\n    border-radius: 2px;\n    width: 100%;\n    height: 100%; }\n\n.next_class_schedule_container .shuttle-img, .next_shuttle_schedule_container .shuttle-img {\n    position: absolute;\n    top: 0;\n    margin: 0;\n    left: 0;\n    -o-object-fit: cover;\n       object-fit: cover;\n    border-radius: 2px;\n    width: 100%;\n    height: 100%; }\n\n.next_class_schedule_container .dark_overlay, .next_shuttle_schedule_container .dark_overlay {\n    background: linear-gradient(to top, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.2));\n    position: absolute;\n    z-index: 1;\n    width: 100%;\n    height: 100%; }\n\n.next_class_schedule_container a, .next_shuttle_schedule_container a {\n    right: 20px;\n    bottom: 20px;\n    font-size: 1.2em;\n    position: absolute;\n    margin: auto;\n    z-index: 3;\n    color: white; }\n\n.right-container {\n  display: flex;\n  width: 100%; }\n\n.right-container .mat-card {\n    position: relative;\n    overflow: hidden;\n    max-width: 200px;\n    padding: 0;\n    height: 90px; }\n\n.right-container .mat-card img {\n      position: absolute;\n      width: 250px;\n      left: -50px;\n      margin: auto; }\n\n.right-container .mat-card #medal {\n      position: absolute;\n      right: 0px;\n      left: 60px;\n      top: 0px;\n      z-index: 4;\n      width: 102px;\n      height: 68px; }\n\n.right-container .mat-card .dark_overlay {\n      background: linear-gradient(to top, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.2));\n      position: absolute;\n      z-index: 1;\n      width: 100%;\n      height: 100%; }\n\n.right-container .mat-card a {\n      right: 10px;\n      bottom: 10px;\n      font-size: 0.7em;\n      position: absolute;\n      margin: auto;\n      z-index: 3;\n      color: white; }\n\n.left-container {\n  width: 100%;\n  position: relative; }\n\n.left-container .mat-card {\n    height: 160px;\n    padding-top: 15px; }\n\n.left-container h2 {\n    top: 0;\n    font-size: 1.2em; }\n\n.left-container p {\n    top: -5px;\n    left: 130px; }\n\n.left-container h2, .left-container p {\n    position: relative;\n    text-align: left; }\n\n.left-container img {\n    position: absolute;\n    border: 2px solid black;\n    width: 100px;\n    left: 35px;\n    top: 70px;\n    height: 120px;\n    margin-top: 0; }\n\n.bottom_container {\n  display: flex;\n  width: 100%;\n  margin-bottom: 20px; }\n\n.bottom_container .mat-card {\n    height: 110px;\n    padding: 0;\n    top: 0; }\n\n.bottom_container .flip3D {\n    position: relative;\n    height: 110px; }\n\n.bottom_container .flip3D > .thefront {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    padding: 0;\n    -webkit-transform: perspective(600px) rotateY(0deg);\n            transform: perspective(600px) rotateY(0deg);\n    -webkit-backface-visibility: hidden;\n            backface-visibility: hidden;\n    transition: -webkit-transform .5s linear 0s;\n    transition: transform .5s linear 0s;\n    transition: transform .5s linear 0s, -webkit-transform .5s linear 0s; }\n\n.bottom_container .flip3D:hover > .thefront {\n    -webkit-transform: perspective(600px) rotateY(-180deg);\n            transform: perspective(600px) rotateY(-180deg); }\n\n.bottom_container .flip3D > .theback {\n    width: 100%;\n    padding: 0;\n    height: 100%;\n    position: absolute;\n    -webkit-transform: perspective(600px) rotateY(180deg);\n            transform: perspective(600px) rotateY(180deg);\n    -webkit-backface-visibility: hidden;\n            backface-visibility: hidden;\n    transition: -webkit-transform .5s linear 0s;\n    transition: transform .5s linear 0s;\n    transition: transform .5s linear 0s, -webkit-transform .5s linear 0s; }\n\n.bottom_container .flip3D:hover > .theback {\n    -webkit-transform: perspective(600px) rotateY(0deg);\n            transform: perspective(600px) rotateY(0deg); }\n\n.bottom_container .mat-icon {\n    font-size: 3em;\n    color: rgba(200, 200, 200, 0.664);\n    left: 10px;\n    position: absolute;\n    top: 8px; }\n\n.bottom_container .words {\n    position: relative;\n    width: 100px;\n    text-align: left;\n    left: 70px;\n    font-size: 0.8em; }\n\n.bottom_container .numb {\n    position: relative;\n    width: 100px;\n    text-align: left;\n    left: 70px;\n    top: 20px;\n    font-size: 1.8em;\n    margin: 0px 0 0 0; }\n\n.bottom_container a {\n    right: 10px;\n    bottom: 10px;\n    font-size: 0.8em;\n    position: absolute;\n    margin: auto;\n    z-index: 3;\n    color: black; }\n\n.profile-header {\n  background: linear-gradient(10deg, #b3bbff, #8476ff);\n  transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);\n  display: block;\n  position: relative;\n  padding: 24px;\n  border-radius: 2px;\n  height: 30px;\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  color: #fff;\n  font-family: Fontil Sans, monospace; }\n\n.profile-container {\n  display: flex;\n  background: linear-gradient(50deg, #eeeeee, #ffffff);\n  transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);\n  position: relative;\n  padding: 24px;\n  border-radius: 2px;\n  height: 250px;\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  color: #fff;\n  font-family: Fontil Sans, monospace; }\n\n.profile-header, .profile-container {\n  -webkit-animation-name: bounceIn;\n          animation-name: bounceIn;\n  -webkit-animation-duration: 0.5s;\n          animation-duration: 0.5s;\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards;\n  opacity: 0;\n  -webkit-animation-delay: 1.6s;\n          animation-delay: 1.6s; }\n\n@-webkit-keyframes bounceIn {\n  from, 20%, 40%, 60%, 80%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n            transform: scale3d(0.3, 0.3, 0.3); }\n  20% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n            transform: scale3d(1.1, 1.1, 1.1); }\n  40% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n            transform: scale3d(0.9, 0.9, 0.9); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\n            transform: scale3d(1.03, 1.03, 1.03); }\n  80% {\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\n            transform: scale3d(0.97, 0.97, 0.97); }\n  to {\n    opacity: 1;\n    -webkit-transform: scale3d(1, 1, 1);\n            transform: scale3d(1, 1, 1); } }\n\n@keyframes bounceIn {\n  from, 20%, 40%, 60%, 80%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n            transform: scale3d(0.3, 0.3, 0.3); }\n  20% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n            transform: scale3d(1.1, 1.1, 1.1); }\n  40% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n            transform: scale3d(0.9, 0.9, 0.9); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\n            transform: scale3d(1.03, 1.03, 1.03); }\n  80% {\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\n            transform: scale3d(0.97, 0.97, 0.97); }\n  to {\n    opacity: 1;\n    -webkit-transform: scale3d(1, 1, 1);\n            transform: scale3d(1, 1, 1); } }\n\n.medal {\n  background: linear-gradient(60deg, #b3bbff, #8476ff);\n  transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);\n  display: block;\n  position: relative;\n  padding: 24px;\n  border-radius: 5px;\n  height: 95px;\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  color: #fff;\n  font-family: Fontil Sans, monospace; }\n\n.medal00_0, .medal00_1 {\n  background: linear-gradient(60deg, #9cdfa7, #61d849);\n  transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);\n  display: block;\n  position: relative;\n  padding: 8px;\n  border-radius: 5px;\n  height: 95px;\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  color: #fff;\n  font-family: Fontil Sans, monospace;\n  -webkit-animation-name: fadeInDown;\n          animation-name: fadeInDown;\n  -webkit-animation-duration: 0.5s;\n          animation-duration: 0.5s;\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards;\n  opacity: 0; }\n\n.medal00_0 {\n  -webkit-animation-delay: 0s;\n          animation-delay: 0s; }\n\n.medal00_1 {\n  -webkit-animation-delay: 0.2s;\n          animation-delay: 0.2s; }\n\n@-webkit-keyframes fadeInDown {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n            transform: translate3d(0, -100%, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 0, 0);\n            transform: translate3d(0, 0, 0); } }\n\n@keyframes fadeInDown {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n            transform: translate3d(0, -100%, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 0, 0);\n            transform: translate3d(0, 0, 0); } }\n\n.medal01_0, .medal01_1, .medal01_2, .medal01_3, .medal01_4, .medal01_5 {\n  background: linear-gradient(60deg, #eee094, #ffbb54);\n  transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);\n  display: block;\n  position: relative;\n  padding: 8px;\n  border-radius: 5px;\n  height: 162px;\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  color: #fff;\n  font-family: Fontil Sans, monospace;\n  -webkit-animation-name: flipInY;\n          animation-name: flipInY;\n  -webkit-backface-visibility: visible !important;\n          backface-visibility: visible !important;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards;\n  opacity: 0; }\n\n.medal01_0 {\n  -webkit-animation-delay: 0.4s;\n          animation-delay: 0.4s; }\n\n.medal01_1 {\n  -webkit-animation-delay: 0.6s;\n          animation-delay: 0.6s; }\n\n.medal01_2 {\n  -webkit-animation-delay: 0.8s;\n          animation-delay: 0.8s; }\n\n.medal01_3 {\n  -webkit-animation-delay: 1.0s;\n          animation-delay: 1.0s; }\n\n.medal01_4 {\n  -webkit-animation-delay: 1.2s;\n          animation-delay: 1.2s; }\n\n.medal01_5 {\n  -webkit-animation-delay: 1.4s;\n          animation-delay: 1.4s; }\n\n@-webkit-keyframes flipInY {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in;\n    opacity: 0; }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in; }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: perspective(400px);\n            transform: perspective(400px);\n    opacity: 1; } }\n\n@keyframes flipInY {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in;\n    opacity: 0; }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in; }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n            transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: perspective(400px);\n            transform: perspective(400px);\n    opacity: 1; } }\n\n.medal02_0, .medal02_1, .medal02_2, .medal02_3, .medal02_4 {\n  background: linear-gradient(60deg, #ffb0a6, #ff5656);\n  transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);\n  display: block;\n  position: relative;\n  padding: 8px;\n  border-radius: 5px;\n  height: 130px;\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  color: #fff;\n  font-family: Fontil Sans, monospace;\n  -webkit-animation-name: jackInTheBox;\n          animation-name: jackInTheBox;\n  -webkit-animation-duration: 0.5s;\n          animation-duration: 0.5s;\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards;\n  opacity: 0; }\n\n.medal02_0 {\n  -webkit-animation-delay: 1.8s;\n          animation-delay: 1.8s; }\n\n.medal02_1 {\n  -webkit-animation-delay: 2s;\n          animation-delay: 2s; }\n\n.medal02_2 {\n  -webkit-animation-delay: 2.2s;\n          animation-delay: 2.2s; }\n\n.medal02_3 {\n  -webkit-animation-delay: 2.4s;\n          animation-delay: 2.4s; }\n\n.medal02_4 {\n  -webkit-animation-delay: 2.6s;\n          animation-delay: 2.6s; }\n\n@-webkit-keyframes jackInTheBox {\n  from {\n    opacity: 0;\n    -webkit-transform: scale(0.1) rotate(30deg);\n            transform: scale(0.1) rotate(30deg);\n    -webkit-transform-origin: center bottom;\n            transform-origin: center bottom; }\n  50% {\n    -webkit-transform: rotate(-10deg);\n            transform: rotate(-10deg); }\n  70% {\n    -webkit-transform: rotate(3deg);\n            transform: rotate(3deg); }\n  to {\n    opacity: 1;\n    -webkit-transform: scale(1);\n            transform: scale(1); } }\n\n@keyframes jackInTheBox {\n  from {\n    opacity: 0;\n    -webkit-transform: scale(0.1) rotate(30deg);\n            transform: scale(0.1) rotate(30deg);\n    -webkit-transform-origin: center bottom;\n            transform-origin: center bottom; }\n  50% {\n    -webkit-transform: rotate(-10deg);\n            transform: rotate(-10deg); }\n  70% {\n    -webkit-transform: rotate(3deg);\n            transform: rotate(3deg); }\n  to {\n    opacity: 1;\n    -webkit-transform: scale(1);\n            transform: scale(1); } }\n\n.medal03 {\n  background: linear-gradient(60deg, #f3f3f3, #ffffff);\n  display: block;\n  position: relative;\n  padding: 20px;\n  border-radius: 2px;\n  height: 55px;\n  color: black;\n  font-family: Fontil Sans, monospace; }\n\n.medal04 {\n  background: linear-gradient(60deg, #f3f3f3, #ffffff);\n  display: block;\n  position: relative;\n  padding: 20px;\n  border-radius: 2px;\n  height: 120px;\n  color: black;\n  font-family: Fontil Sans, monospace;\n  -webkit-animation-name: flipInY;\n          animation-name: flipInY; }\n\n.medal05 {\n  background: linear-gradient(60deg, #f3f3f3, #ffffff);\n  display: block;\n  position: relative;\n  padding: 20px;\n  border-radius: 2px;\n  height: 90px;\n  color: black;\n  font-family: Fontil Sans, monospace; }\n\n.spacer {\n  display: block;\n  width: 100%; }\n\n.mat-card {\n  text-align: center; }\n\n.mat-card img {\n    border-radius: 5px;\n    margin-top: -25px; }\n\n.mb-20 {\n  margin-bottom: 20px; }\n\n.news_card {\n  min-height: 450px;\n  -webkit-animation-name: fadeIn;\n          animation-name: fadeIn;\n  -webkit-animation-duration: 0.5s;\n          animation-duration: 0.5s;\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards;\n  opacity: 0;\n  -webkit-animation-delay: 0.3s;\n          animation-delay: 0.3s; }\n\n@-webkit-keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n@keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n@media only screen and (max-width: 425px) {\n  .left-container h2 {\n    top: 0;\n    font-size: 1.1em; }\n  .left-container p {\n    left: 110px;\n    font-size: 3.5vw;\n    top: 10px; }\n  .left-container h2, .left-container p {\n    position: relative;\n    text-align: left; }\n  .left-container img {\n    width: 100px;\n    left: 20px;\n    top: 55px;\n    height: 120px;\n    margin-top: 0; }\n  .right-container .mat-card #medal {\n    right: 0px;\n    left: -5px;\n    top: 0px;\n    z-index: 4;\n    width: 102px;\n    height: 68px; }\n  .right-container .mat-card a {\n    text-align: center;\n    margin: auto;\n    left: 5px;\n    bottom: 5px; } }\n\n@media only screen and (max-width: 375px) {\n  .left-container h2 {\n    top: 0;\n    font-size: 1.1em; }\n  .left-container p {\n    top: 20px;\n    left: 100px;\n    font-size: 3.5vw; }\n  .left-container h2, .left-container p {\n    position: relative;\n    text-align: left; }\n  .left-container img {\n    width: 100px;\n    left: 20px;\n    top: 55px;\n    height: 120px;\n    margin-top: 0; } }\n\n@media only screen and (max-width: 320px) {\n  .right-container .mat-card #medal {\n    right: 0px;\n    left: -12px;\n    top: 0px;\n    z-index: 4;\n    width: 102px;\n    height: 68px; }\n  .right-container .mat-card a {\n    text-align: center;\n    margin: auto;\n    left: 5px;\n    bottom: 5px;\n    font-size: 0.5em; } }\n"

/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.ts ***!
  \*********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/staff.service */ "./src/app/services/staff.service.ts");
/* harmony import */ var src_app_services_booking_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/booking.service */ "./src/app/services/booking.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_services_recommendation_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/recommendation.service */ "./src/app/services/recommendation.service.ts");
/* harmony import */ var src_app_services_class_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/class.service */ "./src/app/services/class.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' }
];

var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(staffService, http, recommendationService, bookService, classService) {
        var _this = this;
        this.staffService = staffService;
        this.http = http;
        this.recommendationService = recommendationService;
        this.bookService = bookService;
        this.classService = classService;
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.places = [];
        this.panelOpenState = false;
        this.nip = localStorage.getItem('nip');
        this.allFavoriteArray = [];
        this.allVisitArray = [];
        this.allTrendingArray = [];
        this.totalBook = [];
        this.enthusiast = 0;
        this.healthy = 0;
        this.feast = 0;
        this.rare = 0;
        this.trainer = 0;
        this.training = 0;
        this.topRec = { status: '', result: [] };
        this.staffService.getStaffById(this.nip).subscribe(function (res) {
            _this.staff = res.body;
            if (_this.staff.flag_trainee == true) {
                _this.classService.getScheduleByProgram(_this.staff.program).subscribe(function (res) {
                    _this.schedule = res.body;
                    _this.classService.getMaterialById(_this.schedule.material_code).subscribe(function (res) {
                        _this.material = res.body;
                    });
                });
            }
            else if (_this.staff.flag_trainer == true) {
                _this.classService.getScheduleByTrainerNip(_this.nip).subscribe(function (res) {
                    _this.schedule = res.body;
                    _this.classService.getMaterialById(_this.schedule.material_code).subscribe(function (res) {
                        _this.material = res.body;
                    });
                });
            }
        });
        this.recommendationService.getRecommendation().subscribe(function (res) {
            _this.topRec = res.body;
        });
        this.bookService.getFavoriteStall(this.nip).subscribe(function (res) {
            //console.log(res.body);
            _this.allFavoriteArray = res.body;
        });
        this.bookService.getVisitAgain(this.nip).subscribe(function (res) {
            //console.log(res.body);
            _this.allVisitArray = res.body;
        });
        this.bookService.getTotalBookingByNip(this.nip).subscribe(function (res) {
            console.log(res.body);
            _this.totalBook = res.body;
        });
        this.bookService.getTrendingPick().subscribe(function (res) {
            console.log(res.body);
            _this.allTrendingArray = res.body;
        });
        this.bookService.getEnthusiastBadge(this.nip).subscribe(function (res) {
            //console.log(res.body);
            var x = res.body;
            if (x == 0) {
                _this.enthusiast = 0;
            }
        });
        this.bookService.getHealthyBadge().subscribe(function (res) {
            //console.log(res.body);
            var x = res.body;
            if (x == 0) {
                _this.healthy = 0;
            }
        });
        this.bookService.getFeastBadge().subscribe(function (res) {
            //console.log(res.body);
            var x = res.body;
            if (x == 0) {
                _this.feast = 0;
            }
        });
        this.bookService.getRareFoodBadge(this.nip).subscribe(function (res) {
            //console.log(res.body);
            var x = res.body;
            if (x == 0) {
                _this.rare = 0;
            }
        });
        this.staffService.getTrainerBadge(this.nip).subscribe(function (res) {
            var x = res.body;
            if (x == 0) {
                _this.trainer = 0;
            }
        });
        this.staffService.getTrainingBadge(this.nip).subscribe(function (res) {
            var x = res.body;
            if (x == 0) {
                _this.training = 0;
            }
        });
        this.places = [
            {
                imgSrc: 'assets/images/BLI01.jpeg',
                place: 'Finalis Miss Grand Indonesia 2018',
                description: 'Finalis Miss Grand Indonesia 2018 berkunjung ke BCA Learning Institute dan KEMENSOS RI',
                charge: 'BCA Learning Institute',
                location: 'Bogor, Jawa Barat'
            },
            {
                imgSrc: 'assets/images/BLI02.jpg',
                place: 'BCA Learning Institute',
                description: 'Sebagai salah satu instansi keuangan terbesar di Indonesia, PT Bank Central Asia Tbk. (BCA) berkomitmen untuk ikut hadir memberikan dukungan bagi tumbuhnya budaya belajar di perusahaan, khususnya perbankan.',
                charge: 'BCA Learning Institute',
                location: 'Bogor, Jawa Barat'
            },
            {
                imgSrc: 'assets/images/BLI03.jpg',
                place: 'Miss Grand Indonesia 2018',
                description: 'Dikna Faradiba selaku National Director of Miss Grand Indonesia mengungkapkan rasa terima kasihnya kepada BCA atas kesempatan berkunjung yang diberikan kepada para finalis Miss Grand Indonesia 2018.',
                charge: 'BCA Learning Institute',
                location: 'Bogor, Jawa Barat'
            }
        ];
    }
    DashboardComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    };
    DashboardComponent.prototype.ngOnInit = function () {
        // Hack: Scrolls to top of Page after page view initialized
        var top = document.getElementById('top');
        if (top !== null) {
            top.scrollIntoView();
            top = null;
        }
    };
    DashboardComponent.prototype.ngAfterViewInit = function () {
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/layout/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/layout/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_services_staff_service__WEBPACK_IMPORTED_MODULE_1__["StaffService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_app_services_recommendation_service__WEBPACK_IMPORTED_MODULE_4__["RecommendationService"], src_app_services_booking_service__WEBPACK_IMPORTED_MODULE_2__["BookingService"], src_app_services_class_service__WEBPACK_IMPORTED_MODULE_5__["ClassService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.module.ts ***!
  \******************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/modules/stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/layout/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/layout/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_6__["DashboardRoutingModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__["StatModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"]
            ],
            declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.html":
/*!***************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>{{info.data}}</h2>\r\n\r\n<mat-dialog-actions>\r\n    <button class=\"mat-raised-button mat-primary\"(click)=\"close()\">Ok</button>\r\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n"

/***/ }),

/***/ "./src/app/layout/info-dialog/info-dialog.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layout/info-dialog/info-dialog.component.ts ***!
  \*************************************************************/
/*! exports provided: InfoDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoDialogComponent", function() { return InfoDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var InfoDialogComponent = /** @class */ (function () {
    function InfoDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.info = data;
        console.log(this.info);
    }
    InfoDialogComponent.prototype.ngOnInit = function () {
    };
    InfoDialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    InfoDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-info-dialog',
            template: __webpack_require__(/*! ./info-dialog.component.html */ "./src/app/layout/info-dialog/info-dialog.component.html"),
            styles: [__webpack_require__(/*! ./info-dialog.component.scss */ "./src/app/layout/info-dialog/info-dialog.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], InfoDialogComponent);
    return InfoDialogComponent;
}());



/***/ }),

/***/ "./src/app/services/class.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/class.service.ts ***!
  \*******************************************/
/*! exports provided: ClassService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassService", function() { return ClassService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ClassService = /** @class */ (function () {
    function ClassService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'class/';
        this.token = localStorage.getItem('token');
    }
    ClassService.prototype.getListClass = function () {
        return this.http.get(this.url + 'class/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListAvailableClass = function () {
        return this.http.get(this.url + 'class/available', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListUnavailableClass = function () {
        return this.http.get(this.url + 'class/unavailable', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getClassByCode = function (code) {
        return this.http.get(this.url + 'class/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createClass = function (classEntity) {
        return this.http.post(this.url + 'class/' + classEntity, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateClass = function (classEntity) {
        return this.http.put(this.url + 'class/' + classEntity, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListMaterial = function () {
        return this.http.get(this.url + 'material/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getMaterialById = function (code) {
        return this.http.get(this.url + 'material/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createMaterial = function (material) {
        return this.http.post(this.url + 'material/', material, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateMaterial = function (material) {
        return this.http.put(this.url + 'material/', material, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListProgram = function () {
        return this.http.get(this.url + 'program/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getProgramByCode = function (code) {
        return this.http.get(this.url + 'program/detail/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createProgram = function (program) {
        return this.http.post(this.url + 'program/', program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateProgram = function (program) {
        return this.http.put(this.url + 'program/', program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.deleteProgram = function (code) {
        return this.http.delete(this.url + 'program/delete/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListCompetency = function () {
        return this.http.get(this.url + 'competency/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListCompetencybyCode = function (materialCode) {
        return this.http.get(this.url + 'competency/material/' + materialCode, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getCompetencyByTrainerNip = function (trainerNip) {
        return this.http.get(this.url + 'competency/detail/' + trainerNip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getCompetencyByCode = function (material_code) {
        return this.http.get(this.url + 'competency/material/' + material_code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createCompetency = function (competency) {
        return this.http.post(this.url + 'competency/', competency, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListSchedule = function () {
        return this.http.get(this.url + 'schedule/all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getListScheduleByProgram = function (program) {
        return this.http.get(this.url + 'schedule/all/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleById = function (id) {
        return this.http.get(this.url + 'schedule/detail/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByCode = function (code) {
        return this.http.get(this.url + 'schedule/class/' + code, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByProgram = function (program) {
        return this.http.get(this.url + 'schedule/program/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.getScheduleByTrainerNip = function (nip) {
        return this.http.get(this.url + 'schedule/trainer/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createSchedule = function (schedule) {
        return this.http.post(this.url + 'schedule/', schedule, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.updateSchedule = function (schedule) {
        return this.http.put(this.url + 'schedule/', schedule, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.createScheduleNonProgram = function (scheduleNonProgram) {
        return this.http.post(this.url + 'schedule/nonprogram', scheduleNonProgram, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    ClassService.prototype.uploadMaterial = function (file) {
        return this.http.post(this.url + 'material/upload/', file, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            reportProgress: true,
            observe: 'events'
        });
    };
    ClassService.prototype.downloadMaterial = function (filename) {
        return this.http.get(this.url + 'material/download/' + filename, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'blob'
        });
    };
    ClassService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ClassService);
    return ClassService;
}());



/***/ }),

/***/ "./src/app/services/recommendation.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/recommendation.service.ts ***!
  \****************************************************/
/*! exports provided: RecommendationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecommendationService", function() { return RecommendationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecommendationService = /** @class */ (function () {
    function RecommendationService(http) {
        this.http = http;
        this.token = localStorage.getItem('token');
        this.userNip = localStorage.getItem('nip');
        this.url = 'http://10.20.228.116:8003/api/summa/';
        this.recommendationReq = {
            key: 'BLI123',
            nip: this.userNip,
            top: 3,
            actionType: 'predict'
        };
    }
    RecommendationService.prototype.getRecommendation = function () {
        return this.http.post(this.url, this.recommendationReq, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    RecommendationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], RecommendationService);
    return RecommendationService;
}());



/***/ }),

/***/ "./src/app/services/staff.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/staff.service.ts ***!
  \*******************************************/
/*! exports provided: StaffService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffService", function() { return StaffService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StaffService = /** @class */ (function () {
    function StaffService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'staff/';
        this.token = localStorage.getItem('token');
    }
    StaffService.prototype.getAllStaffs = function () {
        return this.http.get(this.url + 'all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainingStaffsByProgram = function (program) {
        return this.http.get(this.url + 'all/training/' + program, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainees = function () {
        return this.http.get(this.url + 'trainees', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllTrainers = function () {
        return this.http.get(this.url + 'trainers', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getAllDivisions = function () {
        return this.http.get(this.url + 'division', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getStaffById = function (nip) {
        return this.http.get(this.url + 'detail/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.createStaff = function (staff) {
        console.log(staff);
        return this.http.post(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.updateStaff = function (staff) {
        console.log(staff);
        return this.http.put(this.url, staff, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.deleteStaff = function (nip) {
        return this.http.delete(this.url + 'delete/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainingBadge = function (nip) {
        return this.http.get(this.url + 'training/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTrainerBadge = function (nip) {
        return this.http.get(this.url + 'trainer/badge/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTrainerToday = function () {
        return this.http.get(this.url + 'trainer/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService.prototype.getTotalTraineeToday = function () {
        return this.http.get(this.url + 'trainee/today/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    StaffService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StaffService);
    return StaffService;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.html":
/*!*********************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card [ngClass]=\"bgClass\">\r\n    <mat-card-header>\r\n        <div mat-card-avatar>\r\n            <mat-icon class=\"icon-lg\">{{icon}}</mat-icon>\r\n        </div>\r\n        <mat-card-title>{{count}}</mat-card-title>\r\n        <mat-card-subtitle>{{label}}</mat-card-subtitle>\r\n    </mat-card-header>\r\n    <mat-card-actions>\r\n        <a href=\"javascript:void(0)\" class=\"float-right card-inverse\">\r\n            View Details\r\n        </a>\r\n    </mat-card-actions>\r\n</mat-card>"

/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host /deep/ .mat-card-header-text {\n  width: 100%;\n  text-align: right; }\n.icon-lg {\n  font-size: 40px; }\n.mat-card {\n  color: #fff; }\n.mat-card .mat-card-header {\n    width: 100%; }\n.mat-card .mat-card-title {\n    font-size: 40px !important; }\n.mat-card .mat-card-subtitle {\n    color: #fff; }\n.mat-card .mat-card-actions a {\n    text-decoration: none;\n    cursor: pointer;\n    color: #fff; }\n.mat-card.danger {\n  background: linear-gradient(60deg, #ec407a, #d81b60); }\n.mat-card.warn {\n  background: linear-gradient(60deg, #ffa726, #fb8c00); }\n.mat-card.success {\n  background: linear-gradient(60deg, #66bb6a, #43a047); }\n.mat-card.info {\n  background: linear-gradient(60deg, #26c6da, #00acc1); }\n"

/***/ }),

/***/ "./src/app/shared/modules/stat/stat.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.component.ts ***!
  \*******************************************************/
/*! exports provided: StatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatComponent", function() { return StatComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatComponent = /** @class */ (function () {
    function StatComponent() {
    }
    StatComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "bgClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "icon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "count", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "label", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "data", void 0);
    StatComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stat',
            template: __webpack_require__(/*! ./stat.component.html */ "./src/app/shared/modules/stat/stat.component.html"),
            styles: [__webpack_require__(/*! ./stat.component.scss */ "./src/app/shared/modules/stat/stat.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StatComponent);
    return StatComponent;
}());



/***/ }),

/***/ "./src/app/shared/modules/stat/stat.module.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/modules/stat/stat.module.ts ***!
  \****************************************************/
/*! exports provided: StatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatModule", function() { return StatModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _stat_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stat.component */ "./src/app/shared/modules/stat/stat.component.ts");
/* harmony import */ var _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../error/show-errors.component */ "./src/app/error/show-errors.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../layout/custom-dialog/custom-dialog.component */ "./src/app/layout/custom-dialog/custom-dialog.component.ts");
/* harmony import */ var _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../layout/info-dialog/info-dialog.component */ "./src/app/layout/info-dialog/info-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var StatModule = /** @class */ (function () {
    function StatModule() {
    }
    StatModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"]],
            declarations: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"], _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__["ShowErrorsComponent"], _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__["CustomDialogComponent"], _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__["InfoDialogComponent"]],
            exports: [_stat_component__WEBPACK_IMPORTED_MODULE_2__["StatComponent"], _error_show_errors_component__WEBPACK_IMPORTED_MODULE_3__["ShowErrorsComponent"], _layout_custom_dialog_custom_dialog_component__WEBPACK_IMPORTED_MODULE_5__["CustomDialogComponent"], _layout_info_dialog_info_dialog_component__WEBPACK_IMPORTED_MODULE_6__["InfoDialogComponent"]]
        })
    ], StatModule);
    return StatModule;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map