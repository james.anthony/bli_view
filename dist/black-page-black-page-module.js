(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["black-page-black-page-module"],{

/***/ "./src/app/layout/black-page/black-page-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/black-page/black-page-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: BlackPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlackPageRoutingModule", function() { return BlackPageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _black_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./black-page.component */ "./src/app/layout/black-page/black-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _black_page_component__WEBPACK_IMPORTED_MODULE_2__["BlackPageComponent"]
    }
];
var BlackPageRoutingModule = /** @class */ (function () {
    function BlackPageRoutingModule() {
    }
    BlackPageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BlackPageRoutingModule);
    return BlackPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/black-page/black-page.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/black-page/black-page.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n    black-page works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/layout/black-page/black-page.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/black-page/black-page.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/layout/black-page/black-page.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/black-page/black-page.component.ts ***!
  \***********************************************************/
/*! exports provided: BlackPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlackPageComponent", function() { return BlackPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlackPageComponent = /** @class */ (function () {
    function BlackPageComponent() {
    }
    BlackPageComponent.prototype.ngOnInit = function () { };
    BlackPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-black-page',
            template: __webpack_require__(/*! ./black-page.component.html */ "./src/app/layout/black-page/black-page.component.html"),
            styles: [__webpack_require__(/*! ./black-page.component.scss */ "./src/app/layout/black-page/black-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BlackPageComponent);
    return BlackPageComponent;
}());



/***/ }),

/***/ "./src/app/layout/black-page/black-page.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/black-page/black-page.module.ts ***!
  \********************************************************/
/*! exports provided: BlackPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlackPageModule", function() { return BlackPageModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _black_page_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./black-page-routing.module */ "./src/app/layout/black-page/black-page-routing.module.ts");
/* harmony import */ var _black_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./black-page.component */ "./src/app/layout/black-page/black-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var BlackPageModule = /** @class */ (function () {
    function BlackPageModule() {
    }
    BlackPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _black_page_routing_module__WEBPACK_IMPORTED_MODULE_2__["BlackPageRoutingModule"]],
            declarations: [_black_page_component__WEBPACK_IMPORTED_MODULE_3__["BlackPageComponent"]]
        })
    ], BlackPageModule);
    return BlackPageModule;
}());



/***/ })

}]);
//# sourceMappingURL=black-page-black-page-module.js.map