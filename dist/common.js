(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/services/booking.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/booking.service.ts ***!
  \*********************************************/
/*! exports provided: BookingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingService", function() { return BookingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BookingService = /** @class */ (function () {
    function BookingService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'booking/';
        this.token = localStorage.getItem('token');
    }
    BookingService.prototype.getListBooking = function () {
        return this.http.get(this.url + 'all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getBookingById = function (id) {
        return this.http.get(this.url + 'detail/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getTotalBookingByNip = function (nip) {
        return this.http.get(this.url + 'booking/total/nip/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getBookingByNip = function (nip) {
        return this.http.get(this.url + 'nip/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.isBooked = function (nip) {
        return this.http.get(this.url + 'today/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getBookingByTotal = function () {
        return this.http.get(this.url + 'totalbook/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getTotalBookingByDate = function (stall_id) {
        return this.http.get(this.url + 'total/date/' + stall_id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getMedal = function () {
        return this.http.get(this.url + 'medals/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.createBooking = function (booking) {
        return this.http.post(this.url, booking, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.updateConfirmation = function (booking) {
        return this.http.put(this.url + 'confirmation', booking, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.updateRating = function (booking) {
        return this.http.put(this.url + 'rating', booking, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    /////////////////////////////
    BookingService.prototype.getAvgRating = function (id) {
        return this.http.get(this.url + 'rating/average/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getStallRank = function (id) {
        return this.http.get(this.url + 'rank/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getFirstRank = function () {
        return this.http.get(this.url + 'first', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getRareFoodBadge = function (nip) {
        return this.http.get(this.url + 'rare/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getTotalBookMonth = function (id) {
        return this.http.get(this.url + 'total/month/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getTotalBook = function (id) {
        return this.http.get(this.url + 'total/stall/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getTotalBookDate = function (id) {
        return this.http.get(this.url + 'total/date/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getFavoriteStall = function (nip) {
        return this.http.get(this.url + 'favorite/food/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getVisitAgain = function (nip) {
        return this.http.get(this.url + 'least/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getTrendingPick = function () {
        return this.http.get(this.url + 'trend/picks', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getEnthusiastBadge = function (nip) {
        return this.http.get(this.url + 'food/enthusiast/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getHealthyBadge = function () {
        return this.http.get(this.url + 'food/healthy/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService.prototype.getFeastBadge = function () {
        return this.http.get(this.url + 'food/feast/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    BookingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], BookingService);
    return BookingService;
}());



/***/ }),

/***/ "./src/app/services/food-stall.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/food-stall.service.ts ***!
  \************************************************/
/*! exports provided: FoodStallService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodStallService", function() { return FoodStallService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FoodStallService = /** @class */ (function () {
    function FoodStallService(http) {
        this.http = http;
        this.masterUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
        this.url = this.masterUrl + 'stall/';
        this.token = localStorage.getItem('token');
    }
    FoodStallService.prototype.getAllStalls = function () {
        return this.http.get(this.url + 'all', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.getInqStalls = function () {
        return this.http.get(this.url + 'inquiry', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.getStallByLocation = function (location) {
        return this.http.get(this.url + 'location/' + location, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.createStall = function (stall) {
        return this.http.post(this.url, stall, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.updateStall = function (stall) {
        return this.http.put(this.url, stall, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.deleteStall = function (stall) {
        return this.http.delete(this.url + 'delete/' + stall, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.getAllStallId = function (id) {
        return this.http.get(this.url + 'id', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.getOneStallById = function (id) {
        return this.http.get(this.url + 'detail/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.getOneStallByNip = function (nip) {
        return this.http.get(this.url + 'detail/nip/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.checkStock = function (location) {
        return this.http.get(this.url + 'stock/check/' + location, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.reduceStock = function (id) {
        return this.http.get(this.url + 'stock/reduce/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.resetStock = function () {
        return this.http.get(this.url + 'stock/reset/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    //vendors
    FoodStallService.prototype.getVendors = function () {
        return this.http.get(this.url + 'vendor/', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.getVendorByNip = function (nip) {
        return this.http.get(this.url + 'vendor/' + nip, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.createVendor = function (vendor) {
        return this.http.post(this.url + 'vendor/', vendor, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService.prototype.updateVendor = function (vendor) {
        return this.http.put(this.url + 'vendor', vendor, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    //medal vendor
    FoodStallService.prototype.getSoldOut = function (id) {
        return this.http.get(this.url + 'soldout/' + id, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', this.token),
            responseType: 'json',
            observe: 'response'
        });
    };
    FoodStallService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], FoodStallService);
    return FoodStallService;
}());



/***/ }),

/***/ "./src/app/services/program.ts":
/*!*************************************!*\
  !*** ./src/app/services/program.ts ***!
  \*************************************/
/*! exports provided: Program */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Program", function() { return Program; });
var Program = /** @class */ (function () {
    function Program() {
    }
    return Program;
}());



/***/ }),

/***/ "./src/app/services/schedule.ts":
/*!**************************************!*\
  !*** ./src/app/services/schedule.ts ***!
  \**************************************/
/*! exports provided: Schedule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Schedule", function() { return Schedule; });
var Schedule = /** @class */ (function () {
    function Schedule() {
    }
    return Schedule;
}());



/***/ }),

/***/ "./src/app/services/stall.ts":
/*!***********************************!*\
  !*** ./src/app/services/stall.ts ***!
  \***********************************/
/*! exports provided: Stall */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stall", function() { return Stall; });
var Stall = /** @class */ (function () {
    function Stall() {
    }
    return Stall;
}());



/***/ }),

/***/ "./src/app/services/vendor.ts":
/*!************************************!*\
  !*** ./src/app/services/vendor.ts ***!
  \************************************/
/*! exports provided: Vendor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Vendor", function() { return Vendor; });
var Vendor = /** @class */ (function () {
    function Vendor() {
    }
    return Vendor;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map