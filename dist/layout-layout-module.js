(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layout-layout-module"],{

/***/ "./src/app/layout/components/sidebar/sidebar.component.html":
/*!******************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"sidebar\">\r\n    <mat-nav-list class=\"admin-menu\" *ngIf=\"user_role == 'ROLE_SUPERADMIN' || user_role == 'ROLE_ADMIN'\">\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/admin-page']\">\r\n            <mat-icon class=\"sidenav-icon\">assignment_ind</mat-icon> {{ 'Administration Page' | translate }}\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/create-schedule']\">\r\n            <mat-icon class=\"sidenav-icon\">list</mat-icon> Create Schedule\r\n        </a>\r\n        <!-- <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/vendor-detail']\">\r\n            <mat-icon class=\"sidenav-icon\">star_border</mat-icon> Detail Vendor\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/booking-detail']\">\r\n            <mat-icon class=\"sidenav-icon\">star_border</mat-icon> Detail Booking\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/program-detail']\">\r\n            <mat-icon class=\"sidenav-icon\">star_border</mat-icon> Detail Program\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/staff-detail']\">\r\n            <mat-icon class=\"sidenav-icon\">star_border</mat-icon> Detail Staff\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/schedule-detail']\">\r\n            <mat-icon class=\"sidenav-icon\">star_border</mat-icon> Detail Kelas\r\n        </a> -->\r\n    </mat-nav-list>\r\n    <mat-nav-list class=\"staff-menu\" *ngIf=\"user_role == 'ROLE_STAFF'\">\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/dashboard']\">\r\n            <mat-icon class=\"sidenav-icon\">dashboard</mat-icon> Personal {{ 'Dashboard' | translate }}\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/class']\">\r\n            <mat-icon class=\"sidenav-icon\">class</mat-icon> {{ 'Class Schedule' | translate }}\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/food-stall']\">\r\n            <mat-icon class=\"sidenav-icon\">restaurant</mat-icon> {{ 'Food Stall' | translate }}\r\n        </a>\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/shuttle']\">\r\n            <mat-icon class=\"sidenav-icon\">directions_bus</mat-icon> Shuttle Schedule\r\n        </a>\r\n        <!-- <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/confirmation-page']\">\r\n            <mat-icon class=\"sidenav-icon\">star</mat-icon> Confirmation\r\n        </a> -->\r\n    </mat-nav-list>\r\n    <mat-nav-list class=\"vendor-menu\" *ngIf=\"user_role == 'ROLE_VENDOR'\">\r\n        <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/dashboard-vendor']\">\r\n            <mat-icon class=\"sidenav-icon\">dashboard</mat-icon> Dashboard Vendor\r\n        </a>\r\n    </mat-nav-list>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n#sidebar {\n  width: 250px;\n  position: fixed;\n  top: 56px;\n  bottom: 0;\n  background: #fff;\n  box-shadow: 3px 0 6px rgba(0, 0, 0, 0.24);\n  z-index: 999;\n  -ms-overflow-y: auto;\n  overflow-y: auto; }\n\n#sidebar .mat-nav-list {\n    width: 100%; }\n\n#sidebar .mat-nav-list a {\n      display: block; }\n\n#sidebar .mat-nav-list a .mat-icon {\n        margin-right: 10px; }\n\n.substall-menu {\n  margin-left: -50px; }\n\n.substall-menu .sidenav-icon {\n    margin-left: 50px; }\n\n.nested-menu .nested {\n  list-style-type: none; }\n\n.nested-menu .submenu {\n  display: none;\n  height: 0; }\n\n.nested-menu .expand.submenu {\n  display: block;\n  list-style-type: none;\n  height: auto;\n  margin: 0; }\n\n.nested-menu .expand.submenu li a {\n    padding: 10px;\n    display: block; }\n\n@media screen and (max-width: 992px) {\n  #sidebar {\n    left: -250px; } }\n"

/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.ts ***!
  \****************************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
        this.showMenu = '';
        this.parent_active = false;
        this.first_active = false;
        this.upper_active = false;
        this.lower_active = false;
        this.user_role = localStorage.getItem('role');
    }
    SidebarComponent.prototype.ngOnInit = function () { };
    SidebarComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu
            || this.showMenu === 'subpages1'
            || this.showMenu === 'subpages2'
            || this.showMenu === 'subpages3') {
            this.showMenu = '0';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        }
        else {
            this.showMenu = element;
            this.parent_active = true;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        }
    };
    SidebarComponent.prototype.addExpandSubClass_first = function (element) {
        if (element === this.showMenu) {
            this.showMenu = 'pages';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        }
        else {
            this.showMenu = element;
            this.parent_active = false;
            this.first_active = true;
            this.upper_active = false;
            this.lower_active = false;
        }
    };
    SidebarComponent.prototype.addExpandSubClass_upper = function (element) {
        if (element === this.showMenu) {
            this.showMenu = 'pages';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        }
        else {
            this.showMenu = element;
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = true;
            this.lower_active = false;
        }
    };
    SidebarComponent.prototype.addExpandSubClass_lower = function (element) {
        if (element === this.showMenu) {
            this.showMenu = 'pages';
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = false;
        }
        else {
            this.showMenu = element;
            this.parent_active = false;
            this.first_active = false;
            this.upper_active = false;
            this.lower_active = true;
        }
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/layout/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/layout/components/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/layout/components/topnav/topnav.component.html":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/topnav/topnav.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\" class=\"fix-nav\">\r\n    <button type=\"button\" mat-icon-button class=\"visible-md\" (click)=\"toggleSidebar()\">\r\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\r\n    </button>\r\n    <div class=\"nav-brand\">\r\n        BLI Companion\r\n    </div>\r\n    \r\n    <span class=\"nav-spacer\"></span>\r\n    \r\n    <button class=\"hidden-sm\" mat-icon-button [routerLinkActive]=\"'active'\" [routerLink]=\"['/dashboard']\">\r\n        <mat-icon>account_circle</mat-icon>\r\n    </button>\r\n    <button mat-icon-button (click)=\"onLoggedout()\">\r\n        <mat-icon>exit_to_app</mat-icon>\r\n    </button>\r\n</mat-toolbar>\r\n"

/***/ }),

/***/ "./src/app/layout/components/topnav/topnav.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/topnav/topnav.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@font-face {\n  font-family: 'Material Icons';\n  font-style: normal;\n  font-weight: 400;\n  src: url('MaterialIcons-Regular.eot'); /* For IE6-8 */\n  src: local('Material Icons'),\n       local('MaterialIcons-Regular'),\n       url('MaterialIcons-Regular.woff2') format('woff2'),\n       url('MaterialIcons-Regular.woff') format('woff'),\n       url('MaterialIcons-Regular.ttf') format('truetype');\n}\n\n.material-icons {\n  font-family: 'Material Icons';\n  font-weight: normal;\n  font-style: normal;\n  font-size: 24px;  /* Preferred icon size */\n  display: inline-block;\n  line-height: 1;\n  text-transform: none;\n  letter-spacing: normal;\n  word-wrap: normal;\n  white-space: nowrap;\n  direction: ltr;\n\n  /* Support for all WebKit browsers. */\n  -webkit-font-smoothing: antialiased;\n  /* Support for Safari and Chrome. */\n  text-rendering: optimizeLegibility;\n\n  /* Support for Firefox. */\n  -moz-osx-font-smoothing: grayscale;\n\n  /* Support for IE. */\n  -webkit-font-feature-settings: 'liga';\n          font-feature-settings: 'liga';\n}\n\n:host {\n  display: block;\n  position: fixed;\n  left: 0;\n  right: 0;\n  top: 0;\n  z-index: 1000; }\n\n.nav-brand {\n  width: 215px;\n  text-align: center; }\n\n.topnav-icon {\n  text-decoration: none;\n  display: flex;\n  color: #fff; }\n\n.nav-spacer {\n  flex: 1 1 auto; }\n\n.visible-md {\n  display: none; }\n\n.visible-sm {\n  display: none; }\n\n@media screen and (max-width: 992px) {\n  .visible-md {\n    display: block; } }\n\n@media screen and (max-width: 768px) {\n  .visible-sm {\n    display: block; }\n  .nav-brand {\n    width: 100%; } }\n\n@media screen and (max-width: 768px) {\n  .hidden-sm {\n    display: none; } }\n\n@media (max-width: 599px) {\n  .mat-toolbar-row, .mat-toolbar-single-row {\n    height: 64px; } }\n"

/***/ }),

/***/ "./src/app/layout/components/topnav/topnav.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/components/topnav/topnav.component.ts ***!
  \**************************************************************/
/*! exports provided: TopnavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopnavComponent", function() { return TopnavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TopnavComponent = /** @class */ (function () {
    function TopnavComponent(router, translate) {
        var _this = this;
        this.router = router;
        this.translate = translate;
        this.pushRightClass = 'push-right';
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] && window.innerWidth <= 992 && _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    TopnavComponent.prototype.ngOnInit = function () { };
    TopnavComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    TopnavComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    TopnavComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        this.router.navigate(['/login']);
    };
    TopnavComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    TopnavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-topnav',
            template: __webpack_require__(/*! ./topnav.component.html */ "./src/app/layout/components/topnav/topnav.component.html"),
            styles: [__webpack_require__(/*! ./topnav.component.scss */ "./src/app/layout/components/topnav/topnav.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
    ], TopnavComponent);
    return TopnavComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/layout/layout-routing.module.ts ***!
  \*************************************************/
/*! exports provided: LayoutRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutRoutingModule", function() { return LayoutRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _layout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layout.component */ "./src/app/layout/layout.component.ts");
/* harmony import */ var _shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/guard/role-guard.service */ "./src/app/shared/guard/role-guard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        component: _layout_component__WEBPACK_IMPORTED_MODULE_2__["LayoutComponent"],
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_STAFF' }
            },
            {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            },
            {
                path: 'components',
                loadChildren: './material-components/material-components.module#MaterialComponentsModule'
            },
            {
                path: 'forms',
                loadChildren: './forms/forms.module#FormsModule'
            },
            {
                path: 'grid',
                loadChildren: './grid/grid.module#GridModule'
            },
            {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            },
            {
                path: 'blank-page',
                loadChildren: './blank-page/blank-page.module#BlankPageModule'
            },
            {
                path: 'black-page',
                loadChildren: './black-page/black-page.module#BlackPageModule'
            },
            {
                path: 'food-stall',
                loadChildren: './food-stall/food-stall.module#FoodStallModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_STAFF' }
            },
            {
                path: 'dashboard-vendor',
                loadChildren: './dashboard-vendor/dashboard-vendor.module#DashboardVendorModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_VENDOR' }
            },
            {
                path: 'admin-page',
                loadChildren: './admin-page/admin-page.module#AdminPageModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'confirmation-page',
                loadChildren: './confirmation-page/confirmation-page.module#ConfirmationPageModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_STAFF' }
            },
            {
                path: 'class',
                loadChildren: './class/class.module#ClassModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_STAFF' }
            },
            {
                path: 'shuttle',
                loadChildren: './shuttle/shuttle.module#ShuttleModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_STAFF' }
            },
            {
                path: 'booking-detail',
                loadChildren: './booking-detail/booking-detail.module#BookingDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'booking-detail/:booking_id/:stall_id/:nip',
                loadChildren: './booking-detail/booking-detail.module#BookingDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'staff-detail',
                loadChildren: './staff-detail/staff-detail.module#StaffDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'staff-detail/:nip',
                loadChildren: './staff-detail/staff-detail.module#StaffDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'program-detail',
                loadChildren: './program-detail/program-detail.module#ProgramDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'program-detail/:code',
                loadChildren: './program-detail/program-detail.module#ProgramDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'schedule-detail',
                loadChildren: './schedule-detail/schedule-detail.module#ScheduleDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'schedule-detail/:code/:availability',
                loadChildren: './schedule-detail/schedule-detail.module#ScheduleDetailModule'
            },
            {
                path: 'vendor-detail',
                loadChildren: './vendor-detail/vendor-detail.module#VendorDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'vendor-detail/:stall_id',
                loadChildren: './vendor-detail/vendor-detail.module#VendorDetailModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            },
            {
                path: 'create-schedule',
                loadChildren: './create-schedule/create-schedule.module#CreateScheduleModule', canActivate: [_shared_guard_role_guard_service__WEBPACK_IMPORTED_MODULE_3__["RoleGuardService"]], data: { role: 'ROLE_ADMIN' }
            }
        ]
    }
];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/layout.component.html":
/*!**********************************************!*\
  !*** ./src/app/layout/layout.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-nav></app-nav> -->\r\n<app-topnav></app-topnav>\r\n<app-sidebar></app-sidebar>\r\n<div class=\"main-container\">\r\n    <router-outlet></router-outlet>\r\n</div>"

/***/ }),

/***/ "./src/app/layout/layout.component.scss":
/*!**********************************************!*\
  !*** ./src/app/layout/layout.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-container {\n  background-color: ghostwhite;\n  overflow: auto;\n  padding-top: 0; }\n\n.mat-card {\n  background-color: white; }\n"

/***/ }),

/***/ "./src/app/layout/layout.component.ts":
/*!********************************************!*\
  !*** ./src/app/layout/layout.component.ts ***!
  \********************************************/
/*! exports provided: LayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponent", function() { return LayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LayoutComponent = /** @class */ (function () {
    function LayoutComponent() {
    }
    LayoutComponent.prototype.ngOnInit = function () { };
    LayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./layout.component.html */ "./src/app/layout/layout.component.html"),
            styles: [__webpack_require__(/*! ./layout.component.scss */ "./src/app/layout/layout.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LayoutComponent);
    return LayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout.module.ts":
/*!*****************************************!*\
  !*** ./src/app/layout/layout.module.ts ***!
  \*****************************************/
/*! exports provided: LayoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutModule", function() { return LayoutModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/sidebar/sidebar.component */ "./src/app/layout/components/sidebar/sidebar.component.ts");
/* harmony import */ var _components_topnav_topnav_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/topnav/topnav.component */ "./src/app/layout/components/topnav/topnav.component.ts");
/* harmony import */ var _layout_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./layout-routing.module */ "./src/app/layout/layout-routing.module.ts");
/* harmony import */ var _layout_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./layout.component */ "./src/app/layout/layout.component.ts");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/layout/nav/nav.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _layout_routing_module__WEBPACK_IMPORTED_MODULE_6__["LayoutRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"]
            ],
            declarations: [_layout_component__WEBPACK_IMPORTED_MODULE_7__["LayoutComponent"], _nav_nav_component__WEBPACK_IMPORTED_MODULE_8__["NavComponent"], _components_topnav_topnav_component__WEBPACK_IMPORTED_MODULE_5__["TopnavComponent"], _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_4__["SidebarComponent"]]
        })
    ], LayoutModule);
    return LayoutModule;
}());



/***/ }),

/***/ "./src/app/layout/nav/nav.component.html":
/*!***********************************************!*\
  !*** ./src/app/layout/nav/nav.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\r\n    <mat-sidenav #drawer class=\"sidenav\" fixedInViewport=\"true\" [attr.role]=\"isHandset ? 'dialog' : 'navigation'\" [mode]=\"(isHandset | async)!.matches ? 'over' : 'side'\"\r\n        [opened]=\"!(isHandset | async)!.matches\">\r\n        <mat-toolbar color=\"primary\">SB Admin Marerial</mat-toolbar>\r\n        <mat-nav-list>\r\n            <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/dashboard']\">\r\n                <mat-icon class=\"sidenav-icon\">dashboard</mat-icon> Dashbard\r\n            </a>\r\n            <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/charts']\">\r\n                <mat-icon class=\"sidenav-icon\">bar_chart</mat-icon> Charts\r\n            </a>\r\n            <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/tables']\">\r\n                <mat-icon class=\"sidenav-icon\">table_chart</mat-icon> Tables\r\n            </a>\r\n            <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/forms']\">\r\n                <mat-icon class=\"sidenav-icon\">input</mat-icon> Forms\r\n            </a>\r\n            <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/grid']\">\r\n                <mat-icon class=\"sidenav-icon\">grid_on</mat-icon> Grid\r\n            </a>\r\n            <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/components']\">\r\n                <mat-icon class=\"sidenav-icon\">code</mat-icon> Components\r\n            </a>\r\n            <a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/blank-page']\">\r\n                <mat-icon class=\"sidenav-icon\">insert_drive_file</mat-icon> Black Page\r\n            </a>\r\n            <a mat-list-item>\r\n                <mat-icon class=\"sidenav-icon\">add</mat-icon> Menu\r\n            </a>\r\n        </mat-nav-list>\r\n    </mat-sidenav>\r\n    <mat-sidenav-content>\r\n        <mat-toolbar color=\"primary\" class=\"fix-nav\">\r\n            <button type=\"button\" aria-label=\"Toggle sidenav\" mat-icon-button (click)=\"drawer.toggle()\" *ngIf=\"(isHandset | async)!.matches\">\r\n                <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\r\n            </button>\r\n            <mat-icon class=\"nav-icon\">menu</mat-icon>\r\n            <span class=\"nav-spacer\"></span>\r\n            <!--\r\n            <mat-icon class=\"nav-icon\">person</mat-icon>\r\n            <span class=\"nav-spacer\"></span>\r\n            <mat-icon class=\"nav-icon\">notifications</mat-icon>\r\n            <mat-icon class=\"nav-icon\">apps</mat-icon>\r\n            <mat-icon class=\"nav-icon\">fullscreen</mat-icon>\r\n            <mat-icon class=\"nav-icon\">flag</mat-icon>\r\n            <mat-icon class=\"nav-icon\">search</mat-icon>\r\n            <mat-icon class=\"nav-icon\">account_circle</mat-icon> -->\r\n            <a class=\"topnav-icon\" [routerLink]=\"['/login']\">\r\n                <mat-icon>exit_to_app</mat-icon>\r\n            </a>\r\n\r\n        </mat-toolbar>\r\n    </mat-sidenav-content>\r\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/layout/nav/nav.component.scss":
/*!***********************************************!*\
  !*** ./src/app/layout/nav/nav.component.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%; }\n\n.sidenav {\n  width: 250px;\n  box-shadow: 3px 0 6px rgba(0, 0, 0, 0.24); }\n\n.topnav-icon {\n  text-decoration: none;\n  display: flex;\n  color: #fff; }\n\n.sidenav-icon {\n  text-decoration: none;\n  padding: 0 10px; }\n\n.nav-spacer {\n  flex: 1 1 auto; }\n"

/***/ }),

/***/ "./src/app/layout/nav/nav.component.ts":
/*!*********************************************!*\
  !*** ./src/app/layout/nav/nav.component.ts ***!
  \*********************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavComponent = /** @class */ (function () {
    function NavComponent(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_0__["Breakpoints"].Handset);
    }
    NavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nav',
            template: __webpack_require__(/*! ./nav.component.html */ "./src/app/layout/nav/nav.component.html"),
            styles: [__webpack_require__(/*! ./nav.component.scss */ "./src/app/layout/nav/nav.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_0__["BreakpointObserver"]])
    ], NavComponent);
    return NavComponent;
}());



/***/ })

}]);
//# sourceMappingURL=layout-layout-module.js.map